pkgs:

{ pkgName
, requirement
}:

pkgs.stdenv.mkDerivation rec {
  name = "go-package-${pkgName}";
  inherit requirement;
  srcGenericShellOptions = ../../include/generic/shell-options.sh;
  srcGenericDirStructure = ../../include/generic/dir-structure.sh;
  buildInputs = with pkgs; [
    cacert
    git
    go
  ];
  builder = ./builder.sh;
}
