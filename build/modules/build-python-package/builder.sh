# shellcheck shell=bash

source "${stdenv}/setup"
source "${srcGenericShellOptions}"
source "${srcGenericDirStructure}"

pip3 install \
    --cache-dir root/python/cache-dir \
    --target    root/python/site-packages \
    --upgrade \
  "${requirement}"

mkdir "${out}"
mv root/python/* "${out}"
