#!/usr/bin/env python3

# pylint: disable= C0325, W0105, W0101, E1102, C0330

"""
--------------------------------------------------------------------------------
                       Pyparsing Commit Message Linter
--------------------------------------------------------------------------------
This script checks ONLY the last commit.
In particular, it does not check for multi-commit Merge Requests,
nor for branches that have not been rebased,
which are also a rejection reason.
A valid commit message must conform to one of
templates/commit-msg-challenges.txt
and in that case we call it a "solution-type" commit,
or start with one of the keywords
feat: fix: style: refac: test:
for internal work on the repository.

Local usage:
add your files:   $ git add xyz
commit them:      $ git commit
It helps if you have set your commit message template
to one of the provided templates.
After comitting, run this script:
$./parse_commit_msg.py
or
$python parse_commit_msg.py
If everything is OK you will get
"Solution commit message OK, continue checks..."
or
"Other commit message OK, continue checks...")
If not, you will see an error message pointing why the commit message is wrong.
In that case you can use
$git commit --amend
to fix the last commit message and re-run the parser.

It is easier to understand the grammar if read from top to bottom, starting
with the top-level parsers and working your way back to the atomic parsers.
"""

import sys
import subprocess
from pyparsing import (Literal, Word, nums, LineStart, LineEnd, Combine,
                       CharsNotIn, srange, Suppress, ParseException, oneOf)

# Parse and fail actions for atomic (lowest-level) parsers


def warnings_sitename(bad_string, loc, _expr, _err):
    """ Warnings for sitename parsers """
    raise ParseException(bad_string, loc,
                         "Sitename must follow repository conventions:\n"
                         + "https://gitlab.com/fluidattacks/writeups"
                         + "/-/wikis/Structure\n")


def warnings_integer(bad_string, loc, _expr, _err):
    """ Warnings for integer parsers """
    raise ParseException(bad_string, loc,
                         "Expected a non-negative integer here. \
                         Only digits.\n")


def warnings_float(bad_string, loc, _expr, _err):
    """ Warnings for float parsers """
    raise ParseException(bad_string, loc,
                         "Expected an non-negative integer or float here.\n")


def warnings_any(bad_string, loc, _expr, _err):
    """ Warnings for anything-except-parens parsers """
    raise ParseException(bad_string, loc,
                         "Anything except parens works here.\n")
    """
    Future: add more specific warnings when faced with a cryptic message.
    sys.exit(1)
    """


def warnings_separator(bad_string, loc, _expr, _err):
    """ Missing blank line warning """
    raise ParseException(bad_string, loc, "Please leave a blank line "
                         + "to separate commit title from body.\n"
                         + "See git-commit(1).\n")


def warnings_issue(bad_string, loc, _expr, _err):
    """ Missing #0 issue """
    raise ParseException(bad_string, loc, "Please add a #0 "
                         + "as explained in commit-msg-systems.txt\n")


# Atomic parsers


INTEGER = Word(nums).setName("entier").setFailAction(warnings_integer)
INTEGER.setParseAction(lambda toks: int(toks[0]))
FLOAT = Combine(Word(nums) + '.' + Word(nums))
FLOAT.setParseAction(lambda toks: float(toks[0]))
NUMBER = (FLOAT | INTEGER).setName("float").setFailAction(warnings_float)
SPACE = Word(' ', exact=1).setFailAction(warnings_sitename)
SOLUTIONS = oneOf(["sol(code):", "sol(hack):"])
SITENAME = Word(srange("[a-z]") + nums + "-"
                ).setName("sitename").setFailAction(warnings_sitename)
ANY_CODE = CharsNotIn("()\n").setName("challcode").setFailAction(warnings_any)
COMPLEXITY = (NUMBER | ANY_CODE)
# warnings_sitename appears in separators to make message easier to understand
TITLE_CHALL = (LineStart() + Suppress(SOLUTIONS("scope"))
               + SPACE(" ")
               + Suppress("#0").setFailAction(warnings_issue)
               + SPACE(" ")
               + SITENAME("sitename")
               + Suppress(",").setFailAction(warnings_sitename)
               + SPACE(" ")
               + SITENAME("code")
               + SPACE(" ")
               + Suppress("(").setFailAction(warnings_sitename)
               + COMPLEXITY("bless") + Suppress(")")
               + Suppress(LineEnd())).leaveWhitespace()
# Future: integrate with title_chall via SCOPE using pyparsing.Forward()

# Parse and fail actions for mid-level parsers


def check_ranks(bad_string, loc, tokens):
    """ Parse action for rank rows """
    ini, fin, pro = tokens
    if fin > ini:
        raise ParseException(bad_string, loc, "Final rank must be less than "
                             + "or equal to initial rank.")
    if ini - fin != pro:
        raise ParseException(bad_string, loc,
                             "Rank progress not correctly computed")
    # Future: in these check_ functions add more checks v.g. against codeabbey
    # or check whether progress is reasonable against absolute ranks, etc.


def check_scores(bad_string, loc, tokens):
    """ Parse action for score row """
    ini, fin, pro = tokens
    if ini > fin:
        raise ParseException(bad_string, loc,
                             "Final score must be greater than "
                             + " or equal to initial score.")
    if abs(fin - ini - pro) > 0.1:
        raise ParseException(bad_string, loc,
                             "Score progress not correctly computed")


def check_others(bad_string, loc, tokens):
    """ Parse action for others and discoveries row """
    ini, fin, pro = tokens
    if ini + fin != pro:
        raise ParseException(bad_string, loc,
                             "Total others not correctly computed")
    # Future: since subprocess has already been imported,
    # why not check against actual number of others in directory
    # (take directory name from site-name in parser:tokens["title"]["sitename"]
    # and in OTHERS.lst

# Helper function to construct mid-level parsers with similar structure


def int_row(label, ini, fin, oper):
    """ Returns a parser with integer row prototype.
    Good for all except scores which might admit decimals.
    "oper" refers to both "total" (addition) and "progress" (subtraction).
    """
    return (LineStart() + Suppress(label)
            + INTEGER("ini") + Suppress(ini)
            + INTEGER("fin") + Suppress(fin)
            + INTEGER("oper") + Suppress(oper))

# Mid-level parsers


OTHERS = int_row("- others:", "in,", "out,", "total.") \
    .setParseAction(check_others)
GLORNK = int_row("- global-rank:", "initial,", "final,", "progress.") \
    .setParseAction(check_ranks)
NATRNK = int_row("- national-rank:", "initial,", "final,", "progress.") \
    .setParseAction(check_ranks)
SCORES = (LineStart() + Suppress("- score:")
          + NUMBER("ini") + Suppress("initial,")
          + NUMBER("fin") + Suppress("final,")
          + NUMBER("oper") + Suppress("progress.")
          + Suppress(LineEnd())
          ).setParseAction(check_scores)

EFFPRO = (LineStart() + Suppress("- effort:") + NUMBER("hours")
          + Suppress("hours during")
          + (Literal("immersion") ^ Literal("challenges"))("phase")
          + Suppress(", productivity") + NUMBER("prod")
          + Suppress("points/hour."))

BLANK = Suppress(LineEnd()
                 ).setName("blank line").setFailAction(warnings_separator)
# setFailAction commented out above because
# since the problem string is empty, produces a RecursionError later on.


# Parse action for top-level parsers


def challenge_checks(bad_string, loc, tokens):
    """For checking things that go across lines,
    like blessing in title being the same as score progress."""
    sco_progress = tokens["score"]["oper"]
    sco_title = tokens["title"]["bless"]
    if sco_progress != sco_title:
        if tokens["title"]["sitename"] == "codeabbey":
            if abs(sco_progress - sco_title) > 0.1:
                raise ParseException(bad_string, loc,
                                     ("The progress in score ({0}) must be "
                                      + "the same as the blessing ({1})."
                                      ).format(sco_progress, sco_title))
        else:
            print(("The progress in score ({0}) is usually "
                   + "the same as the challenge complexity ({1})."
                   ).format(sco_progress, sco_title))
    computed_productivity = float(tokens["effort"]["prod"])
    hours = tokens["effort"]["hours"]
    actual_productivity = float(sco_progress)/float(hours)
    if abs(actual_productivity - computed_productivity) > 0.1:
        raise ParseException(bad_string, loc, "Productivity incorrect")
    # Future: here add codeabbey/wechall checks.


# Top-level parsers

SOLUTION_COMMIT = (TITLE_CHALL("title")
                   + BLANK("blank")
                   + OTHERS("others")
                   + SCORES("score")
                   + GLORNK("global")
                   + NATRNK("national")
                   + EFFPRO("effort")
                   ).setName("challsol").setParseAction(challenge_checks)

CLASSIFIER = SOLUTIONS.setResultsName("type")

# pp.Or not used because it generates ugly exceptions
# Instead we chose to use the CLASSIFIER parser above to
# determine which of the top-level parsers to use
# and generate the appropriate warnings.


def handle_except_exit(exception, mess):
    """ Final error message. Prints line and a pointer to
    the guilty column. """
    print(exception.line)
    print(' '*(exception.col-1) + '^')
    print(exception)
    print(mess)
    sys.exit(1)


def parse_commit(message):
    """ Main function to parse a commit message """
    try:
        commit = CLASSIFIER.parseString(message)
        if commit["type"] == "sol(code):" or commit["type"] == "sol(hack):":
            try:
                SOLUTION_COMMIT.parseString(message)
                print("Challenge commit message OK, continue checks...")
                sys.exit(0)
            except ParseException as exc_bad_challenge:
                emsg = ("\nIf this is a commit for a CHALLENGE solution,"
                        + "\nfollow templates/commit-msg-challenges.txt:")
                handle_except_exit(exc_bad_challenge, emsg)
        else:
            print("Other commit message OK, continue checks...")
            sys.exit(0)
    except ParseException:
        print("Challenge commit message OK, continue checks...")
        sys.exit(0)


if len(sys.argv) > 1:  # for bats testing
    MESSAGE = sys.argv[1]
    parse_commit(MESSAGE)

else:
    try:
        CMD_OUT = subprocess.check_output('git log -1 --pretty=%B', shell=True)
        MESSAGE = CMD_OUT.decode('utf-8')
        parse_commit(MESSAGE)
    except subprocess.CalledProcessError as cmd_err:
        print("Something went wrong while retrieving the last commit:")
        print(cmd_err)
        sys.exit(1)
