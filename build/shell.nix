let
  pkgs = import ./pkgs/stable.nix;

  nodejsEnv = import ./modules/nodejs-env pkgs;
  pythonEnv = import ./modules/python-env pkgs;
  rubyEnv = import ./modules/ruby-env pkgs;

  buildGoPackage = import ./modules/build-go-package pkgs;
  buildNodejsModule = import ./modules/build-nodejs-module pkgs;
  buildPythonPackage = import ./modules/build-python-package pkgs;

in
  pkgs.stdenv.mkDerivation rec {
    name = "builder";

    srcGenericShellOptions = ./include/generic/shell-options.sh;
    srcGenericDirStructure = ./include/generic/dir-structure.sh;
    srcJavaCheckstyle =
      let
        repo = "https://github.com/checkstyle/checkstyle";
        file = "checkstyle-8.29/checkstyle-8.29-all.jar";
      in
        pkgs.fetchurl {
          url = "${repo}/releases/download/${file}";
          sha256 = "1rbipf4031inv34ci0rczz7dipi3b12cpn45h949i095gdh37pgh";
        };
    srcJavaCheckstyleConfig =
      let
        repo = "https://raw.githubusercontent.com/checkstyle/checkstyle";
        rev = "95defdd68df471f69b0749474c3b5b7a5e76b4f9";
        file = "src/main/resources/google_checks.xml";
      in
        pkgs.fetchurl {
          url = "${repo}/${rev}/${file}";
          sha256 = "14ws8bympxn4dcrd2yfjsvs6cwacqs6c029k690g9n2zh17fq16r";
        };
    srcPhpCodeSnifferCbf = pkgs.fetchurl {
      url = "https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar";
      sha256 = "18x7fk59l821pivw1i2r868y78qgs0qk47b9il1smwi6plwyyflr";
    };
    srcPhpCodeSnifferCs = pkgs.fetchurl {
      url = "https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar";
      sha256 = "1lrybdxxig3yqd3i3nwji5jjn377p50sbpm4s4852dlsxz9qnlgs";
    };

    goPkgGocyclo = buildGoPackage {
      pkgName = "gocyclo";
      requirement = "github.com/fzipp/gocyclo";
    };
    goPkgGolint = buildGoPackage {
      pkgName = "golint";
      requirement = "golang.org/x/lint/golint";
    };

    nodejsModuleHusky = buildNodejsModule {
      moduleName = "husky";
      requirement = "husky@1.2.1";
    };
    nodejsModuleCommitlintCli = buildNodejsModule {
      moduleName = "commitlintCli";
      requirement = "@commitlint/cli@7.6.0";
    };
    nodejsModuleCommitlintConfigConventional = buildNodejsModule {
      moduleName = "commitlintConfigConventional";
      requirement = "@commitlint/config-conventional@7.6.0";
    };
    nodejsModuleEslintConfigStrict = buildNodejsModule {
      moduleName = "eslint-config-strict";
      requirement = "eslint-config-strict@14.0.1";
    };

    pyPkgPykwalify = buildPythonPackage {
      pkgName = "pykwalify";
      requirement = "pykwalify==1.7.0";
    };
    pyPkgPylint = buildPythonPackage {
      pkgName = "pylint";
      requirement = "pylint==2.4.4";
    };
    pyPkgPyparsing = buildPythonPackage {
      pkgName = "pyparsing";
      requirement = "pyparsing==2.2.2";
    };
    pyPkgYamllint = buildPythonPackage {
      pkgName = "yamllint";
      requirement = "yamllint==1.17.0";
    };

    buildInputs = with pkgs; [
      bats
      cacert
      cppcheck
      cucumber
      curl
      docker
      file
      hlint
      git
      go
      gnupg
      nix-linter
      nodejsEnv
      openjdk
      php
      pre-commit
      pythonEnv
      rubyEnv
      shellcheck
      splint
    ];
  }
