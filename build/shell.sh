#! /usr/bin/env nix-shell
#!   nix-shell -i bash
#!   nix-shell --keep CI_COMMIT_REF_NAME
#!   nix-shell --keep CI_REGISTRY
#!   nix-shell --keep CI_REGISTRY_IMAGE
#!   nix-shell --keep CI_REGISTRY_PASSWORD
#!   nix-shell --keep CI_REGISTRY_USER
#!   nix-shell --keep GITLAB_USER_LOGIN
#!   nix-shell --keep SITE_TRIGGER_TOKEN
#!   nix-shell --option restrict-eval false
#!   nix-shell --option sandbox false
#!   nix-shell --pure
#!   nix-shell --show-trace
#!   nix-shell shell.nix
#  shellcheck shell=bash

# shellcheck disable=SC2154
# shellcheck source=./build/include/generic/shell-options.sh
source "${srcGenericShellOptions}"

function ensure_required_external_environment_vars {
  local required_external_var
  local required_external_vars=(
    CI_REGISTRY
    CI_REGISTRY_IMAGE
    CI_REGISTRY_PASSWORD
    CI_REGISTRY_USER
    GITLAB_USER_LOGIN
    SITE_TRIGGER_TOKEN
  )

  for required_external_var in "${required_external_vars[@]}"
  do
    if test -z "${!required_external_var:-}"
    then
      echo "[WARNING] For better results export: ${required_external_var}"
      echo "[WARNING] Exporting ${required_external_var} as an empty string"
      export "${required_external_var}"=''
    fi
  done
}

function prepare_ephemeral_vars {
  export TEMP_FD
  export TEMP_FILE

  exec {TEMP_FD}>TEMP_FD
  TEMP_FILE=$(mktemp)
}

function prepare_environment_vars {
  export COMMIT_SUMMARY
  export CURRENT_BRANCH
  export FALSE
  export IS_SOLUTION_COMMIT
  export TRUE

  TRUE='true'
  FALSE='false'

  COMMIT_SUMMARY=$(git log --max-count=1 --format=%s)

  test "${COMMIT_SUMMARY}" = 'sol'* \
    && IS_SOLUTION_COMMIT="${TRUE}" \
    || IS_SOLUTION_COMMIT="${FALSE}"

  test -n "${CI_COMMIT_REF_NAME:-}" \
    && CURRENT_BRANCH="${CI_COMMIT_REF_NAME}" \
    || CURRENT_BRANCH=$(
      git rev-parse --abbrev-ref HEAD 2>/dev/null \
        || echo "unknown-branch")

}

function prepare_packages {
  echo '[INFO] Setting environment packages'
  export PATH
  local go_packages=(
    "${goPkgGocyclo}"
    "${goPkgGolint}"
  )
  local node_modules=(
    "${nodejsModuleHusky}"
    "${nodejsModuleCommitlintCli}"
    "${nodejsModuleCommitlintConfigConventional}"
    "${nodejsModuleEslintConfigStrict}"
  )
  local python_packages=(
    "${pyPkgPykwalify}"
    "${pyPkgPylint}"
    "${pyPkgPyparsing}"
    "${pyPkgYamllint}"
  )

  # set Go packages
  for go_package in "${go_packages[@]}"
  do
    GOPATH="${GOPATH}:${go_package}"
    PATH="${PATH}:${go_package}/bin"
    chmod +x "${go_package}/bin/"*
  done

  # set NodeJS modules
  for node_module in "${node_modules[@]}"
  do
    NODE_PATH="${NODE_PATH}:${node_module}/node_modules"
  done

  # set Python packages
  for python_package in "${python_packages[@]}"
  do
    PATH="${PATH}:${python_package}/site-packages/bin"
    PYTHONPATH="${PYTHONPATH}:${python_package}/site-packages"
    if test -e "${python_package}/site-packages/bin"
    then
      chmod +x "${python_package}/site-packages/bin/"*
    fi
  done
}

function prepare_worktree {
  export WORKTREE
  export PRE_COMMIT_HOME

  WORKTREE=$(readlink -f "${PWD}/../training.ephemeral")
  echo '[INFO] Deleting previous worktree'
  rm -rf "${WORKTREE}"
  echo '[INFO] Adding a pristine worktree'
  cp -r . "${WORKTREE}"
  echo '[INFO] Entering the worktree'
  cd "${WORKTREE}"
  echo '[INFO] Setting binaries'
  cp "${srcJavaCheckstyle}" ./java-checkstyle.jar
  cp "${srcJavaCheckstyleConfig}" ./java-checkstyle-config.xml
  cp "${srcPhpCodeSnifferCbf}" ./phpcbf.phar
  cp "${srcPhpCodeSnifferCs}" ./phpcs.phar
  echo '[INFO] Setting cache directories'
  mkdir .cache
  mkdir .cache/pre-commit
  PRE_COMMIT_HOME="${PWD}/.cache/pre-commit"
}

function prepare_commitlint {
  local base_repo='https://gitlab.com/autonomicmind/default'
  local base_url="${base_repo}/raw/master/commitlint-configs/training"
  local parser_name='parser-preset.js'
  local rules_name='commitlint.config.js'

  echo '[INFO] Preparing external files'
  for file_to_fetch in "${parser_name}" "${rules_name}"
  do
    if ! test -f "${file_to_fetch}"
    then
      curl "${base_url}/${file_to_fetch}" > "${file_to_fetch}"
    fi
  done
}

function helper_display_last_commit_info {
  git log --max-count=1 --show-signature
}

function helper_display_rebase_status {
  local commits_ahead_of_master

  commits_ahead_of_master=$(git log --format=%h HEAD...origin/master | wc -l)

  echo "[INFO] Testing the following commit:"
  helper_display_last_commit_info | helper_indent

  case "${commits_ahead_of_master}" in
    '0')
      echo '[WARNING] You have not commited your changes!!'
      ;;
    '1')
      echo '[OK] You are exactly one commit ahead of master'
      ;;
    *)
      echo '[WARNING] You are too ahead or behind of master, please rebase!'
      echo '  [HEAD] origin/master'
      git log --format='[%h] %s' HEAD...origin/master \
        | helper_indent \
        | helper_indent
      echo '  [HEAD] local'
      ;;
  esac
}

function helper_indent {
  sed 's/^/  /g'
}

function helper_list_declared_jobs {
  declare -F | grep -oP 'job_[a-z_]+' | sort
}

function job_build_features {
  find code hack -type f -wholename '*.feature' \
    -exec cucumber --format rerun {} +
}

function job_lint_build_code {
     shellcheck --external-sources build.sh \
  && find 'build' -name '*.sh' -exec \
      shellcheck --external-sources --exclude=SC1090,SC2154, {} + \
  && echo '[OK] Shell code is compliant' \
  && nix-linter --recursive . \
  && echo '[OK] Nix code is compliant' \

}

function job_check_commit_message {
     git log -1 --pretty=%B HEAD | npx commitlint \
  && pushd build/modules/parse-commit-msg \
  &&   pushd tests \
  &&     echo '[INFO] Testing the parser' \
  &&     ./test-commit-msg-parser.sh \
  &&   popd \
  &&     echo '[INFO] Using the parser to check your commit message' \
  &&   ./commit_msg_parser.py \
  && popd \

}

function job_check_files {
  source ./build/modules/checks/lib.sh

     check_directory_depth \
  && check_for_duplicates_in_others_lst \
  && check_for_non_png_evidences \
  && check_for_misplaced_png_evidences \
  && check_mimes \
  && check_no_atfluid_account \
  && check_that_filenames_are_short \
  && check_that_github_urls_are_raw \
  && check_that_solution_and_commit_author_match \
  && check_that_there_is_not_asc_extension \
  && check_that_there_are_no_tabs \
  && check_that_paths_contain_only_allowed_characters \
  && check_80_columns_margin \
  && lint_java \
  && run_pre_commit \

}

function job_check_schemas {
  source ./build/modules/checks/lib.sh

     check_lang_data \
  && check_site_data \

}

function job_check_urls {
  source ./build/modules/checks/lib.sh

     check_status_code_of_urls_in_file 'LINK.lst' \
  && check_status_code_of_urls_in_file 'OTHERS.lst' \

}

function job_trigger_default_deploy {
  if test "${CURRENT_BRANCH}" = 'master'
  then
    echo '[INFO] Triggering pipeline'
    curl \
        --request 'POST' \
        --form "ref=master" \
        --form "token=${SITE_TRIGGER_TOKEN}" \
      "https://gitlab.com/api/v4/projects/10466586/trigger/pipeline"
  else
    echo '[SKIPPED] Not on master branch'
  fi
}

function job_deploy_ci_container {
  local tag

  if test "${CURRENT_BRANCH}" = 'master'
  then
    tag="${CI_REGISTRY_IMAGE}:ci"

    echo "[INFO] Login in: ${CI_REGISTRY}"
    docker login --username "${CI_REGISTRY_USER}" \
                 --password "${CI_REGISTRY_PASSWORD}" \
      "${CI_REGISTRY}"

    echo "[INFO] Pulling: ${tag}"
    (
      set +o errexit
      docker pull "${tag}"
    )

    echo "[INFO] Building: ${tag}"
    docker build --tag "${tag}" 'build/'

    echo "[INFO] Pushing: ${tag}"
    docker push "${tag}"
  else
    echo '[SKIPPED] Not on master branch'
  fi
}

function job_all {
  local function_to_call

  # Execute all job functions except this mere one
  helper_list_declared_jobs | while read -r function_to_call
  do
    echo "[INFO] Executing function: ${function_to_call}"
    test "${function_to_call}" = "job_all" \
      || "${function_to_call}" \
      || return 1
  done
}

function cli {
  local function_to_call

  function_to_call="${1:-}"

  if test -z "${function_to_call}" \
      || test "${function_to_call}" = '-h' \
      || test "${function_to_call}" = '--help'
  then
    echo
    echo "Use: ./build.sh [job-name]"
    echo
    echo 'List of jobs:'
    helper_list_declared_jobs | sed -e 's/job_/  * /g'
    return 1
  else
    echo
    ensure_required_external_environment_vars
    helper_display_rebase_status
    prepare_commitlint
    prepare_ephemeral_vars
    prepare_environment_vars
    prepare_packages
    prepare_worktree
    echo "[INFO] Executing function: job_${function_to_call}"
    if "job_${function_to_call}"
    then
      echo
      echo "Successfully executed: ${function_to_call}"
      echo '  Congratulations!'
      return 0
    else
      echo
      echo 'We have found some problems with your commit'
      echo '  You can replicate this output locally with:'
      echo "    training $ ./build.sh ${function_to_call}"
      return 1
    fi
  fi
}

cli "${@}"
