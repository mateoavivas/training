/**
linting
$ haxelib run checkstyle -s Andres568.hx
Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
No issues found.
**/
class Andres568 {

    //Haxe applications have a static entry point called main
    static function main() {
        var content:String = sys.io.File.getContent("DATA.lst");
        var numbers:Array<String> = content.split(" ");
        var result:Int = Std.parseInt(numbers[0]) + Std.parseInt(numbers[1]);
        trace(result);
    }
}
/**
$ haxe -main Andres568 --interp
Andres568.hx:15: 18772
**/
