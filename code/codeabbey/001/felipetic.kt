/*
$ ktlint felipetic.kt #linting
$ kotlinup run felipetic.kt #compilation
Finished dev [unoptimized + debuginfo] target(s) in 0.22 secs
$ ktlint felipetic.kt
$
*/

fun main(args: Array<String>) {

    val listNumbers = File("DATA.lst").readLines()
    val a = listNumbers[0].toInt()
    val b = listNumbers[1].toInt()
    println("Sum: ${a + b}")
}

/*
$ ./felipetic.kt
19735
*/
