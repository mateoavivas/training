#! /usr/bin/crystal

# $ ameba richardalmanza.cr  #linting
# Inspecting 1 file.
# .
# Finished in 2.91 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release #compilation

require "option_parser"

version = "0.1.1"

def sum(args)
  result = 0
  len = args.size

  while len > 0
    len -= 1

    begin
      num = args[len].is_a?(String) ? args[len].try &.to_f : args[len]
    rescue ArgumentError
      next
    end

    if num.is_a?(Number)
      result += num
    else
      next
    end
  end

  result
end

OptionParser.parse do |parser|
  parser.banner = "SUM A + B \n" \
                  "a program to practice and learn Crystal Language\n" \
                  "file.cr number number number\n" \
                  "[Example] sum-a+b 023 56 112.5 0.5\n" \
                  "[Expected] 192.0\n\n" \
                  "[Weird Stuff] sum-a+b.cr -- -- 03 55 .5 t 10 fdf -192 \n" \
                  "[Expected] -123.5\n"

  parser.on "-v", "--version", "Show version" do
    puts "Version => #{version}"
    exit
  end

  parser.on "-h", "--help", "Show help" do
    puts parser
    exit
  end

  parser.on "-t", "--test", "Sum 7588 and 14071" do
    puts (sum [7588, 14071])
    exit
  end

  parser.invalid_option do |option_flag|
    STDERR.puts "Invalid option #{option_flag}"
    STDERR.puts ""
    puts parser
    exit(1)
  end
end

puts (sum ARGV)

# $ ./richardalmanza.cr -- -- $(cat DATA.lst)
# 18772.0       // if DATA.lst file contains "4419 14353"
