/**
linting
$ haxelib run checkstyle -s Andres568.hx
Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
No issues found.
**/
using StringTools;

/**
class with the main method
**/
class Andres568 {

    //Haxe applications have a static entry point called main
    static function main() {
        var cont:String = sys.io.File.getContent("DATA.lst");
        var formatedCont:String = StringTools.rtrim(cont.replace("\n", " "));
        var numbers:Array<String> = formatedCont.split(" ").slice(1);
        var sum:Int = 0;
        for (i in 0...numbers.length) {
            sum = sum + Std.parseInt(numbers[i]);
        }
        trace(sum);
    }
}
/**
$ haxe -main Andres568 --interp
Andres568.hx:19: 31752
**/
