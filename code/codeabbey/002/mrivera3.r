# $ lintr::lint('mrivera3.r')


sumloop <- function() {
  "loop for adding"
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- 0
  for (i in seq_len(length(data))) {
    results <- results + data[1, i]
  }
  return(results)
}
sumloop()

# $ Rscript mrivera3.r
#31752
