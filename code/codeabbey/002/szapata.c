/*
$cppcheck szapata.c
Checking szapata.c ...
$splint *.c
Splint 3.1.2 --- 20 Feb 2018
Finished checking --- no warnings
$gcc szapata.c -o szapata
*/

#include <stdio.h>

int main(){

    int numberTestCases=0;
    # ifndef S_SPLINT_S
    (void)scanf("%d",&numberTestCases);
    # endif
    int i=0;
    for (i = 0; i < numberTestCases; i=i+1) {
      int firsValue=0;
      int stepSize=0;
      int numberValue=0;
      int sum=0;
      # ifndef S_SPLINT_S
      (void)scanf("%d",&firsValue);
      (void)scanf("%d",&stepSize);
      (void)scanf("%d",&numberValue);
      # endif
      int current=firsValue;
      int j=0;
      for (j = 0; j < numberValue; j++) {
        sum+=current;
        current+=stepSize;
      }
      printf("%d ", sum);
    }
return 1;
}

/*
$ ./szapata
21 30
*/
