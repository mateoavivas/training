"""
$ pylint calderonsin.py
Global evaluation
----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def main():
    """Main function"""
    data = open('DATA.lst', 'r')
    number = int(data.readline())
    a_1 = [None] * number
    b_1 = [None] * number
    sum_ = []
    for i in range(0, number):
        numbers = data.readline()
        words = numbers.split()
        a_1[i] = int(words[0])
        b_1[i] = int(words[1])
        sum_.append(a_1[i]+b_1[i])
    for i in sum_:
        print(i, " ")


main()


# $ python3 calderonsin.py build
# 602139  543855  1409727  707784
# 745831  738766  1493476  792403
# 881291  811859  732132  1458154
# 1016464 1525208 666583
