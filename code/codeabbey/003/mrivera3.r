# $ lintr::lint('mrivera3.r')


library(readr)
loopsum <- function() {
  "loop sum"
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    prov <- sum(data[i, ])
    results <- c(results, prov)
  }
 return(results)
}
loopsum()

# $ Rscript mrivera3.r
# 602139  543855 1409727  707784  745831  738766 1493476  792403
# 881291  811859  732132 1458154 1016464 1525208  666583
