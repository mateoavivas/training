#! /usr/bin/crystal

# $ ameba richardalmanza.cr #linting
# Inspecting 1 file.
# .
# Finished in 5.67 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

require "option_parser"

version = "0.2.0"
oput = ""
integer_out = false

def sum(args)
  result = 0
  len = args.size

  while len > 0
    len -= 1

    begin
      num = args[len].is_a?(String) ? args[len].try &.to_f : args[len]
    rescue ArgumentError
      next
    end

    if num.is_a?(Number)
      result += num
    else
      next
    end
  end

  result
end

def pairs(args)
  result = ""
  n_pairs = [] of Array(String)
  args = args[-1].split('\n') if args.size == 1
  args = args.map { |x| x.split(' ') }
  args = args.flatten

  args = args[1..] if args.size % 2 != 0

  (0...args.size).step(2).each do |x|
    n_pairs << [args[x], args[x + 1]]
  end

  n_pairs.each do |x|
    result = "#{result} #{sum(x).to_s}"
  end
  result
end

OptionParser.parse do |parser|
  parser.banner = "SUM A + B \n" \
                  "a program to practice and learn Crystal Language\n" \
                  "file.cr number number number\n" \
                  "[Example] sum-a+b 023 56 112.5 0.5\n" \
                  "[Expected] 192.0\n\n" \
                  "[Weird Stuff] sum-a+b.cr -- -- 03 55 .5 t 10 fdf -192 \n" \
                  "[Expected] -123.5\n"

  parser.on "-v", "--version", "Show version" do
    puts "Version => #{version}"
    exit
  end

  parser.on "-h", "--help", "Show help" do
    puts parser
    exit
  end

  parser.on "-t", "--test", "Sum 7588 and 14071" do
    puts (sum [7588, 14071])
    exit
  end

  parser.on "-i", "--integer", "The ouput will be rounded down" do
    integer_out = true
  end

  parser.on "-p", "--pairs", "Sum between pairs" do
    oput = pairs ARGV
  end

  parser.invalid_option do |option_flag|
    STDERR.puts "Invalid option #{option_flag}"
    STDERR.puts ""
    puts parser
    exit(1)
  end
end

if oput == ""
  oput = (sum ARGV).to_s
end

if integer_out
  oput = oput.gsub(/\.[0-9]*\ /, " ")
  oput = oput.gsub(/(\.[0-9]*)$/, " ")
end

oput = oput.gsub(/^\ /, "")

puts oput

# $ ./richardalmanza.cr -- -p -i $(cat DATA.lst)
# 602139 543855 1409727 707784 745831 738766 1493476 792403  // wrapped here
# 881291 811859 732132 1458154 1016464 1525208 666583 // here the continuation
