/**
linting
$ haxelib run checkstyle -s Andres568.hx
Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
No issues found.
**/
using StringTools;

/**
class with the main method
**/
class Andres568 {

    //Haxe applications have a static entry point called main
    static function main() {
        var cont:String = sys.io.File.getContent("DATA.lst");
        var formatedCont:String = StringTools.rtrim(cont.replace("\n", " "));
        var numbers:Array<String> = formatedCont.split(" ").slice(1);
        var index:Int = 0;
        var result:String = "";
        var firstNumber:Int = 0;
        var secondNumber:Int = 0;

        while (index < numbers.length) {
            firstNumber = Std.parseInt(numbers[index]);
            secondNumber = Std.parseInt(numbers[index + 1]);

            if (firstNumber < secondNumber) {
                result = result + firstNumber + " ";
            }
            else {
                result = result + secondNumber + " ";
            }

            index = index + 2;
        }
        trace(result);
    }
}
/**
$ haxe -main Andres568 --interp
Andres568.hx:37: -2605178 -8641437 -1673153 -8560512 391090 -6006084 -446852
-7897953 5695945 6727636 -6232151 320213 -8684553 -7369740 -4312186 3082635
-9672707 -7403785 -1345861 -9665427 -8571476 -5498134 1971156 3278711 1395820
1187365 -6111861 -4366784
**/
