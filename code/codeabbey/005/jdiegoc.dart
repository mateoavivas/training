/**
 * $ dartanalyzer jdiegoc.dart
 * Analyzing jdiegoc.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    var n1,n2,n3;

    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      cas.split(" ");
      n1 = int.parse(cas[1]);
      n2 = int.parse(cas[2]);
      n3 = int.parse(cas[3]);

      if (n1<n2 && n1>n3) || (n1>n2 && n1<n3){
        print(n1);
      }
      if (n2<n1 && n2>n3) || (n2>n1 && n2<n3){
        print(n2);
      }
      if (n3<n2 && n3>n1) || (n3>n2 && n3<n1){
        print(n3);
      }
    }
  }
  );
}

/* $ dart jdiegoc.dart
 107814 386086 -5721210 -3265497 -9006366 124682 -455274 -4695046 -2027624
 -2726599 6289004 -2232700 -5564619 -9844812 -3110310 -2335335 -9253696
 -9053831 -3981282 -2784460 -1406274 -8164135 3309434
*/
