/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    var n1,n2;

    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      var cas2 = cas.split(" ");
      n1 = int.parse(cas2[0]);
      n2 = int.parse(cas2[1]);
      print((n1/n2).round());
    }
  }
  );
}

/* $ dart mrsossa.dart
 * 2 5 4 91528 8 -4 18117 30841 -1 -19 6 -90 10 10 13 4 7
*/
