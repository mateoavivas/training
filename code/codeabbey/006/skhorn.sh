#!/bin/bash
#
# Problem #6 Rounding
#
while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" | wc -c)
    if [[ "$len" -gt 2 ]]
    then
        count=1
        FRACTION_PART=0.5
        data_array=($line)

        val_1="${data_array[0]}"
        val_2="${data_array[1]}"

        division=$(echo "scale=1; $val_1/$val_2" | bc)
        compare=$(echo "$division>0" | bc)

        if [[ "$compare" -gt 0 ]]
        then
            division=$(echo "$division+$FRACTION_PART" | bc)
        else
            division=$(echo "$division-$FRACTION_PART" | bc)
        fi

        output_array[$count]="${division%.*}"
        let "count+=1"
    fi

    printf '%s ' "${output_array[@]}"
done < "$1"
