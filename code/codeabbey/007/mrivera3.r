# $ lintr::lint('mrivera3.r')
#------------------------------------
#lintr gives no comments when score is perfect



fartocel <- function() {
"changing fahrenheit to celcius"
  data1 <- scan("DATA.lst")
  celsius <- c()
  for (i in 2:length(data1))
    celcius <- c(celcius, round((data1[i] - 32) * (5 / 9)))
  return(celcius)
}

fartocel()

# $ Rscript mrivera3.R
# 0 247 116  26  42  38 146  57 139  77  94 117 309 232 264  11 138 169 278
# 132 126 263 293 138 262 123 170 298 154  11 121 154 257 237
