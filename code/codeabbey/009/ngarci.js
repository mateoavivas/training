/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const inputs = inputData.toString().split('\r\n');

  for (let i = 1; i <= Number(inputs[0]); i++) {
    const subArray = inputs[i].split(' ').map((x) => Number(x));
    const maxOfThree = Math.max(...subArray);
    const sumOfSubArray = subArray
      .reduce((accumulator, currentValue) => accumulator + currentValue);
    if (maxOfThree <= sumOfSubArray - maxOfThree) {
      results.push(1);
    }
    else {
      results.push(0);
    }
  }

  console.log(results.map((x) => x.toString()).join(' '));
});

/*
$ js ngarci
output:
0 0 1 0 0 1 0 1 0 0 1 0 1 1 1 0 1 0 0 0 1 1
*/
