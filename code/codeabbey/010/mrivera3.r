# $ lintr::lint('mrivera3.r')


linfn <- function() {
  "finding a and b from y = b + mx"
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  m <- (data[, 4] - data[, 2]) / (data[, 3] - data[, 1])
  b <- data[, 2] - data[, 1] * m
  results <- data.frame(m, b, stringsAsFactors = FALSE)
  return(results)
}
linfn()

# $ Rscript mrivera3.r
#   (-78 -834) (-48 -353) (95 621) (58 -495) (91 -681) (-60 254) (-10 430)
#   (-32 -645) (-48 -855) (-21 -279) (-66 520) (-97 -212) (11 268) (-48 -699)
#   (73 -977)
