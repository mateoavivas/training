"use strict";

/**
 * Computes the slope and intercept for a line trhough the given points.
 * @param {number} x1 The x-coordinate of one point.
 * @param {number} y1 The y-coordinate of one point.
 * @param {number} x2 The x-coordinate of another point.
 * @param {number} y1 The y-coordinate of another point.
 * @return {string} (m b) where m is the slope and b the intercept.
*/
function lineThruPoints(x1, y1, x2, y2) {
    var m = (y1 - y2) / (x1 - x2);
    var b = y1 - m * x1;
    return "(" + m + " " + b + ")";
}

var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += lineThruPoints(Number(line[0]), Number(line[1]),
        Number(line[2]), Number(line[3])) + " ";
}
console.log(salida);
