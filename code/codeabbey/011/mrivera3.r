# $ lintr::lint('mrivera3.r')


library(readr)
digsum <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  calc <- data$X1 * data$X2 + data$X3
  string <- as.character(calc)
  sep <- strsplit(string, "")
  results <- c()
  for (i in seq_len(length(sep))) {
    sep[[i]] <- as.integer(sep[[i]])
  }
  for (i in seq_len(length(sep))) {
    results <- c(results, sum(sep[[i]]))
  }
  print(results)
}
digsum()

# $ Rscript mrivera3.r
#21 18 26 28 22 22 11 22 17 19 27 28 21 22
