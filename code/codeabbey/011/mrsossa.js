/*
 $ eslint mrsossa.js
*/
/* global print */

import { readFileSync } from 'fs';

const dat = readFileSync('DATA.lst').toString().split('\n');
for (let i = 0; i < dat.length - 1; i++) {
  if (i !== 0) {
    const token = dat[i].split(' ');
    const nums = (Number(token[0]) * Number(token[1])) + Number(token[2]);
    let sum = 0;
    for (let cas = 0; cas < nums.toString().length; cas++) {
      sum += Number(nums.toString().charAt(cas));
    }
    print(sum);
  }
}

/*
 $ js mrsossa.js
 21 18 26 28 22 22 11 22 17 19 27 28 21 22
*/

