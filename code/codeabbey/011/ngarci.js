/*
$ eslint ngarci
$ node ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const inputs = inputData.toString().split('\r\n');

  for (let i = 1; i <= Number(inputs[0]); i++) {
    const subArray = inputs[i].split(' ').map((x) => Number(x));
    const operation = (subArray[0] * subArray[1]) + subArray[2];
    const digitsArray = operation.toString().split('');
    let sumOfDigits = 0;
    for (let Index = 0; Index < digitsArray.length; Index++) {
      sumOfDigits += Number(digitsArray[Index]);
    }
    results.push(sumOfDigits);
  }
  const outputs = results.map((x) => x.toString()).join(' ');

  files.writeFile('Output.txt', outputs, (issue) => {
    if (issue) {
      throw issue;
    }
  });
});

/*
$ node ngarci
output:
21 18 26 28 22 22 11 22 17 19 27 28 21 22
*/
