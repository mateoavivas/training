# $ lintr::lint('mrivera3.r')


library(readr)
distancet <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  daysmin <- (data$X5 - data$X1) * 24 * 60
  hourmin <- (data$X6 - data$X2) * 60
  minmin <- (data$X7 - data$X3)
  secmin <- (data$X8 - data$X4) / 60
  total <- daysmin + hourmin + minmin + secmin
  days_fl <- total / (24 * 60)
  days <- as.integer(days_fl)
  hour_fl <- (days_fl - days) * 24
  hour <- as.integer(hour_fl)
  minute_fl <- (hour_fl - hour) * 60
  minute <- as.integer(minute_fl)
  second <- (minute_fl - minute) * 60
  for (i in seq_len(nrow(data))) {
    cat("(", days[i], hour[i], minute[i], second[i], ")")
  }
}
distancet()

# $ Rscript mrivera3.r
# ( 17 19 1 41 )( 17 10 23 45 )( 15 7 59 4 )( 6 5 48 34 )( 0 8 47 27 )
# ( 2 2 9 6 )( 17 5 13 33 )( 11 19 37 8 )( 5 3 49 34 )( 0 0 2 4 )
