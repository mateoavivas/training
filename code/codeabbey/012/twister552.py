data=[x.split() for x in """7 14 28 43 25 9 30 24
2 16 23 49 20 2 47 34
6 1 38 50 21 9 37 54
6 12 41 49 12 18 30 23
8 12 19 21 8 21 6 48
10 19 22 11 12 21 31 17
11 6 43 7 28 11 56 40
12 23 15 32 24 18 52 40
20 4 18 25 25 8 7 59
13 6 16 12 13 6 18 16""".splitlines()]

for array in data:
  for i,value in enumerate(array):
    array[i]=int(value)

def MTD(args):
  for array in args:
    deltaS=(array[4]-array[0])*86400+(array[5]-array[1])*3600+(array[6]-array[2])*60+(array[7]-array[3])
    day=int(deltaS/86400)
    hours=int((abs(deltaS/86400)-day)*24)
    minutes=int((abs((abs(deltaS/86400)-abs(day))*24)-hours)*60)
    seconds=round(((abs((abs(deltaS/86400)-abs(day))*24)-hours)*60-minutes)*60)
    print("(",end="")
    print(day,hours,minutes,seconds,end="")
    print(")",end="")
    
MTD(data)
