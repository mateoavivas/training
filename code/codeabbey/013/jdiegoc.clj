(comment "
 $ lein check
   Compiling namespace jdiegoc.clj
 $ lein eastwood
   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
   Directories scanned for source files:
     src test
   == Linting jdiegoc.clj ==
   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
")

(ns jdiegoc.clj
  (:gen-class)
)

(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
         sum=0
         i=1
         e =>
         sum = sum + (e.toInt * (i))
         i = i + 1
         (print (sum))
          )
        )
      )
    )
  )

(defn -main [& args]
  (process_file "DATA.lst")
)

(comment "
 $ clojure jdiegoc.clj
 38 118 253 154 27 15 139 8 2 38 34 60
 21 6 138 113 128 204 179 128 8 224 46
 1 40 172 68 110 31 142 26 15 37 1 170 9
")
