#$ julia
#julia> using Lint
#julia> lintfile("mmarulandc.jl")

file = open("DATA.lst")
lines = readlines(file)
length = parse(Int,lines[1])
numbers = split(lines[2], " ")

function process(numberLength,number)
  result = 0
  auxNumber = parse(Int,number)
  position = numberLength
  next = auxNumber
  for i = 1: numberLength
    lastNumber = next % 10
    next = floor(Int32,auxNumber / 10)
    auxNumber = next
    result = floor(Int32,result + (lastNumber * position))
    position = position - 1
  end
  return string(result)
end

function wsd(length, numbers)
  result = ""
  for number in numbers
    numberLength = sizeof(number)
    print(process(numberLength,number)," ")
  end
end

wsd(length,numbers)

#$ julia mmarulandc
#124 17 99 28 11 39 8 50 42 81 53 134 6 64 11 39 71 27 16 175
#110 167 161 24 245 161 104 62 5 18 104 29 3 25 28 133 5 1
