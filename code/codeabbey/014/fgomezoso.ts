/*
$ tslint fgomezoso.ts
$ tsc fgomezso.ts
*/

/*
This program makes arithmetic calculations sequentially and it returns
the remainder of all operations divided by the last number.
*/

import * as fs from "fs";

const operators = [];
const nums = [];
let M = 0;

const data = fs.readFileSync("DATA.lst", "utf8");
const inputArray = data.split("\r\n");

let result = parseInt(inputArray.shift(), 10);

inputArray.pop();

for (const index of inputArray) {

    const splitted = index.split(" ");
    operators.push(splitted[0]);
    nums.push(parseInt(splitted[1], 10));

    if (index[0] === "%") {
        M = parseInt(splitted[1], 10);
    }
}

for (let i = 0; i < operators.length; i++) {

    switch (operators[i]) {
        case "*":
            const intermediate = result * nums[i];

            if (intermediate > M) {
                result = (( result % M) * (nums[i] % M )) % M;
            }
            else {
                result *= nums[i];
            }
            break;
        case "+":
            result += nums[i];
            break;
        case "-":
            result -= nums[i];
            break;
        case "%":
            result %= nums[i];
            break;
    }
}

console.log(result);

/*
$ node fgomezoso.js
2412
*/
