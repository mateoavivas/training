# $ lintr::lint('mrivera3.r')

library(stringr)
modc <- function() {
  data <- read_csv("DATA.lst", col_names = FALSE)
  initial <- as.integer(data$X1[1])
  split <- str_split_fixed(data$X1[seq_len(nrow(split))], " ", 2)
  res <- initial
  for (i in seq_len(nrow(split))) {
    if (split[i, 1] == "+") {
      res <- res + as.numeric(split[i, 2])
    }
    else if (split[i, 1] == "*") {
      res <- res * as.numeric(split[i, 2])
    }
    else if (split[i, 1] == "%") {
      res <- res %% as.numeric(split[i, 2])
    }
  }
  return(res)
}
modc()

# $ Rscript mrivera3.r
#2412
