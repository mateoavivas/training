; $ lein check
;   Compiling namespace mrsossa.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting mrsossa.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns mrsossa.clj
  (:gen-class))

(defn stoi
  ([string]
    (map #(Integer/parseInt %)
       (clojure.string/split string #" ")
    )
  )
)

(defn process_file
  (def sum 0)
  ([file]
   (with-open [rdr (clojure.java.io/reader file)]
     (doseq [line (line-seq rdr)]
       (let [l (stoi line)
          v (into [] l)]
          (let [n1 (get v 0)
            n2 (get v 1)
            (if (= n1 "+")
              sum (+ sum (int n2)))
            (if (= n1 "*")
              sum (* sum (int n2)))
            (if (= n1 "%")
              sum (mod sum (int n2))
             )]
           )
         )
       )
     )
   )
  (print (str sum))
  )

(defn -main
  ([& args]
   (process_file "DATA.lst")
   (println)))

; $lein run
;  sutcac tuoba ydrapeoj flehs ffo reppus eraf no
