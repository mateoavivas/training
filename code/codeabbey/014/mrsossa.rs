/*
$ rustup run nightly cargo clippy
Compiling mrsossa.rs
$ rustc mrsossa.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("DATA.lst").unwrap();
    let reader = BufReader::new(file);
    let mut sum: <&u64> = 0.0;
    for lines in reader.lines() {
        let line = lines.unwrap();
        if sum == 0.0 {
            sum = line.parse().unwrap();
        }
        else {
            let token:Vec<&str> = line.split(" ").collect();
            if token[0] == "+" {
                sum = sum + token[1].parse().unwrap();
            }
            else if token[0] == "*" {
                sum = sum + token[1].parse().unwrap();
            }
            else {
                sum = sum + token[1].parse().unwrap();
            }
        }
    }
    println!("{}",sum);
}

/*
 $ rustc mrsossa.rs
 $ ./mrsossa
 2412
*/
