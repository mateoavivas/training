#!/bin/bash
#
# Problem #14 Modular Calculator
# 
while read -r line || [[ -n "$line" ]];
do
    line=$(echo "$line" | sed 's/*/\x/g')

    data_array=($line)
    if [[ "${#data_array[@]}" -eq 1 ]]
    then
        total="${data_array[0]}"
    else
        operator="${data_array[0]}"
        num="${data_array[1]}"
        if [ "$operator" == "+" ]
        then
            total=$(echo "$total"+"$num" | bc)

        elif [ "$operator" == "x" ]
        then
            total=$(echo "$total"*"$num" | bc)

        elif [ "$operator" == "%" ]
        then 
            total=$(echo "$total"%"$num" | bc)
        fi
    fi


done < "$1"

printf '%s ' "$total"
