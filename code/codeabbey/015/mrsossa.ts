/*
 $ tslint mrsossa.ts
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
    let mayor = -999999999999999;
    let menor = 999999999999999;
    const token = line.split(" ");
    for (const toke of token) {
        if (+toke < menor) {
            menor = +toke;
        }
        if (+toke > mayor) {
            mayor = +toke;
        }
    }
    console.log(mayor);
    console.log(menor);
});
/*
 $ tsc mrsossa.ts
 $ node mrsossa.js
 79241 -78703
*/
