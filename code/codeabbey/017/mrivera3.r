# $ lintr::lint('mrivera3.r')


check <- function() {
  data <- scan("DATA.lst")
  data <- data[2:length(data)]
  result <- 0
  for (i in seq_len(length(data))) {
    result <-  (result + data[i])  * 113
    result <- as.bigz(result)
  }
  result <- result %% 10000007
  return(result)
}
check()

# $ Rscript mrivera3.r
# 6617103
