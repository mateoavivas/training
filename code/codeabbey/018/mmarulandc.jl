#$ julia
#julia> using Lint
#julia> lintfile("mmarulandc.jl")

function squareRoot()
  file = open("DATA.lst")
  lines = readlines(file)
  result = ""
  for i = 2 : parse(Int,lines[1]) + 1
    numbers = split(lines[i], " ")
    r = 1
    x = parse(Int,numbers[1])
    n = parse(Int,numbers[2])
    for i = 1 :  n
      r = (r + (x/r))/2
    end
    print(r, " ")
  end
end

squareRoot()

#$ julia mmarulandc
#7.416198487095663 6.08276253029822 22.068076490713914 293.5188235491771 5.47722557564769
#81.15417426109393 59.67411499134277 89.70507232035433 2178.9583211890717 27.53179979587241
#12.755078162170646 9.110433579154478 4.898979485566356
