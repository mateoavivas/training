; $ lein check
;   Compiling namespace mrsossa.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting mrsossa.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns mrsossa.clj
  (:gen-class))

(defn stoi
  ([string]
   (map #(Integer/parseInt %)
        (clojure.string/split string #""))))

(defn process_file
  ([file]
   (with-open [rdr (clojure.java.io/reader file)]
     (doseq [line (line-seq rdr)]
       (let [l (stoi line)
             v (into [] l)
             sum (set 0)]
         (loop [i (count l)]
           (if (or (= (get v i) "a") (= (get v i) "e") (= (get v i) "i"))
             sum (+ sum 1))
           (if (or (= (get v i) "o") (= (get v i) "u") (= (get v i) "y"))
             sum (+ sum 1))
           )
         )
       (print (str sum))
       )
     )
   )
)

(defn -main
  ([& args]
   (process_file "DATA.lst")
   (println)))

; $lein run
;  10 11 15 14 17 11 10 126 6 5 8 11 10 16 9

