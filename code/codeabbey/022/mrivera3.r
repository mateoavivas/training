# $ lintr::lint('mrivera3.r')


dprinter <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  res <- c()
  for (i in seq_len(nrow(data))) {
    x <- data$X1[i]
    y <- data$X2[i]
    n <- data$X3[i]
    maxi <- ceiling(max(x, y) / as.numeric(x + y) * n) * min(x, y)
    minm <- ceiling(min(x, y) / as.numeric(x + y) * n) * max(x, y)
    count <- min(maxi, minm)
    res <- c(res, count)
  }
  return(res)
}
dprinter()

# $ Rscript mrivera3.r
# 318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
# 309055713  33295378 322580664 359769725 332892224 162592318 175123025
