# $ lintr::lint('mrivera3.r')


bubbarray <- function() {
  data <- scan("DATA.lst")
  data <- data[seq_len(length(data) - 1)]
  counter <- 0
  result <- 0
  for (i in seq_len(length(data) - 1)) {
    a <- data[i]
    b <- data[i + 1]
    if (data[i] > data[i + 1]) {
      data[i] <- b
      data[i + 1] <- a
      counter <- counter + 1
    }
  }
  library(gmp)
  for (i in seq_len(length(data))) {
    result <-  (result + data[i])  * 113
    result <- as.bigz(result)
  }
  result <- result %% 10000007
  print(counter); print(result)
}
bubbarray()

# $ Rscript mrivera3.r
# 41 1962385
