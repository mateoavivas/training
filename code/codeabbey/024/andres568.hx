/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var result:String = "";
    var inputData:Array<String> = [];
    var numbers:Array<Int> = [];
    var number:Int = 0;
    var randomNumber:Int = 0;
    var interactions:Int = 0;

    inputData = lines[1].split(" ");

    for (i in 0...inputData.length) {
      number = Std.parseInt(inputData[i]);
      randomNumber = 0;
      interactions = 0;
      numbers = [number];
      randomNumber = Std.int(((number * number) / 100) % 10000);
      interactions++;

      while (true) {
        if (numbers.indexOf(randomNumber) != -1) {
          break;
        }
        numbers.push(randomNumber);
        randomNumber = Std.int(((randomNumber * randomNumber) / 100) % 10000);
        interactions++;
      }

      result = result + " " + interactions;
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:48:  107 105 104 100 101 110 97 103 108 103 101 106
**/
