# $ lintr::lint('mrivera3.r')


neumann <- function() {
  data <- scan("DATA.lst")
  data <- data[-1]
  res <- c()
  for (j in seq_len(length(data))) {
    a <- data[j]
    i <- 0
    values <- c(a)
    repeat {
      a <- as.integer(substr(str_pad(a^2, 8, pad = "0"), 3, 6))
      i <- i + 1
      if (a %in% values) {
        break
      }
      values <- c(values, a)
    }
    res <- c(res, i)
  }
  return(res)
}
neumann()

# $ Rscript mrivera3.r
#107 105 104 100 101 110  97 103 108 103 101 106
