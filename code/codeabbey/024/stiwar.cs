using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyNameSpace
{
    class Program{

      static void Main(string[] args){
        
          int lengthNumber,alCuadrado,intentos;
          int counter;
          bool start = true;
          int[] numberArray = new int[8];
          int[] fourDigitsArray = new int[4];
          List<int> listInt;// = new List<int>();
          int[] intentosArray, output;
          Console.WriteLine("ingrese el numero de intentos:");
          intentos = Int32.Parse(Console.ReadLine());
          intentosArray = new int[intentos];
          output = new int[intentos];
          
          for(int i=0;i<intentos;i++){
            Console.WriteLine("ingrese el numero " + (i+1) + " de maximo 4 digitos:");
            intentosArray[i] = Int32.Parse(Console.ReadLine());
          }
          
          for(int p=0;p<intentos;p++){
            alCuadrado = intentosArray[p];
            listInt = new List<int>();
          
          start = true;
          counter = 0;
          
          while (start){
            alCuadrado *= alCuadrado;

            if(alCuadrado == 0){
              output[p] = (counter + 1);
              start = false;
              break;
            }else{
                  lengthNumber = Convert.ToInt32( Math.Floor(Math.Log10(alCuadrado) + 1) );
                  
                  if(listInt.Count > 1){//validar sino ha entrado a un loop
                    if(Operation.ValidateLoop(listInt)){
                      output[p] = counter;
                      start = false;
                      break;
                    }
                  }
                  //validar el numero de digitos del numero
                  if(lengthNumber==8){      
                    numberArray = Operation.digitArr(alCuadrado);
                  }else{
                    int[] tempnumberArray = new int[lengthNumber];
                    int zeros = 8 - lengthNumber;//numero de ceros que debo agregar para completar el array de 8 posiciones
                    tempnumberArray = Operation.digitArr(alCuadrado);
                    Operation.FillNumberData(zeros, tempnumberArray, numberArray);
                  }
                  
                  //creamos el array de 4 digitos
                  Operation.CreateFourDigitNumber(fourDigitsArray, numberArray);
                  
                  //convertimos el array de 4 posiciones a entero de 4 digitos:
                  alCuadrado = Operation.ArrtoDigit(fourDigitsArray);
                  listInt.Add(alCuadrado);
                  
                  //if(alCuadrado == number){
                  if(alCuadrado == intentosArray[p]){  
                    output[p] = (counter + 1);
                    start = false;
                    break;
                  }else{
                    counter++;
                  }
            }//end primer else
          }//end while
          }
          //print output
          for(int p=0;p<intentos;p++){
            Console.Write(output[p]+" ");
          }
      }
    }
    
    class Operation{
      
      public static int[] digitArr(int n)
      {
          var digits = new List<int>();
      
          for (; n != 0; n /= 10)
              digits.Add(n % 10);
      
          var arr = digits.ToArray();
          Array.Reverse(arr);
          return arr;
      }
      
      public static int ArrtoDigit(int[] intArray){
        return intArray.Select((t, i) => t * Convert.ToInt32(Math.Pow(10, intArray.Length - i - 1))).Sum();
      }
      
      public static bool ValidateLoop(List<int> listInt){
        if(listInt.Count != listInt.Distinct().Count())
        {
          return true;            
        }
        return false;
      }
      
      public static void FillNumberData(int zeros, int[] tempnumberArray, int[] numberArray)
      {
          for (int c = 0; c < zeros; c++)
          {
              numberArray[c] = 0;
          }
                    
          int j = 0;
          for(int cm = zeros;cm<=(8 -zeros);cm++){
            numberArray[cm] = tempnumberArray[j];                
            j++;
          }
      }
      
      public static void CreateFourDigitNumber(int[] fourDigitsArray, int[] numberArray){
        //creamos el array de 4 digitos
        int t = 0;
        for(int i=2; i<6;i++){
          fourDigitsArray[t] = numberArray[i];                
          t++;
        }
      }
      
    }
}
