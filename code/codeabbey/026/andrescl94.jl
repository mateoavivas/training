#=
$ julia
julia> using Lint
julia> lintfile("andrescl94.jl")
=#

using Compat.Printf

function process_file(filename::String)
  result = String[]
  open(filename, "r") do f
    for (i, ln) in enumerate(eachline(f))
      if i == 1
        continue
      end
      nums = parse.(Int64, split(ln))
      gcd_num = string(gcd(nums[1], nums[2]))
      lcm_num = string(lcm(nums[1], nums[2]))
      push!(result, @sprintf("(%s %s)", gcd_num, lcm_num))
    end
  end
  return result
end

function main()
  result = process_file("DATA.lst")
  println(join(result, ' '))
end

main()

#=
$ julia andrescl94.jl
(30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
(1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641)
(1 10) (30 48180) (160 184800) (2 40)
=#
