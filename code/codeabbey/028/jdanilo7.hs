{- Linting:
$ hlint jdanilo7.hs
No hints

Compilation:
$ ghc -Wall -Wmissing-local-signatures -Wmissing-signatures jdanilo7.hs
[1 of 1] Compiling Main             ( jdanilo7.hs, jdanilo7.o )
Linking jdanilo7.exe ...
-}

diagnose :: Float -> String
diagnose x
  | x >= 30    = "obese "
  | x >= 25    = "over "
  | x >= 18.5  = "normal "
  | otherwise  = "under "

diagAll :: [[Float]] -> String
diagAll = foldr
        (\ x -> (++) (diagnose (bodMasIndex (head x) (head (tail x)))))
        ""

bodMasIndex :: Float -> Float -> Float
bodMasIndex w h = w / h ** 2

splitLines :: [String] -> [[Float]]
splitLines = map (map (read :: String -> Float) . words)

main :: IO ()
main = do
    src <- readFile "DATA.lst"
    let lins = lines src
    putStr $ diagAll $ splitLines $ tail lins

{- $ runhaskell jdanilo7.hs
over normal under obese obese obese obese over under normal normal under
normal normal obese normal obese under normal obese obese normal obese under
obese under under obese obese obese under obese normal normal under
-}
