/*
tslint jdiegoc.ts
jdiegoc.ts file  pass linting
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
 const token = line.split(" ");
 +token[2] == ((+token[0] / +token[1]) ** 2);

 if ((+token[2] < 18.5)){
    console.log("under");
 }

 if ((+token[2] < 25 && +token[2] >= 18.5)){
    console.log("normal");
 }

 if ((+token[2] < 30 && +token[2] >= 25)){
    console.log("over");
 }

 if ((+token[2] >= 30 )){
    console.log("obese");
 }
});

/*
 $ts jdiegoc.ts
 over normal under obese obese obese obese over under normal normal
 under normal normal obese normal obese under normal obese obese
 normal obese under obese under under obese obese obese under obese
 normal normal under
*/
