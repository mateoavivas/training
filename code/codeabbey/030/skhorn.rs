/*
$ rustup run nightly cargo clippy
Compiling skhorn v0.1.0 (file:///../skhorn)
Finished dev [unoptimized + debuginfo] target(s) in 0.57 secs
$ rustc skhorn.rs
$
*/

//Read file 
use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
  let file = File::open("DATA.lst").unwrap();

  let mut output_vec = Vec::new(); 

  for line in BufReader::new(file).lines() {

    let cur_line: &str = &line.unwrap();
    let array: Vec<&str> =   cur_line.split(' ').collect();

    for i in (0 .. array.len()).rev() {
      //println!("{}", array[i]);
      let mut _word: String = array[i].to_string();
      let mut output: String = Default::default();
      for item in _word.chars().rev() {
        output += &item.to_string();
      }
      output_vec.push(output);
    }
    
    let mut result: String = Default::default();
    for item in &output_vec {
      result.push(' ');
      result.push_str(item);
    }
    println!("{}", result);
  }
}

/*
$ ./skhorn 
sutcac tuoba ydrapoej flehs ffo reppus eraf no
*/
