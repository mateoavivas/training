/*
$ cargo clippy
Checking neds v0.1.0 (/home/ndsanchez/Code/rust/neds)
Finished dev [unoptimized + debuginfo] target(s) in 0.86s
$ cargo run
Compiling neds v0.1.0 (/home/ndsanchez/Code/rust/neds)
Finished dev [unoptimized + debuginfo] target(s) in 1.29s
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let filepath = "src/DATA.lst";
  let file = File::open(filepath).unwrap();
  let reader = BufReader::new(file);
  // Read the file
  for (_index, line) in reader.lines().enumerate() {
    let line = line.unwrap();
    let data: Vec<&str> = line.split(' ').collect();
    if _index != 0 {
      let n = data[0].parse::<i32>().unwrap();
      let string = data[1];
      rotate_string(n, string);
    }
  }
}

fn rotate_string(n: i32, string: &str) {
  let tale: &str;
  let head: &str;
  let length = string.len() as usize;
  if n > 0 {
    tale = &string[0..n as usize];
    head = &string[n as usize..length];
  } else {
    let ind = n.abs() as usize;
    head = &string[length-ind..length];
    tale = &string[0..length-ind];
  }
  print!("{}{} ", head, tale);
}

/*
$ cargo run
Running `target/debug/neds`
whomthebelltollsfor numberverycomplex
*/
