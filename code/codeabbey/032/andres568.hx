/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var inputData:Array<String> = [];
    var result:Float = 0.0;
    var k:Int = 0;
    var n:Int = 0;
    var index:Int = 1;

    lines.pop();
    inputData = lines[0].split(" ");
    n = Std.parseInt(inputData[0]);
    k = Std.parseInt(inputData[1]);

    while (index <= n){
      result = (result + k) % index;
      index++;
    }

    result++;
    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:36: 54
**/
