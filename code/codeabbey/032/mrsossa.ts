/*
 $ tslint mrsossa.ts
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
    const token = line.split(" ");
    const n = +token[0];
    const k = +token[1];
    let res = 0;
    let i = 1;
    while (i <= n) {
        res = (res + k) % i;
        i += 1;
    }
    console.log(res);
});

/*
 $ tsc mrsossa.ts
 $ node mrsossa.js
 53
*/
