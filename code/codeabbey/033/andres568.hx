/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var inputData:Array<String> = [];
    var binary:String = "";
    var filteredBinary:String = "";
    var result:String = "";
    lines.pop();
    inputData = lines[0].split(" ");

    function DecimalToBinary(num) {
      if (num > 1) {
        DecimalToBinary(Math.floor(num / 2));
      }
      binary = binary + (num % 2);
    }

    for (i in 0...inputData.length) {
      binary = "";
      DecimalToBinary(Std.parseInt(inputData[i]));
      filteredBinary = binary.replace("0", "");

      if ([2, 4, 6, 8].indexOf(filteredBinary.length) != -1) {
        if (binary.charAt(0) == "1" && binary.length == 8) {
          result += String.fromCharCode(Std.parseInt(inputData[i]) - 128);
        }
        else {
          result += String.fromCharCode(Std.parseInt(inputData[i]));
        }
      }
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:48:
  8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
**/
