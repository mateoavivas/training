/*
$ eslint jarboleda.js
$
*/

function decTobin(dec) {
  return (parseInt(dec, 10)).toString(2);
}

function parseToBinary(encodedChar) {
  const preParse = decTobin(encodedChar);
  const zeros = '00000000';
  const duringParse = zeros.concat(preParse);

  const postParse = duringParse.slice(duringParse.length - zeros.length);
  return postParse;
}

function getParityTwo(stringified, currentPosition, sum) {
  if (currentPosition >= stringified.length) {
    return sum;
  }
  if (stringified[currentPosition] === '1') {
    return getParityTwo(stringified, currentPosition + 1, sum + 1);
  }
  return getParityTwo(stringified, currentPosition + 1, sum);
}

function getParity(stringified) {
  return getParityTwo(stringified, 0, 0) % 2;
}

function solve(encodedChar) {
  const stringified = parseToBinary(encodedChar);
  const parity = getParity(stringified);
  if (parity === 1) {
    return '';
  }
  const newString = '0'.concat(stringified.slice(1));
  return String.fromCharCode(parseInt(newString, 2));
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const answer = contents.split(' ').map((encodedChar) =>
    solve(encodedChar)
  ).join('');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
54
*/
