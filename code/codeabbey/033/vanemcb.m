%checkcode('vanemcb.m')

clear
clc
fileID = fopen('DATA.lst');
vecInput = fscanf(fileID,'%f');
cellCod = cell(length(vecInput),4);
vecFinal = zeros(1,length(vecInput));
cont = 0;
cont2 = 0;

for i=1:length(vecInput)
  cellCod{i,1} = vecInput(i,1);
  cellCod{i,2} = dec2bin(vecInput(i,1));
  for k=1:length(cellCod{i,2})
    if cellCod{i,2}(1,k) == '1'
      cont = cont + 1;
    end
  end
  cellCod{i,3} = cont;
  cont = 0;
  if length(cellCod{i,2}) == 8 && mod(cellCod{i,3},2) == 0
    cellCod{i,2}(1,1) = '0';
    cellCod{i,4} = 'x';
  end
  if mod(cellCod{i,3},2) == 0
    vecFinal(1,i) = bin2dec(cellCod{i,2});
  end
end
vecFinal(vecFinal==0) = [];
vecFinal = char(vecFinal);
disp(vecFinal);

%vanemcb
%8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
