/*
$ dartanalyzer neds.dart
Analyzing neds.dart...
No issues found!
$ dart neds.dart
*/

import 'dart:io';
import 'dart:math';

const double minRange = 0.0;
const double maxRange = 100.0;
double left = minRange;
double right = maxRange;
double x = maxRange;
double res = 1;

binarySearch(double a, double b, double c ,double d) {
  while (res < 0 || res >= 1e-7) {
    res = a * x + b * sqrt(pow(x, 3)) - c * exp(- x / 50) - d;
    (res > 0)? right = x: left = x;
    x = (left + right) / 2;
  }
  stdout.write("$x ");
  x = maxRange;
  res = 1;
  left = minRange;
  right = maxRange;
}

void readFile() {
  File file = new File('DATA.lst');
  List<String> lines = file.readAsLinesSync();
  List<String> dat = [];
  int i = 0;
  double a, b, c, d;
  lines.forEach((l) => {
    if (i != 0) {
      dat = l.split(" "),
      a = double.parse(dat[0]),
      b = double.parse(dat[1]),
      c = double.parse(dat[2]),
      d = double.parse(dat[3]),
      binarySearch(a, b, c, d)
    },
    i = 1
  });
}

void main() {
  readFile();
}

/*
$ dart neds.dart
59.32347947964445 29.824244919291232 86.18108126247535
10.640756721841171 23.504429499735124 30.97452640213305
48.741526788217016
*/
