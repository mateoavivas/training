/*
$ eslint jarboleda.js
$
*/
function getNumberOfMatches(theRecibedNumber, theCurrentNumberStringified) {
  const filtered = theRecibedNumber.split('').filter((char, index) =>
    (char === theCurrentNumberStringified[index])
  );

  return filtered.length;
}

function solveFor(inputFile, currentNumber) {
  if (currentNumber < 0) {
    const errorMessage = -1;
    return errorMessage;
  }
  const booleans = inputFile.map((line) => {
    const lineParts = line.split(' ');
    const theRecibedNumber = lineParts[0].toString();
    const expectedMatches = parseInt(lineParts[1], 10);
    const format = '0000';
    const theCurrentNumberStringified = format.concat(currentNumber.toString())
      .slice(-format.length);
    const numbeOfMatches = getNumberOfMatches(theRecibedNumber,
      theCurrentNumberStringified);
    return (expectedMatches === numbeOfMatches);
  });
  const condition = booleans.reduce((previous, current) =>
    (previous && current), true);
  if (condition === true) {
    return currentNumber;
  }
  return solveFor(inputFile, currentNumber - 1);
}

function solveForInverse(inputFile, currentNumber) {
  const maxNumber = 10000;
  if (currentNumber >= maxNumber) {
    const errorMessage = -1;
    return errorMessage;
  }
  const booleans = inputFile.map((line) => {
    const lineParts = line.split(' ');
    const theRecibedNumber = lineParts[0].toString();
    const expectedMatches = parseInt(lineParts[1], 10);
    const format = '0000';
    const theCurrentNumberStringified = format.concat(currentNumber.toString())
      .slice(-format.length);
    const numbeOfMatches = getNumberOfMatches(theRecibedNumber,
      theCurrentNumberStringified);
    return (expectedMatches === numbeOfMatches);
  });

  const condition = booleans.reduce((previous, current) =>
    (previous && current), true);
  if (condition === true) {
    return currentNumber;
  }
  return solveForInverse(inputFile, currentNumber + 1);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n').slice(1);
  try {
    const maxValue = 9999;
    const answer = solveFor(inputFile, maxValue);
    const output = process.stdout.write(`${ answer }\n`);
    return output;
  } catch (theErr) {
    const answer = solveForInverse(inputFile, 0);
    const output = process.stdout.write(`${ answer }\n`);
    return output;
  }
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
5347
*/
