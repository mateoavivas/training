/*
$ eslint jarboleda.js
$
*/
/*
code from
https://derickbailey.com/2014/09/21/
calculating-standard-deviation-with-array-map-and-array-reduce-in-javascript/
as in production a library would be used, lets not reinvent the wheel
also this is common public knowledge
*/

function average(values) {
  const summation = values.reduce((sum, value) =>
    (sum + value)
  , 0);
  const avg = summation / values.length;
  return avg;
}

function standardDeviation(values) {
  const avg = average(values);
  const squareDiffs = values.map((value) => {
    const diff = value - avg;
    const sqrDiff = diff * diff;
    return sqrDiff;
  });
  const avgSquareDiff = average(squareDiffs);
  const stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}
/*
end of code from outside
*/

function solveFor(inputLine) {
  const namesOfStock = inputLine.split(' ').slice(0, 1);
  const valuesOfStock = inputLine.split(' ').slice(1).map((pricePoint) =>
    (parseInt(pricePoint, 10))
  );
  const avgStockPrice = average(valuesOfStock);
  const stdStockPrice = standardDeviation(valuesOfStock);
  const magicNumberOne = 4;
  const magicNumberTwo = 0.01;
  if (avgStockPrice * magicNumberOne * magicNumberTwo <= stdStockPrice) {
    return namesOfStock;
  }
  return '';
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n').slice(1);
  const answer = inputFile.map((inputLine) => (solveFor(inputLine)))
    .filter((word) => word.length > 0);
  const output = process.stdout.write(`${ answer.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
GOLD
*/
