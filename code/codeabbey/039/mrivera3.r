# $ lintr::lint('mrivera3.r')


stockvol <- function() {
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    input <- unlist(data[i, 2:15])
    m <- mean(input)
    std <- sd(input)
    stock <- as.character(data[i, 1])
    if (std > 4 * m / 100) {
      results <- c(results, stock);
    }
  }
  print(results, quote = FALSE)
}
stockvol()

# $ Rscript mrivera3.r
# DALG FANT FLNT JOOG CKCL MYTH WOWY INSX IMIX SUGR
# ZEOD GEEK JABA MARU PNSN SLVR VDKL
