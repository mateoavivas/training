print_string "ingrese la cantidad de intentos: \n";;
let c = ref (read_int ());;
let out = Array.make !c "no";;
let j = ref 0;;
while !c > 0 do
  let floatArray = Array.make  14  (-1.0) in (*crear Array con valores negativos al inicio*)
    print_string "ingrese los datos separados por un espacio: \n";
    let data = read_line () in
      let dataList = (Str.split (Str.regexp " ") data) in
        for i=0 to ((Array.length floatArray) - 1) do  
          floatArray.(i) <- float_of_string  (List.nth dataList (i+1));
        done;
        let sum = ref 0.0 in
          let m = ref 0.0 in
          for i=0 to ((Array.length floatArray) - 1) do
            sum := !sum +. floatArray.(i);
            m := (!sum) /. (float (Array.length floatArray));
          done;
          let comi = !m /. 100.0 in
            (*desviacion estandar*)
            let sum_d = ref 0.0 in
              for i=0 to ((Array.length floatArray) - 1) do
                sum_d := !sum_d +. ( (!m -. floatArray.(i)) *. (!m -. floatArray.(i)) );                
              done;
              let mean = !sum_d /. (float (Array.length floatArray)) in
                let des = sqrt mean in
                  if(des >= (4.0*.comi))then(                      
                      out.(!j) <- (List.nth dataList (0));
                      j := !j + 1;
                    );
    if (!c = 1) then(
      for k=0 to (!j - 1) do
        if( k != (!j - 1 ) ) then Printf.printf "%s " out.(k) else Printf.printf "%s \n" out.(k) ;
      done;
    );
  c := !c - 1;
done;;
