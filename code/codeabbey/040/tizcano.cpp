/*
  $ g++ tizcano.cpp -o tizcano.out
  $
*/
#include<iostream>
#include<fstream>

using namespace std;

int main() {
  int M, N;
  ifstream input("DATA.lst");
  cin.rdbuf(input.rdbuf());
  cin>>M>>N;
  char grid[M][N];
  int counter[M][N] = { -1 };
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < N; ++j) {
      cin>>grid[i][j];
    }
  }
  for (int i = 1; i < M; ++i) {
    if (grid[i][0] == '+') {
      counter[i][0] = 1;
    } else {
      break;
    }
  }
  for (int i = 1; i < N; ++i) {
    if (grid[0][i] == '+') {
      counter[0][i] = 1;
    } else {
      break;
    }
  }
  counter[0][0] = 1;
  for (int i = 1; i < M; ++i) {
    for (int j = 1; j < N; ++j) {
      if (grid[i][j] == '+' || grid[i][j] == '$') {
        if (counter[i - 1][j] > 0 && counter[i][j - 1] > 0) {
          counter[i][j] = counter[i - 1][j] + counter[i][j - 1];
        } else {
          if (counter[i - 1][j] > 0) {
            counter[i][j] = counter[i - 1][j];
          } else if (counter[i][j - 1] > 0) {
            counter[i][j] = counter[i][j - 1];
          }
        }
      }
    }
  }
  if (counter[M - 1][N - 1] < 0) {
    cout<<0<<endl;
  } else {
    cout<<counter[M - 1][N - 1]<<endl;
  }
  return 0;
}
/*
  $ ./tizcano.out
  output:
  3547333
*/
