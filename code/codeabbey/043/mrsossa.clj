; $ lein check
;   Compiling namespace mrsossa.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting mrsossa.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns mrsossa.clj
  (:gen-class))

(defn process_file
  (def sum 0)
  ([file]
   (with-open [rdr (clojure.java.io/reader file)]
     (doseq [line (line-seq rdr)]
       res (Math/floor (+ (* (int line) 6) 1))
       (print (str res))
       )
     )
   )
  )

(defn -main
  ([& args]
   (process_file "DATA.lst")
   (println)))

; $lein run
; 6 3 1 6 6 5 4 1 5 4 4 6 3 4 1 6 6 2 5 4 3 4 5 2
