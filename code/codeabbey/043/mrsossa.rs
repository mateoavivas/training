/*
$ rustup run nightly cargo clippy
Compiling mrsossa.rs
$ rustc mrsossa.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("DATA.lst").unwrap();
    let reader = BufReader::new(file);

    for (i, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        if i != 0 {
            let res = line.parse::<f64>().unwrap() * (6) as f64;
            println!("{}",(res+0.5).round());
        }
    }
}

/*
 $ rustc mrsossa.rs
 $ ./mrsossa
 6 3 1 6 6 5 4 1 5 4 4 6 3 4 1 6 6 2 5 4 3 4 5 2
*/
