"""
$ pylint calderonsin.py
Global evaluation
-----------------
Your code has been rated at 10.00/10 (previous run: 7.50/10, +2.50)

"""


def dice():
    "convert 2 Integer to values between [0,6] and sum both of them"
    data = open('DATA.lst', 'r')
    iteration = int(data.readline())
    for i in range(iteration):
        string = data.readline()
        numbers = string.split()
        number1 = int(numbers[0])
        number2 = int(numbers[1])
        dice1 = (number1 % 6) + 1
        dice2 = (number2 % 6) + 1
        print(dice1 + dice2, " ")
        i = i + 1


dice()


# $ python calderonsin.py build
# 5  7  10  4  9  7  6  4  6  8  3
# 11  10  9  4  9  5  8  4  8  5  5
