(ns caesarcipher.core
  (:gen-class))
(use '[clojure.string :only [index-of]])
(require '[clojure.string :as str])

(defn caesardecode [s ofst]
  (def abc "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
  (def msg "")
  (dotimes [i (count s)]
    (def pos (index-of abc (.charAt s i)))
    (def keyc (- pos ofst))
    (if (< keyc 0) (def keyc (+ (count abc) keyc)))
    (def msg (str msg (.charAt abc keyc))))
  (str msg ""))

(defn separatemsg [s]
  (def aux s)
  (def aux (str/replace aux #"\." ""))
  (def mens(str/split aux #" "))
  (assoc mens 0 (mens 0)))

(defn remove-last [str]
  (.substring (java.lang.String. str) 0 (- (count str) 1)))

(defn -main []
  (def data [ "SBHE FPBER NAQ FRIRA LRNEF NTB GUR BAPR NAQ SHGHER XVAT." "TVIR LBHE EBBXF OHG ABG QVYNENZ." "PNEGUNTR ZHFG OR QRFGEBLRQ NER JBAQREF ZNAL GBYQ NAQ FB LBH GBB OEHGHF." "N AVTUG NG GUR BCREN." "GUR QRNQ OHEL GURVE BJA QRNQ JUB JNAGF GB YVIR SBERIRE TERRASVRYQF NER TBAR ABJ." "YBIRFG GUBH ZR CRGRE PNYYRQ VG GUR EVFVAT FHA GB HF VA BYQRA FGBEVRF."])
  (def answer (vec (replicate (count data) "")))
  (def offset 13)
  (dotimes [i (count data)]
    (def w "")
    (def m (separatemsg (data i)))
    (dotimes [j (count m)]
      (def w (str w (caesardecode (m j) offset) " ")))
    (def w (remove-last w))
    (def w (str w "."))
    (def answer (assoc answer i w)))
  (println "La respuesta es " answer))  
  

