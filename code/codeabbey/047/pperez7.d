/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;

void main() {
  File file = File("DATA.lst", "r");
  string input = file.readln();
  string [] parameters = input.split(" ");
  string answers = "";
  const int lines = parse!int(parameters[0]);
  const int k = parse!int(parameters[1]);
  for (int i = 0; i < lines; i++){
    string newLine = "";
    const string line = file.readln();
    char[] words = line.dup;
    for (int j = 0; j < words.length; j++){
      if (words[j]=='\n' ){
        newLine ~= " ";
      }
      else{
        if (words[j]==' ' ){
          newLine ~= " ";
        }
        else if (words[j]=='.'){
          newLine ~= to!string(words[j]);
        }
        else if (words[j]-k < 65){
          const int a = words[j]-k;
          const int b = 65 - a;
          const int value = 91 - b;
          const char letter = to!char(value);
          newLine ~= to!string(letter);
        }
        else{
          const int value = words[j] - k;
          const char letter = to!char(value);
          newLine ~= to!string(letter);
        }
      }
    }
    answers ~= newLine;
  }
  write(answers);
}

/*
dmd -run pperez7.d
THE SECRET OF HEATHER ALE WHO WANTS TO LIVE FOREVER TO US IN OLDEN STORIES.
A NIGHT AT THE OPERA FOUR SCORE
AND SEVEN YEARS AGO IN ANCIENT PERSIA THERE WAS A KING.
THAT ALL MEN ARE CREATED EQUAL.
AND FORGIVE US OUR DEBTS.
GREENFIELDS ARE GONE NOW.
A DAY AT THE RACES.
*/
