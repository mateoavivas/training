; $ lein check
;   Compiling namespace jdiegoc.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting jdiegoc.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns jdiegoc.clj
  (:gen-class)
)

(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
          p  (get c 0)
          b  (get c 1)
          (loop [i 0]
          (if (p % 2 == 0)
             p = p/2
             b = b+1
             (ptrint(b[i]))
            ))
          (loop [j 0]
          (if(p % 2 ~= 0 )
             p = 3*p+1
             b = b+1
             (ptrint(b[j]))
            ))
        )
      )
    )
  )
)

(defn -main [& args]
  (process_file "DATA.lst")
)

; $clojure jdiegoc.clj
; 106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
