/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;

void main(){
  File file = File("DATA.lst", "r");
  string input = file.readln();
  string [] parameters = input.split(" ");
  const int count = parse!int(parameters[0]);
  string[] answers = new string[](count);
  string readcards = file.readln();
  string [] inputcards = readcards.split(" ");
  int[] cards = new int[](inputcards.length);

  string[] suits = ["Clubs", "Spades", "Diamonds", "Hearts"];
  string[] ranks1 = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack"];
  string[] ranks2 = ["Queen", "King", "Ace"];
  string[] ranks = ranks1~ranks2;

  for(int i = 0; i < count; i++){
    cards[i] = parse!int(inputcards[i]);
    const int suit_value = cards[i] / 13;
    const int rank_value = cards[i] % 13;
    answers[i] = ranks[rank_value] ~ "-of-" ~ suits[suit_value] ;
  }
  writefln("%-(%s %)", answers[]);
}

/*
dmd -run pperez7.d
King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
*/
