--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local result = {}
local secret
local i = -1
for line in data do
    if i == -1 then
        local a = 0
        for token in string.gmatch(line, "[^%s]+") do
            if a == 0 then
                secret = tonumber(token)
                a = 1
            end
        end
        i = i + 1
    else
        for token in string.gmatch(line, "[^%s]+") do
            local correct = 0
            local correct2  = 0
            local a = 0
            for t in tostring(secret):gmatch"." do
                local b = 0
                for s in tostring(token):gmatch"." do
                    if t==s  then
                        if a==b then
                            correct = correct + 1
                        else
                            correct2 = correct2 + 1
                        end
                    end
                    b = b +1
                end
                a = a +1
            end
            i = i + 1
            result[i] = correct .. "-" .. correct2
            print(result[i])
        end
    end
end

--[[
$ lua mrsossa.lua
1-1 0-3 0-2 0-2 1-2 0-2 1-0 0-1 1-1 1-0 1-2 0-2 2-0 0-2 0-3
]]
