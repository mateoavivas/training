/*
$ tslint neds.ts
$ tsc neds.ts
*/

import * as fs from "fs";

const filePath = "DATA.lst";

function bullCow(value: string, guesses: string[]) {
  let out: string = "";
  let ch: string;
  let bulls: number;
  let cows: number;
  let n: number;
  for (const x of guesses) {
    bulls = 0;
    cows = 0;
    for (let i = 0; i < 4; i++) {
      ch = x[i];
      n = value.indexOf(ch);
      if (n !== -1) {
        n === i ? bulls++ : cows++;
      }
    }
    out += bulls.toString() + "-" + cows.toString() + " ";
  }
  console.log(out);
}

function readData(contents: string) {
  const data: string[] = [];
  const lines: string[] = contents.split("\n");
  lines.map((line) => {
    const element: string[] = line.split(" ");
    element.map((el) => data.push(el));
});
  const secretValue = data[0];
  const guesses = data.slice(2, data.length - 1);
  bullCow(secretValue, guesses);
}

function main() {
  return fs.readFile(filePath, "utf8", (readerr, contents) => {
    readData(contents);
  });
}

main();

/*
$ node neds
0-2 1-2 0-1 1-1 1-1 0-2 0-1 1-0 0-1 0-1
*/
