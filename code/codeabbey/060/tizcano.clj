(comment "
$ lein eastwood
== Eastwood 0.2.8 Clojure 1.8.0 JVM 10.0.2
Directories scanned for source files:
  src
== Linting tizcano.core ==
== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0")

(ns tizcano.core
  (:gen-class)
  (:require [clojure.string :as str]))

(defn maximize-candies
  [v]
  (if (< (count v) 3)
    (first v)
    (if (>= (count v) 4)
      (max (+ (first v) (maximize-candies (subvec v 3)))
           (+ (first v) (maximize-candies (subvec v 2))))
      (+ (first v) (maximize-candies (subvec v 2))))))

(defn read-inputs
  [^long numbers]
  (print (maximize-candies (mapv read-string (str/split (read-line) #" "))) "")
  (when (> numbers 1)
    (read-inputs (- numbers 1))))

(defn -main
  [& args]
  (read-inputs (Long/parseLong (read-line))))

(comment "
  $ cat DATA.lst | lein run
output:
213 195 112 214 170 170 188 225 160 167 186 126 115 147 140 155 200 156")
