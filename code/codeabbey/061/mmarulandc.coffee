###
$coffeelint mmarulandc.coffee
###

fs = require 'fs'
config = 'DATA.lst'

readedFile = ->
  fs.readFileSync config, 'utf8'

isPrime = (number)->
  count = 0
  flag = 0
  limit = Math.sqrt number
  for i in [2..limit]
    if number % i == 0
      flag = 1
      break
  if flag == 0
    return true
  else
    return false

generatePrimes = ->
  primes = []
  primes.push 2
  for i in [3..2850131]
    if(isPrime(i) == true)
      primes.push i
  return primes


process = ->
  result = ''
  reader = readedFile().split '\n'
  numbers = reader[1].split ' '
  primes = generatePrimes()
  for number in numbers
    number = parseInt number
    result = result + primes[number-1] + ' '
  console.log result
process()

###
$coffee mmarulandc.coffee
1521991 2635883 1340153 1648253 2043817 2386247
1986889 1540073 2544229 1940201 2464349 1724861
###
