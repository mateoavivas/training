 #load "str.cma";;
       #load "nums.cma";;
let primo n =
    let n = max n (-n) in
    let rec divisor d =
      d * d > n || (n mod d <> 0 && divisor (d+1)) in
    divisor 2

  let rec cal_prim a b =
    if a > b then [] else
      let rest = cal_prim (a + 1) b in
      if primo a then a :: rest else rest;;


let dataArray = Array.make 2 (-1);;

  print_endline "\n\t Cuantos Casos desea Verificar? : ";;
   let () = Scanf.scanf "%d" (fun can ->
    for i =0 to can do let _ = print_string "\n" in
        let data = read_line () in
        let intList = List.map int_of_string(Str.split (Str.regexp " ") data) in

    let tmno = Big_int.big_int_of_int (List.length (cal_prim dataArray.(0) dataArray.(1))) in let _ =
           Printf.printf "%s" (Big_int.string_of_big_int tmno) in
        (for i=0 to ((Array.length dataArray) -1) do
            dataArray.(i) <- (List.nth intList i);


        done)





done)
