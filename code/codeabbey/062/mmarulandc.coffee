###
$coffeelint mmarulandc.coffee
###

fs = require 'fs'
config = 'DATA.lst'

readedFile = ->
  fs.readFileSync config, 'utf8'

isPrime = (number)->
  count = 0
  flag = 0
  limit = Math.sqrt number
  if number == 2
    return true
  for i in [2..limit]
    if number % i == 0
      flag = 1
      break
  if flag == 0
    return true
  else
    return false

process = ->
  solution = ""
  count = 0
  reader = readedFile().split '\n'
  reader.shift()

  # primes = generatePrimes()
  for number,index in reader
    values = reader[index].split " "
    fst = parseInt(values[0])
    snd = parseInt(values[1])
    for i in [fst..snd]
      if isPrime(i)
        count = count + 1
    solution = solution + count + " "
    count = 0
  console.log solution.substr(0,solution.length-2)
process()

###
$coffee mmarulandc.coffee
83240 8887 14177 7452 51121 4484 11804 31344 2119 60923 17235 27382 11202
38837 28516 43885 25869 5028 49326 9581 32738 3878 16131 535 54619 3369 5562
###
