'''
$ pylint mrivera3.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.82/10, +0.18)
'''
from itertools import groupby


def mazepath():
    "finding the maze's shortest path"
    data = [line.rstrip('\n') for line in open("DATA.lst")]
    inputs = data[0].split(sep=" ")
    del data[0]
    initial_points = [[0, int(inputs[0]) - 1], [int(inputs[1]) - 1, 0],
                      [int(inputs[1]) - 1, int(inputs[0]) - 1]]
    ans = []
    for k in initial_points:
        start = k
        prev = [list(x) for x in data]
        index = [[start[0], start[1]]]
        i = 0
        prev[start[0]][start[1]] = 1
        while index[i] != [0, 0]:
            # down
            if (index[i][0] + 1) <= int(inputs[1]) - 1 and \
                    prev[index[i][0] + 1][index[i][1]] == "1":
                prev[index[i][0] + 1][index[i][1]] = \
                    int(prev[index[i][0]][index[i][1]]) + 1
                index.append([index[i][0] + 1, index[i][1]])
            # left
            if (index[i][1] - 1) >= 0 and \
                    prev[index[i][0]][index[i][1] - 1] == "1":
                prev[index[i][0]][index[i][1] - 1] = \
                    int(prev[index[i][0]][index[i][1]]) + 1
                index.append([index[i][0], index[i][1] - 1])
            # right
            if (index[i][1] + 1) <= int(inputs[0]) - 1 and \
                    prev[index[i][0]][index[i][1] + 1] == "1":
                prev[index[i][0]][index[i][1] + 1] = \
                    int(prev[index[i][0]][index[i][1]]) + 1
                index.append([index[i][0], index[i][1] + 1])
            # up
            if (index[i][0] - 1) >= 0 and \
                    prev[index[i][0] - 1][index[i][1]] == "1":
                prev[index[i][0] - 1][index[i][1]] = \
                    int(prev[index[i][0]][index[i][1]]) + 1
                index.append([index[i][0] - 1, index[i][1]])
            i += 1
        goal = prev[0][0]
        target = [0, 0]
        res = []
        j = 0
        while j < prev[0][0]:
            # up
            if (target[0] + 1) <= int(inputs[1]) - 1 and \
                    prev[target[0] + 1][target[1]] == goal - 1:
                res.append("U")
                target[0] = target[0] + 1
                goal = prev[target[0]][target[1]]
            # right
            if (target[1] - 1) >= 0 and \
                    prev[target[0]][target[1] - 1] == goal - 1:
                res.append("R")
                target[1] = target[1] - 1
                goal = prev[target[0]][target[1]]
            # left
            if (target[1] + 1) <= int(inputs[0]) - 1 and \
                    prev[target[0]][target[1] + 1] == goal - 1:
                res.append("L")
                target[1] = target[1] + 1
                goal = prev[target[0]][target[1]]
            # down
            if (target[0] - 1) >= 0 and \
                    prev[target[0] - 1][target[1]] == goal - 1:
                res.append("D")
                target[0] = target[0] - 1
                goal = prev[target[0]][target[1]]
            j += 1
        res = res[::-1]
        dire = [group[0] for group in groupby(res)]
        ocur = [len(list(group)) for key, group in groupby(res)]
        res = ''.join([str(elem) for elem in
                       [str(p) + q for p, q in zip(ocur, dire)]])
        ans.append(res)
    return ans


mazepath()

# $ python3 mrivera3.py
# 4D4L2D4R4D14L8U2L2D10L2D2L2U2L4U4L
# 2R2U6R2U2R4U4R2D6R2U2L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
# U4L6U12L2U2L4U4R6U2L2D10L2D2L2U2L4U4L
