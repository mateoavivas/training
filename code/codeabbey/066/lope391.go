/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build417633694
*/

package main

import (
  "bufio"
  "fmt"
  "io"
  "math"
  "os"
  "path/filepath"
  "strconv"
  "strings"
)

var engFreqs = []float64{
  8.1, 1.5, 2.8, 4.3, 13.0, 2.2, 2.0, 6.1, 7.0, 0.15, 0.77, 7.0, 2.4,
  6.8, 7.5, 1.9, 0.095, 6.0, 6.3, 9.1, 2.8, 0.98, 2.4, 0.15, 2.0, 0.074,
}

func main() {

  absPath, _ := filepath.Abs("DATA.lst")
  arr, _ := readFileToStr(absPath)
  n, _ := strconv.Atoi(arr[0])
  arr = arr[1:]
  out := ""
  for i := 0; i < n; i++ {
    solStep := 0
    minFreqAv := 9999.00
    for j := 1; j < 26; j++ {
      tmpStr := crackCaesar(arr[i], j)
      tmpFreqAv := calcFreqAv(tmpStr)
      if tmpFreqAv < minFreqAv {
        minFreqAv = tmpFreqAv
        solStep = j
      }
    }
    solStr := strings.Split(crackCaesar(arr[i], solStep), " ")

    out += solStr[0] + " " + solStr[1] + " " +
      solStr[2] + " " + strconv.Itoa(solStep) + " "
  }
  fmt.Println(out)
}

func calcFreqAv(input string) (freqAv float64) {

  cont := len(input)
  var freqs [26]float64
  for i := 0; i < len(input); i++ {
    c := int(input[i])

    if c == 32 {
      cont--
      continue
    }

    pos := c % 65
    freqs[pos] += 1.0

  }

  freqAv = 0.0
  //Frequency difference between natural english frequency and coded message
  for i := 0; i < 26; i++ {
    cFreq := (freqs[i] / float64(cont)) * 100
    freqDif := cFreq - engFreqs[i]
    freqs[i] = freqDif
    freqAv += math.Pow(freqDif, 2)
  }

  //Returns single value of added frequencies
  return freqAv
}

func crackCaesar(input string, step int) (output string) {
  // fmt.Println(input)
  output = ""

  for i := 0; i < len(input); i++ {
    c := int(input[i])

    //Ignore spaces
    if c == 32 {
      output += " "
      continue
    }

    //Loop backwards all capital letters
    c -= step
    if c < 65 {
      c = 90 - (64 - c)
    }

    output += string(c)
  }

  return output
}

func readFileToStr(fn string) (arr []string, err error) {

  //Save file to buffer and close
  file, err := os.Open(fn)
  defer file.Close()

  if err != nil {
    return nil, err
  }

  //Cread reader buffer from file
  reader := bufio.NewReader(file)

  //Start reading buffer
  var line string
  for {
    line, err = reader.ReadString('\n')
    //Delete \n from last character of string
    if last := len(line) - 1; last >= 0 && line[last] == '\n' {
      line = line[:last]
    }
    if line != "" {
      arr = append(arr, line)
    }
    if err != nil {
      break
    }
  }

  if err != io.EOF {
    fmt.Printf("ERROR: %v\n", err)
  }

  return arr, nil
}

/*
./lope391
POETRY IS WHAT 5 CARTHAGE MUST BE 22 NO SOONER SPOKEN 21 IT IS BLACK 6 \
THAT ALL MEN 18
*/
