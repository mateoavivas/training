; $ lein eastwood
;== Eastwood 0.3.5 Clojure 1.10.0 JVM 11.0.4 ==
;Directories scanned for source files:
;
;src test
;== Linting adrianfcn.core ==
;== Linting adrianfcn.core-test ==
;== Linting done in 1180 ms ==
;== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
;

(ns adrianfcn.core
  (:gen-class)
  (:require [clojure.math.numeric-tower :as math])
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as input]))

(defn s2c [l]
  (into [] (map #(BigInteger. %) (str/split l #" ")))
)

(def list_fib (map first (iterate (fn [[a b]] [b (+' a b)]) [0 1])))
(def arr (into [] (take 2001 list_fib)))
(defn sol [c]
    (let [b (.indexOf arr c)]
      (if (> b -1)(print b))
    )
)

(defn read_file
  ([file]
    (with-open [rdr (input/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c (s2c line)
              s (count c)
              n (get c 0)]
          (if (= s 1)
            (print (str (sol n) " "))
          )
        )
      )
    )
    (println)
  )
)

(defn -main [& args]
  (read_file "DATA.lst")
)

; $ lein run
; 977 442 846 557 800 683 943 360 630 385 702 322 549 693
