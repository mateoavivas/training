/*
$ rustup run nightly cargo clippy
Compiling jdiegoc.rs
$ rustc jdiegoc.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut line: &str = &buff.unwrap();
        let count: usize = input().trim().parse().unwrap();
        for _ in 0..count {
            let mut num: f64 = input().trim().parse().unwrap();
            print!("{:?} ", fibonacci(num));
        }
    }
    fn fibonacci(num: f64) -> usize {
    let phi = (1.0 + 5f64.sqrt()) / 2.0;
    let index = ((num * 5f64.sqrt() + 0.5).log(phi)).ceil();
    index as usize
}
fn input() -> String {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("Error");
    input
}


/*
 $ rust jdiegoc.rs
 977 442 846 557 800
 683 943 360 630 385
 702 322 549 693
*/
