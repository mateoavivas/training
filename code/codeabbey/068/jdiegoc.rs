/*
$ rustup run nightly cargo clippy
Compiling jdiegoc.rs
$ rustc jdiegoc.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut line: &str = &buff.unwrap();
        let mut array: Vec<&str> = line.split(' ').collect();
        let mut numbers: Vec<i64> = Vec::new();

        for x in &array {
            let mut string_number: String = x.to_string();
            let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
            numbers.push(number_conver);
            let s=0;
            let a=0;
            let b=0;
            let s=0;
            s=((s*a)/(a+b));
            print!(s);
        }
    }
}

/*
 $ rust jdiegoc.rs
 106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
 6.681818181818182 27.925925925925924 5.75 64.42307692307692 18.5625
 70.10714285714286 11.063829787234043 47.51282051282051
 39.76744186046512 9.25925925925926 32.869565217391305 152.4193548387097
 163.95555555555555 5.28125 298.2 40.285714285714285 5.7272727272727275
 45.68421052631579 15.545454545454547 26.666666666666668
 41.26315789473684 12 8.307692307692307 26.511627906976745
 12.23404255319149 35.18518518518518 4.333333333333333 117.5
*/
