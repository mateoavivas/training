# $ lintr::lint("mrivera3.r")


meetpoint <- function() {
"finding the bicyclist meet point"
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    sample <- data[i, ]
    s <- as.integer(sample[1])
    a <- as.integer(sample[2])
    b <- as.integer(sample[3])
    mpoint <- s / (a + b) * a
    results <- c(results, mpoint)
  }
  return(results)
}
meetpoint()

# $ Rscript mrivera3.R
#6.681818  27.925926   5.750000  64.423077  18.562500  70.107143  11.063830
#47.512821  39.767442   9.259259  32.869565 152.419355 163.955556   5.281250
#298.200000  40.285714   5.727273  45.684211  15.545455  26.666667  41.263158
#12.000000   8.307692  26.511628  12.234043  35.185185   4.333333 117.500000
