;; $ lein eastwood
;; == Eastwood 0.3.5 Clojure 1.10.0 JVM 11.0.4 ==
;; Directories scanned for source files:
;; src test
;; == Linting neds.core ==
;; == Linting done in 2657 ms ==
;; == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns neds.core
(:gen-class))

(use '[clojure.string :only (split)])
(use '[clojure.java.io :only (reader)])

(def filePath "src/DATA.lst")

(def ^:dynamic len 0)
(def ^:dynamic info "")
(def ^:dynamic distance 0)
(def ^:dynamic vel_a 0)
(def ^:dynamic vel_b 0)
(def ^:dynamic indicate 0)

(defn race [dat]
  (let [
    distance (float (read-string (get dat 0)))
    vel_a (float (read-string (get dat 1)))
    vel_b (float (read-string (get dat 2)))]
    (print (/ (* distance vel_a) (+ vel_a vel_b)))
  )
  (print " ")
)

(defn split_data [line]
  (let [info(split line #" ")]
    (let [len(count info)]
      (if(> len 1) (do(race info)))
    )
  )
)

(defn read_data [path]
  (with-open [readr (reader path)]
    (doseq [line (line-seq readr)]
      (split_data line)
    )
  )
)

(defn -main
  [& args]
  (read_data filePath)
)

;; $ lein run
;; 6.681818181818182 27.925925925925927 5.75 64.42307692307692
;; 18.5625 70.10714285714286 11.063829787234043 47.51282051282051
;; 39.76744186046512 9.25925925925926 32.869565217391305
;; 152.41935483870967 163.95555555555555 5.28125 298.2
;; 40.285714285714285 5.7272727272727275 45.68421052631579
;; 15.545454545454545 26.666666666666668 41.26315789473684 12
;; 8.307692307692308 26.511627906976745 12.23404255319149
;; 35.18518518518518 4.333333333333333 117.5
