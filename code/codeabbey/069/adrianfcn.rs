/*
$ rustfmt adrianfcn.rs
$ cargo test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
extern crate num_bigint;
extern crate num_traits;

use num_bigint::BigUint;
use num_traits::{One, Zero};
use std::fs::File;
use std::io::prelude::*;
use std::mem::replace;

fn fibonacci(n: usize) {
    let mut f0: BigUint = Zero::zero();
    let number_zero: BigUint = Zero::zero();
    let mut f1: BigUint = One::one();
    let nc: BigUint = number_zero + &n;
    let mut b: BigUint = One::one();
    let mut i = 0;
    while b != Zero::zero() {
        i += 1;
        let f2 = f0 + &f1;
        f0 = replace(&mut f1, f2);
        b = &f0 % &nc;
        if (&f0 % &nc) == Zero::zero() {
            print!("{} ", i);
            break;
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("DATA.lst")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines = contents.lines();
    lines.next();
    for l in lines {
        for n in l.split_whitespace() {
            fibonacci(n.parse::<usize>().unwrap());
        }
    }
    println!();
    Ok(())
}

/*
  $ ./adrianfcn
  102 396 7728 1914 990 840 750 900 2700 8700 1592 72 552 807 1000 2688
  270 2100 3738 300
*/
