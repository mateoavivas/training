/*
 $ eslint jdiegoc.js
*/
/* global print */

import * as file from 'file';

const dat = file.readFile('DATA.lst').toString().split('\n');
for (let i = 0; i < dat.length - 1; i++) {
  if (i !== 0) {
    const token = dat[i].split(' ');
    const nnn = (Number(token[0]));
    const xxx = (Number(token[1]));
    for (let jaa = 0; jaa < nnn; jaa++) {
      const fibs = [ 0, 1 ];
      for (let cas = 0; cas < nnn.toString().length; cas++) {
        const fff = fibs[fibs.length - 1] + fibs[fibs.length - 2];
        fibs.push(fff % xxx[jaa]);
        if (fibs[fibs.length - 1] === 0) {
          print(fibs.length - 1);
          break;
        }
      }
    }
  }
}

/*
$ js jdiegoc.js
 102 396 7728 1914 990 840 750 900 2700 8700 1592 72 552 807 1000 2688
 270 2100 3738 300
*/
