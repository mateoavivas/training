#!/usr/bin/env ruby
# $ ruby -wc jicardona.rb
# Syntax OK

total = 0
testCases = []

File.open('DATA.lst') do |file|
  total = file.readline
  testCases = file.readline.split
end

solution = []

testCases.each do |testCase|
  old = act = nxt= 1
  count = 0
  while nxt % testCase.to_i != 0
    old, act = act, nxt
    nxt = old + act
    count += 1
  end
  solution.push(count + 2)
end

puts solution.join(' ')

# ruby jicardona.rb
# 102  396 7728 1914 990  840  750 900  2700 8700
# 1592 72  552  807  1000 2688 270 2100 3738 300
