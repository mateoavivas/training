#!/usr/bin/env python3
'''
$ pylint jarh1992.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''


def getidxfib(mod):
    '''
    return index of a fib[index] % mod == 0
    '''
    fib1, fib2, fib3 = 0, 1, 0
    idx = 2
    while True:
        fib3 = (fib1 + fib2) % mod
        if 0 in (fib3 % mod,):
            return str(idx)
        fib1 = fib2
        fib2 = fib3
        idx += 1


if __name__ == '__main__':
    N_CASES = int(input())
    CASES = list(map(int, input().split(" ")))
    RSP = [getidxfib(c) for c in CASES]
    print " ".join(RSP)

# $ python 71.py
# 19
# 449825 940999 891051 674588 241652 1049193 1024240 857743 408165 641261
#        349920 1015891 982578 291607 657942 374884 508055 458138 732856
# 71100 123000 1400 28290 60414 233172 3480 857744 54420 320630 29160
#        1015890 118608 73308 55836 12654 101610 172533 68100
