/*
Problem 72: Funny Words Generator
With JavaScript - Node.js
Linting: eslint develalopez.js

--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

const CONSONANTS = 'bcdfghjklmnprstvwxz';
const VOWELS = 'aeiou';
const MULTIPLIER = 445;
const ADDER = 700001;
const MODULO = 2097152;
const CON_FILTER = 19;
const VOW_FILTER = 5;

let seed = 0;

function congruentialGenerator(length, previous) {
  seed = ((MULTIPLIER * seed) + ADDER) % MODULO;
  previous.push(seed);
  return length === 1 ? previous : congruentialGenerator(length - 1, previous);
}

function charParser(numbers) {
  const chars = numbers.map((gen, index) => {
    if (index % 2 === 0) {
      return CONSONANTS.charAt(gen % CON_FILTER);
    }
    return VOWELS.charAt(gen % VOW_FILTER);
  });
  return chars;
}

function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const dataLines = contents.split('\n');
  const [ firstLine, secondLine ] = dataLines;
  [ , seed ] = firstLine.split(' ').map(Number);
  const lengths = secondLine.split(' ').map(Number);
  const generatedNumbers = lengths.map((length) =>
    congruentialGenerator(length, []));
  const words = generatedNumbers.map((sequence) =>
    charParser(sequence));
  const output = words.map((word) =>
    process.stdout.write(`${ word.join('') } `));
  return output;
}

const filesRe = require('fs');
function main() {
  return filesRe.readFile('DATA.lst', 'utf-8', (erro, contents) =>
    dataProcess(erro, contents));
}

main();

/*
$ node develalopez.js
hoj ladog xopim mipu direco sizucowo cebagebe kezuv jep cocov misu lovasaf
pebolo fici teb vimok bejis dipanat hasekad cunig kukipada hehaho
*/
