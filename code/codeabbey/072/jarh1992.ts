/*
$ tslint --init
$ tslint jarh1992.ts
*/

import * as fs from "fs";

function main() {
  const file = fs.readFileSync("DATA.lst", "utf8");
  const inp = file.split("\n");
  const i1 = inp[0].split(" ");
  const inp1 = i1.map((item) => parseInt(item, 10));
  const i2 = inp[1].split(" ");
  const inp2 = i2.map((item) => parseInt(item, 10));
  const con = "bcdfghjklmnprstvwxz";
  const vow = "aeiou";
  const aa = 445;
  const cc = 700001;
  const mm = 2097152;
  let xn = inp1[1];
  let li = 0;
  const rsp: string[] = [];
  let wl: number;
  let i: number;
  for (wl of inp2) {
    let word = "";
    for (i = 1; i <= wl; i++) {
      xn = (aa * xn + cc) % mm;
      if (i % 2 === 0) {
        li = xn % 5;
        word = word.concat(vow.charAt(li));
      }
      else {
        li = xn % 19;
        word = word.concat(con.charAt(li));
      }
    }
    rsp.push(word);
  }
  console.log(rsp.join(" "));
}

main();

/*
$ tsc jarh1992.ts
$ node jarh1992.js
hoj ladog xopim mipu direco sizucowo cebagebe kezuv jep cocov misu lovasaf
pebolo fici teb vimok bejis dipanat hasekad cunig kukipada hehaho
*/
