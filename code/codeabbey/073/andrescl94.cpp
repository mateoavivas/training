#include <iostream>
#include <string.h>
#include <math.h>
#include <iomanip>

#define Ri 0.5
#define Y 0.86602540

using namespace std;

void read_vals(int num,char *moves){
    int i;
    cin.ignore();
    for(i=0;i<num;i++){
        int offset=100*i;
        cin.getline(moves+offset,100);
    }
}

void find_distance(int num, char *moves){
    int i; cout<<fixed<<setprecision(8);
    for(i=0;i<num;i++){
        int j=0, offset=100*i;
        float pos[2]={0.0,0.0};
        while(*(moves+offset+j)!='\0'){
            switch(*(moves+offset+j)){
                case 'A': pos[0]+=1.0;
                          break;
                case 'B': pos[0]+=Ri;
                          pos[1]+=Y;
                          break;
                case 'C': pos[0]-=Ri;
                          pos[1]+=Y;
                          break;
                case 'D': pos[0]-=1.0;
                          break;
                case 'E': pos[0]-=Ri;
                          pos[1]-=Y;
                          break;
                case 'F': pos[0]+=Ri;
                          pos[1]-=Y;
                          break;
            }
            j++;
        }    
        cout<<sqrt(pow(pos[0],2)+pow(pos[1],2))<<' ';
    }    
}

int main(void){
    int num;
    cin>>num;
    char moves[num][100]; fill(moves[0],moves[0]+num*100,'0');
    read_vals(num,&moves[0][0]);
    find_distance(num,&moves[0][0]);
    return 0;
}
