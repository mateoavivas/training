; $ lein eastwood
;Directories scanned for source files:
;
;src test
;== Linting adrianfcn.core ==
;== Linting adrianfcn.core-test ==
;== Linting done in 1144 ms ==
;== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
;

(ns adrianfcn.core
  (:gen-class)
  (:require [clojure.math.numeric-tower :as math])
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as input]))

(defn s2c [l]
  (into [] (map #(Integer/parseInt %) (str/split l #" ")))
)

(defn solution [pa pb]
  (let [a (float (/ pa 100)) b (float (/ pb 100))
  c (- (+ a b) (* a b))]
    (print (math/round (* (/ a c) 100)))
  )
)

(defn read_file
  ([file]
    (with-open [rdr (input/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
              s  (count c)
              pa  (get c 0)
              pb  (get c 1)]
          (if (= s 2)
            (print (str (solution pa pb) " "))
          )
        )
      )
    )
    (println)
  )
)

(defn -main [& args]
  (read_file "DATA.lst")
)

(-main)
; $ clojure adrianfcn.clj
; 48 46 94 72 89 72 71 73 58 50 55 52 87 85 76 96 66 88 35 93 82 61 74 40
