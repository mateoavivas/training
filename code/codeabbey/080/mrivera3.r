# $ lintr::lint('mrivera3.r')


probwin <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    input <- data[i, ]
    pa <- as.integer(input[1]) / 100
    pb <- as.integer(input[2]) / 100
    pwin <- round(100 * (pa / (1 - (1 - pa) * (1 - pb))))
    results <- c(results, pwin)
  }
  return(results)
}
probwin()

# $ Rscript mrivera3.R
#48 46 94 72 89 72 71 73 58 50 55 52 87 85 76 96 66 88 35 93 82 61 74 40
