#load "str.cma";;
(*Referencia de  la Matriz de un ejercicio en internet*)
let tipo a b c = min a (min b c)

let compdatos pal1 pal2 =
  let tam1 = String.length pal1 and tam2 = String.length pal2 in
    let matr = Array.make_matrix (tam1+1) (tam2+1) 0 in
  for i = 0 to tam1 do
    matr.(i).(0) <- i done;
  for j = 0 to tam2 do
    matr.(0).(j) <- j done;
  for j = 1 to tam2 do
    for i = 1 to tam1 do
      if pal1.[i-1] = pal2.[j-1]
      then
        matr.(i).(j) <- matr.(i-1).(j-1)
      else
        matr.(i).(j) <- tipo
                       (matr.(i-1).(j) + 1)
                       (matr.(i).(j-1) + 1)
                       (matr.(i-1).(j-1) + 1)
    done;
    done;
  matr.(tam1).(tam2)
;;
print_endline "Ingrese La Cantidad de Casos a Verificar: ";
print_string "Ingrese dos palabras separadas por un espacio...\n"
let () = Scanf.scanf " %d " (fun can ->
for i =0 to can do
  let data = read_line() in
  let insList = (Str.split (Str.regexp " ") data) in
  let dataArray = Array.make 2 (" ") in Printf.printf "" ;
  (for i=0 to ((Array.length dataArray) -1) do
   dataArray.(i) <- (List.nth insList i)
  done); let pal1 = dataArray.(0) in let pal2 = dataArray.(1) in let llam = compdatos pal1 pal2 in

   (Printf.printf "%d " llam)
done)

