// $ rustfmt oscardjuribe.rs
// $ rustc oscardjuribe.rs
/*
Code to solve levenshtein distance from codeabbey
*/

use std::io::{self, BufRead};

// return the smaller between 3 numbers
fn min(x: i32, y: i32, z: i32) -> i32 {
  if x < y {
    if x < z {
      return x;
    }
    else {
      return z;
    }
  }
  else if x < z {
    if x < y {
      return x;
    }
    else {
      return y;
    }
  }
  else {
    if y < z {
      return y;
    }
    else {
      return z;
    }
  }
}

// n size of str1 and m size of str2
fn levenshtein(str1: &String, n: i32, str2: &String, m: i32) -> i32 {
  let mut matrix: Vec<Vec<i32>> = Vec::with_capacity(n as usize);

  // fill with zeros the nxm matrix
  for _i in 0..n + 1 {
    let mut row: Vec<i32> = Vec::with_capacity(m as usize);
    for _j in 0..m + 1 {
      row.push(0);
    }
    matrix.push(row);
  }

  // first row, default row, need to add one to insert
  for i in 0..m + 1 {
    matrix[0][i as usize] = i;
  }

  // first col, default col, need to add one to delete
  for i in 0..n + 1 {
    matrix[i as usize][0] = i;
  }

  // iterate over all matrix to calculate all substrings
  for i in 1..n + 1 {
    for j in 1..m + 1 {
      // init cost
      let cost: i32;
      //  if chars are equals, we dont have a cost of substitution
      if str1.chars().nth((i - 1) as usize).unwrap()
        == str2.chars().nth((j - 1) as usize).unwrap() {
        cost = 0;
      }
      else {
        // else cost is 1
        cost = 1;
      }
      // the cost is the minimun between insert[i-1,j],
      // delete [i,j-1] or substitute [i-1,j-1]
      matrix[i as usize][j as usize] = min(
        matrix[(i - 1) as usize][j as usize] + 1,
        matrix[i as usize][(j - 1) as usize] + 1,
        matrix[(i - 1) as usize][(j - 1) as usize] + cost,
      );
    }
  }

  // return the [n][m] value after all substring calculation
  matrix[n as usize][m as usize]
}

fn init_levenshtein(str1: String, str2: String) -> i32 {
  // calculate the lenght of each string
  let str1_size: i32 = str1.chars().count() as i32;
  let str2_size: i32 = str2.chars().count() as i32;

  // call levenshtein distance with strings and sizes
  levenshtein(&str1, str1_size, &str2, str2_size)
}

fn main() {
  // read from stdin
  let stdin = io::stdin();
  // iterator to get values
  let mut iterator = stdin.lock().lines();

  // first value as string, number of test cases
  let _strnumber = iterator.next().unwrap().unwrap();

  // cast _strnumber to i32
  let number: i32 = _strnumber.parse().unwrap();

  // array to save answers
  let mut answers = Vec::new();

  for _i in 0..number {
    // read ith test case
    let _line2 = iterator.next().unwrap().unwrap() as String;
    // split array by space
    let str_array: Vec<&str> = _line2.split_whitespace().collect();
    // cast first position of splitted vector as string
    let str1: String = str_array[0].to_string();
    // cast second position of splitted vector as string
    let str2: String = str_array[1].to_string();
    // call method to init levenshtein and push it into answer vector
    answers.push(init_levenshtein(str1, str2));
  }

  // iterate over answers vector and print each answer
  for answer in &answers {
    print!("{} ", answer);
  }

  // final new line
  println!("");
}

/*
$ cat DATA.lst | ./oscardjuribe.rs
6 12 8 7 7 2 11 6 5 4 8 9 4 7 9 7 7 5 4 5 3 4 5 9 6
*/
