import java.util.Scanner;

class BfckInterpreter {

    public static void main(String ... args) {

        Scanner sc = new Scanner(System.in);
        String[] bfckCode = sc.nextLine().split("");
        String[] inputSplit = sc.nextLine().split(" ");
        
        int memory[] = new int[1024];
        
        int inputData[] = new int[inputSplit.length];
        for (int i = 0; i < inputSplit.length; i++) {
            inputData[i] = Integer.parseInt(inputSplit[i]);
        }
        
        int inputIndex = 0;
        int memPointer = 0;
        int lastOpenBracketPos = 0;

        for (int i = 0; i < bfckCode.length; i++) {

            switch (bfckCode[i]) {

                case ";":
                    memory[memPointer] = inputData[inputIndex];
                    inputIndex++;
                    break;
                case ">":
                    memPointer++;
                    break;
                case "<":
                    memPointer--;
                    break;
                case "[":
                    lastOpenBracketPos = i;
                    if (memory[memPointer] == 0) {
                        for (int j = i; j < bfckCode.length; j++) {
                            if (bfckCode[j].equals("]")) {
                                i = j;
                                break;
                            }
                        }
                    }
                    break;
                case "]":
                    i = lastOpenBracketPos - 1;
                    break;
                case "-":
                    memory[memPointer]--;
                    break;
                case "+":
                    memory[memPointer]++;
                    break;
                case ":":
                    System.out.print(memory[memPointer]);
                    System.out.print(" ");
                    break;
            }
        }
    }
}
