/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

function createNode(current, newNode, dir) {
  if (dir === 'left') {
    current[0] = newNode;
  }
  if (dir === 'right') {
    current[2] = newNode;
  }
  return current;
}

function locate(current, newNode) {
  let pos = 0;
  let dir = '';
  if (newNode[1] < current[1]) {
    pos = 0;
    dir = 'left';
  }
  else if (newNode[1] >= current[1]) {
    pos = 2;
    dir = 'right';
  }
  if (current[pos] === '-') {
    current = createNode(current, newNode, dir);
  }
  else {
    current = current[pos];
    locate(current, newNode);
  }
  return current;
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ numNodes, stringNodes ] = inputData
    .split('\r\n');

  const nodes = stringNodes.split(' ').map((x) => Number(x));
  const current = [ '-', nodes[0], '-' ];
  let count = 1;
  let tree = '';
  while (count < Number(numNodes)) {
    const newNode = [ '-', nodes[count], '-' ];
    locate(current, newNode);
    count += 1;
    tree = JSON.stringify(current);
  }
  tree = tree
    .replace(/[[]/g, '(')
    .replace(/[\]]/g, ')')
    .replace(/"-"/g, '-');
  console.log(tree);
});

/*
$ js ngarci
output:
(((((((-,1,-),2,(-,3,-)),4,((-,5,-),6,(-,7,-))),8,-),
9,(-,10,(-,11,-))),12,((-,13,-),14,((-,15,(-,16,((-,17,-),
18,(((-,19,((-,20,-),21,-)),22,-),23,-)))),24,-))),25,-)
*/
