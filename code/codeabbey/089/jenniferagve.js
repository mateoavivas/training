/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function defNotes() {
/* Initial values and order of the notes scale*/
  const notes = [
    'C',
    'C#',
    'D',
    'D#',
    'E',
    'F',
    'F#',
    'G',
    'G#',
    'A',
    'A#',
    'B',
  ];
  return notes;
}

function reverseCal(firstFreq, notes, notesQuan, initFreq, initOct) {
/* Calculus of the letter and octed for each frquency*/
  const intPosition = 9;
  const negPosition = (Math.log2(firstFreq / initFreq)) * notesQuan;
  if (negPosition < -intPosition) {
    const octNumber = (initOct + Math.round(negPosition / notesQuan));
    const letterIndex = Math.round(negPosition + ((
      initOct - octNumber) * notesQuan)) + intPosition;
    if (letterIndex >= notesQuan) {
      const letterIndexN = letterIndex - notesQuan;
      const octNumberN = octNumber + 1;
      const letter = String(notes[letterIndexN]) + String(octNumberN);
      return letter;
    }
    const letter = String(notes[letterIndex]) + String(octNumber);
    return letter;
  } else if ((negPosition <= intPosition - 2) && (negPosition > initOct - 1)) {
    const octNumber = Math.round(negPosition) + 1;
    const letterIndex = (intPosition - 2 + Math.round(
      negPosition)) - notesQuan - 2;
    const letter = String(notes[letterIndex]) + String(octNumber);
    return letter;
  } else if ((negPosition > 0) && (negPosition <= 2)) {
    const octNumber = initOct;
    const letterIndex = (intPosition - 1 - Math.round(negPosition));
    const letter = String(notes[letterIndex]) + String(octNumber);
    return letter;
  } else if ((negPosition < 0) && (negPosition >= -intPosition)) {
    const octNumber = initOct;
    const letterIndex = (notesQuan - 1 + Math.round(negPosition));
    const letter = String(notes[letterIndex]) + String(octNumber);
    return letter;
  }
  const octNumber = initOct + (Math.round(negPosition / notesQuan));
  const letterIndex = Math.round(negPosition - ((
    octNumber - initOct) * notesQuan)) + intPosition;
  const letter = String(notes[letterIndex]) + String(octNumber);
  return letter;
}

/* eslint array-element-newline: ["error", "consistent"]*/
function noteTotalCal(erro, contents) {
/* Here every function is called to calculate the notes
with the content of the file */
  if (erro) {
    return erro;
  }
  const notes = defNotes();
  const inputFile = contents.split('\n');
  const inputNotes = String(inputFile.slice(1));
  const eachNote = inputNotes.split(' ');
  const notesQuan = 12;
  const initFreq = 440;
  const initOct = 4;
  const freqArr = eachNote.map((element) => reverseCal(element, notes,
    notesQuan, initFreq, initOct));
  const totalFreq = freqArr.join(' ');
  const output = process.stdout.write(`${ totalFreq } \n`);
  return output;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    noteTotalCal(erro, contents));
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input data:22
G#4 F#1 G#2 A#1 E5 A4 A#3 E1 A3 A2 D#5 G#5 B2 A1 F2 D5 F4 C#3 D1 B3 F#2 C#5
answer:
-------------------------------------------------------------------
output:
415 46 104 58 659 440 233 41 220 110 622 831 123 55 87 587 349 139 37 247
92 554
*/
