# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function game2048()
  matrix = Array{Int32}(4,4)
  moves = Array{Char}
  open("DATA.lst") do f
    for i = 1:4
      n1, n2, n3, n4 = split(readline(f))
      n1 = parse(Int32,n1)
      n2 = parse(Int32,n2)
      n3 = parse(Int32,n3)
      n4 = parse(Int32,n4)
      matrix[i,:] = [n1,n2,n3,n4]
    end
    moves = split(readline(f))
  end
  boolArray = BitArray(4)
  for i = 1:size(moves,1)
    if moves[i] == "D"
      boolArray = matrix[4,:] .== matrix[3,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[4,j] = matrix[4,j] + matrix[3,j]
          matrix[3,j] = matrix[2,j]
          matrix[2,j] = matrix[1,j]
          matrix[1,j] = 0
        end
      end
      boolArray = matrix[3,:] .== matrix[2,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[3,j] = matrix[3,j] + matrix[2,j]
          matrix[2,j] = matrix[1,j]
          matrix[1,j] = 0
        end
      end
      boolArray = matrix[2,:] .== matrix[1,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[2,j] = matrix[2,j] + matrix[1,j]
          matrix[1,j] = 0
        end
      end
      boolArray = matrix[2,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[2,j] = matrix[1,j]
          matrix[1,j] = 0
        end
      end
      boolArray = matrix[3,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[3,j] = matrix[2,j]
          matrix[2,j] = matrix[1,j]
          matrix[1,j] = 0
        end
      end
      boolArray = matrix[4,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[4,j] = matrix[3,j]
          matrix[3,j] = matrix[2,j]
          matrix[2,j] = matrix[1,j]
          matrix[1,j] = 0
        end
      end
    elseif moves[i] == "U"
      boolArray = matrix[1,:] .== matrix[2,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[1,j] = matrix[1,j] + matrix[2,j]
          matrix[2,j] = matrix[3,j]
          matrix[3,j] = matrix[4,j]
          matrix[4,j] = 0
        end
      end
      boolArray = matrix[2,:] .== matrix[3,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[2,j] = matrix[2,j] + matrix[3,j]
          matrix[3,j] = matrix[4,j]
          matrix[4,j] = 0
        end
      end
      boolArray = matrix[3,:] .== matrix[4,:]
      for j = 1:4
        if boolArray[j] == true
          matrix[3,j] = matrix[3,j] + matrix[4,j]
          matrix[4,j] = 0
        end
      end
      boolArray = matrix[3,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[3,j] = matrix[4,j]
          matrix[4,j] = 0
        end
      end
      boolArray = matrix[2,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[2,j] = matrix[3,j]
          matrix[3,j] = matrix[4,j]
          matrix[4,j] = 0
        end
      end
      boolArray = matrix[1,:] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[1,j] = matrix[2,j]
          matrix[2,j] = matrix[3,j]
          matrix[3,j] = matrix[4,j]
          matrix[4,j] = 0
        end
      end
    elseif moves[i] == "L"
      boolArray = matrix[:,1] .== matrix[:,2]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,1] = matrix[j,1] + matrix[j,2]
          matrix[j,2] = matrix[j,3]
          matrix[j,3] = matrix[j,4]
          matrix[j,4] = 0
        end
      end
      boolArray = matrix[:,2] .== matrix[:,3]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,2] = matrix[j,2] + matrix[j,3]
          matrix[j,3] = matrix[j,4]
          matrix[j,4] = 0
        end
      end
      boolArray = matrix[:,3] .== matrix[:,4]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,3] = matrix[j,3] + matrix[j,4]
          matrix[j,4] = 0
        end
      end
      boolArray = matrix[:,3] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,3] = matrix[j,4]
          matrix[j,4] = 0
        end
      end
      boolArray = matrix[:,2] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,2] = matrix[j,3]
          matrix[j,3] = matrix[j,4]
          matrix[j,4] = 0
        end
      end
      boolArray = matrix[:,1] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,1] = matrix[j,2]
          matrix[j,2] = matrix[j,3]
          matrix[j,3] = matrix[j,4]
          matrix[j,4] = 0
        end
      end
    else
      boolArray = matrix[:,4] .== matrix[:,3]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,4] = matrix[j,4] + matrix[j,3]
          matrix[j,3] = matrix[j,2]
          matrix[j,2] = matrix[j,1]
          matrix[j,1] = 0
        end
      end
      boolArray = matrix[:,3] .== matrix[:,2]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,3] = matrix[j,3] + matrix[j,2]
          matrix[j,2] = matrix[j,1]
          matrix[j,1] = 0
        end
      end
      boolArray = matrix[:,2] .== matrix[:,1]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,2] = matrix[j,2] + matrix[j,1]
          matrix[j,1] = 0
        end
      end
      boolArray = matrix[:,2] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,2] = matrix[j,1]
          matrix[j,1] = 0
        end
      end
      boolArray = matrix[:,3] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,3] = matrix[j,2]
          matrix[j,2] = matrix[j,1]
          matrix[j,1] = 0
        end
      end
      boolArray = matrix[:,4] .== [0,0,0,0]
      for j = 1:4
        if boolArray[j] == true
          matrix[j,4] = matrix[j,3]
          matrix[j,3] = matrix[j,2]
          matrix[j,2] = matrix[j,1]
          matrix[j,1] = 0
        end
      end
    end
  end
  maxValue = maximum(matrix)
  answer = string(count(p->(1<p<3), matrix))
  for j = 2:log2(maxValue)
    answer = string(answer, " ", count(p->((2^j)-1<p<(2^j)+1), matrix))
  end
  return answer
end

game2048()

# $ include("actiradob.jl")
# "2 4 1 1"
