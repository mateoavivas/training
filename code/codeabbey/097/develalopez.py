'''
Problem #97 Girls and Pigs
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function


def solutions(legs, breasts):
    '''
    Function that returns that amount of possible solutions for the girls and
    pigs problem.
    '''
    sols = 1
    pigs = 2
    girls = (legs - (pigs * 4)) // 2
    g_breasts = girls * 2
    p_breasts = breasts - g_breasts
    while girls >= 2:
        if p_breasts % pigs == 0:
            if (p_breasts // pigs) % 2 == 0:
                sols += 1
        pigs += 1
        girls = (legs - (pigs * 4)) // 2
        g_breasts = girls * 2
        p_breasts = breasts - g_breasts
    return sols


def main():
    '''
    Main fucntion that reads input and prints the solution.
    '''
    d_file = open("DATA.lst", "r")
    cases = int(d_file.readline())
    answers = []
    for _ in range(cases):
        legs, breasts = map(int, [int(x) for x in d_file.readline().split()])
        answers.append(solutions(legs, breasts))
    print(" ".join(str(x) for x in answers))


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
11 12 11 12 3 8 2 3 8 5 11 13 6 3 5
'''
