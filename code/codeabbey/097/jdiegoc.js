/*
 $ eslint jdiegoc.js
*/
/* global print */


import * as file from 'file';

const dat = file.readFile('DATA.lst').toString().split('\n');
for (let i = 0; i < dat.length - 1; i++) {
  if (i !== 0) {
    const input = dat[i].split(' ');
    const numOfTests = input.shift();
    const result = [];
    for (let xxx = 0; xxx < numOfTests * 2; xxx += 2) {
      let legs = input[i];
      const count = 0;
      for (let jjj = 1; jjj < ((legs / 2) - 1); jjj++) {
        jjj++;
        legs++;
      }
      result.push(count);
    }
    print(result.join(' '));
  }
}

/*
 $ js jdiegoc.js
 11 12 11 12 3 8 2 3 8 5 11 13 6 3 6
*/
