#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

const [ input ] = arguments;
const lines = input.split(/\n/);

const three = 3;

const slopes = [];
const shots = [];

for (let slope = 0; slope < three; slope++) {
  slopes[slope] = lines.shift().split(' ');
  shots[slope] = [];
  for (let shot = 0; shot < three; shot++) {
    shots[slope][shot] = lines.shift().split(' ');
  }
}

const solution = [];

const gravity = 9.81;
const degrees = 180;
const cube = 4;

for (const [ index, slope ] of slopes.entries()) {
  for (const shot of shots[index]) {
    const velX = shot[0] * Math.cos(shot[1] * (Math.PI / degrees));
    const velY = shot[0] * Math.sin(shot[1] * (Math.PI / degrees));
    const velXSquare = Math.pow(velX, 2);
    let current = 0;
    for (let x = 0; ; x++) {
      const previous = current;
      const xSquare = Math.pow(x, 2);
      current = ((x * velY) / velX) - ((gravity / 2) * (xSquare / velXSquare));
      if (x !== 0 && current <= slope[parseInt(x / cube, 10)] * cube) {
        if (current < previous &&
          parseInt(previous, 10) === slope[parseInt(x / cube, 10)] * cube) {
          solution.push(x - 1);
          break;
        } else {
          solution.push(x);
          break;
        }
      }
    }
  }
}

print(solution.join(' '));

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
96 68 120 60 88 120 97 68 76
*/
