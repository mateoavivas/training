;; $ lein eastwood
;;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;;   Directories scanned for source files:
;;     src test
;;   == Linting kamadoatfluid.clj ==
;;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

;; namespace
(ns kamadoatfluid.clj
  (:gen-class))

;; load lines of file handled by 'rdr' into a vector of strings
(defn loadf [rdr]
  (vec (line-seq rdr)))

;; tokenize whitespace separated string into a convenient type and container
(defn sintoc [wsstr]
  (vec (map #(Double/parseDouble %) (clojure.string/split wsstr #" "))))

;; angular degrees to radians
(defn to-rad [deg]
  (/ (* deg Math/PI) 180.0))

;; get expected height of the terrain at given position in x-axis
(defn get-ter-y [x terrain]
  (let [index  (int (/ x 4.0))]
    (* 4.0 (get terrain index))))

;; get pos in y-axis for a parabolic shot for given position in x-axis
(defn get-pos-y [x v ad]
  (let [ar   (to-rad ad)
        cosa (Math/cos ar)
        tana (Math/tan ar)]
    (- (* x tana) (/ (* 9.81 (* x x)) (* 2.0 (* (* v v) (* cosa cosa)))))))

;; search for a vertical impact
(defn sver-impact [terrain v a]
  ;; ===========================================================
  ;;               *   *        |
  ;;          *            *  ---
  ;;      *                  *|         <- vertical impact!
  ;;   *                 ------
  ;; * ------------------
  ;; ===========================================================
  (loop [x 1.001]
    (let [y  (get-pos-y x v a)
          eh (get-ter-y x terrain)]
      (if (<= y eh)
        (int x)
        (if (< x 159.9)
          (recur (+ x 0.001))
          -1)))))

;; search for an horizontal impact
(defn shor-impact [terrain v a]
  ;;===========================================================
  ;;               *   *             |
  ;;          *             *      ---
  ;;      *                    *   |    <- horizontal impact!
  ;;   *                 --------*--
  ;; * ------------------
  ;;===========================================================
  (loop [x 1.001]
    (let [y  (get-pos-y x v a)
          eh (get-ter-y x terrain)]
      (if (and (<= eh y) (<= (Math/abs (- eh y)) 0.01))
        (int x)
        (if (< x 159.9)
          (recur (+ x 0.001))
          (sver-impact terrain v a))))))

;; get the x-axis position of the impact between bullet & terrain
(defn get-impact-pos [terrain shot-params]
  (let [v (get shot-params 0)
        a (get shot-params 1)]
    (shor-impact terrain v a)))

;; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (let [f (loadf rdr)]
      ;; three lines of terrain description
      (dotimes [i 3]
        (let [thead   (* 4 i)
              terrain (sintoc (get f thead))]
          ;; three shots per terrain
          (dotimes [j 3]
            (let [shead       (+ (+ thead j) 1)
                  shot-params (sintoc (get f shead))]
              (print (get-impact-pos terrain shot-params) " "))))))))

;; fire it ma'am!
(defn -main [& args]
  (process_file "DATA.lst")
  (println))

;; $lein run
;;  96  68  120  60  88  120  97  68  76
