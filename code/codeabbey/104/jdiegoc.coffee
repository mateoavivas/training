###
$coffelint jdiegoc.coffee
###

file = require 'file'

do ->
  for line in file.readFile("DATA.lst").toString().split '\n'
    token = line.split(" ")
    x1 = token[0]
    y1 = token[1]
    x2 = token[2]
    y2 = token[3]
    x3 = token[4]
    y3 = token[5]
    ab = sqrt((x1 - x2) ^ 2 + (y1 - y2) ^ 2)
    ac = sqrt((x1 - x3) ^ 2 + (y1 - y3) ^ 2)
    bc = sqrt((x2 - x3) ^ 2 + (y2 - y3) ^ 2)
    area = 1 / 4 * sqrt(4 * (bc ^ 2 * ac ^ 2) - (bc ^ 2 + ac ^ 2 - ab ^ 2) ^ 2)
    print(area)

###
 $coffee jdiegoc.coffee
 8008545.5 9597811.000000006 1.1915667000000002e7 1.1450171e7 1.5066084e7
 2427172.5000000005 3265797.0000000047 3.102292e7 492795.0
 5095620.999999998 4526801.999999997 8112862.5 1371712.9999999972
 1036641.0 7015461.5 200912885e7
 ###
