/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const inputs = inputData.toString().split('\r\n');

  for (let i = 1; i <= Number(inputs[0]); i++) {
    const [ X1, Y1, X2, Y2, X3, Y3 ] = inputs[i]
      .toString().split(' ').map((x) => Number(x));
    const lengthOne = Math
      .sqrt(Math.pow(Math.abs(Y2 - Y1), 2) + Math.pow(Math.abs(X2 - X1), 2));
    const lengthTwo = Math
      .sqrt(Math.pow(Math.abs(Y3 - Y1), 2) + Math.pow(Math.abs(X3 - X1), 2));
    const lengthThree = Math
      .sqrt(Math.pow(Math.abs(Y3 - Y2), 2) + Math.pow(Math.abs(X3 - X2), 2));
    const semiPerimeter = (lengthOne + lengthTwo + lengthThree) / 2;
    const area = Math.sqrt(semiPerimeter * (semiPerimeter - lengthOne) *
     (semiPerimeter - lengthTwo) * (semiPerimeter - lengthThree));
    results.push(area.toFixed(1));
  }

  console.log(results.map((x) => Number(x)).join(' '));
});
/*
$ js ngarci12
output:
8008545.5 9597811 11915667 11450171 15066084
2427172.5 3265797 31022920 492795 5095621 4526802
8112862.5 1371713 1036641 7015461.5 20091288.5
*/
