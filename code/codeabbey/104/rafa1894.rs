/*
$ rustfmt rafa1894.rs #linting
$ rustc rafa1894.rs #compilation
*/

use std::env;
use std::fs;

fn calc(v: &[f64]) -> f64 {
    let a1 = f64::powf((v[0] - v[2]) as f64, 2.0);
    let a2 = f64::powf((v[1] - v[3]) as f64, 2.0);
    let a = (a1 + a2).sqrt();
    let b1 = f64::powf((v[0] - v[4]) as f64, 2.0);
    let b2 = f64::powf((v[1] - v[5]) as f64, 2.0);
    let b = (b1 + b2).sqrt();
    let c1 = f64::powf((v[2] - v[4]) as f64, 2.0);
    let c2 = f64::powf((v[3] - v[5]) as f64, 2.0);
    let c = (c1 + c2).sqrt();
    let s = (a + b + c) / 2.0;
    return (s * (s - a) * (s - b) * (s - c)).sqrt();
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args[1];
    let inputstr = fs::read_to_string(file).expect("Error");
    let lines = inputstr.lines();
    let mut vec = vec![0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
    let mut n;
    let mut x = 0;
    let mut y = 0;
    for i in lines {
        if x == 0 {
            x = 1;
            continue;
        }
        let numsts = i.split(' ');
        for j in numsts {
            n = j.parse::<f64>().unwrap();
            vec[y] = n;
            y += 1;
        }
        y = 0;
        print!("{} ", calc(&vec));
    }
}
/*
$ ./rafa1894 DATA.lst
8008545.500000013 9597811.000000004 11915667.000000007 11450170.999999996
 15066083.999999994 2427172.5000000005 3265796.999999956 31022920
 492795.0000000145 5095621.000000001 4526801.999999999
 8112862.499999996 1371713.0000000023 1036640.9999999971
 7015461.499999996 20091288.499999996
*/
