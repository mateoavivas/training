/*
  $ dartanalyzer adrianfcn.dart
Analyzing adrianfcn.dart...
No issues found!
 */
import 'dart:io';
import "dart:convert";

double polygonArea(List points) {
  double valL = 0;
  double valR = 0;
  for (int i = 0; i < points.length - 1; i++) {
    valL +=((points[i][0] * points[i+1][1]));
  }
  valL += (points[points.length - 1][0] * points[0][1]);
  for (int i = 0; i < points.length - 1; i++) {
    valR +=((points[i][1] * points[i+1][0]));
  }
  valR += (points[points.length - 1][1] * points[0][0]);
  return ((valL - valR)/2).abs();
}

void main() {
  final List<String> source = File("DATA.lst")
    .readAsStringSync(encoding: utf8)
    .split("\n");
  List inputs = [];
  for (int i = 1; i < source.length - 1; i++) {
    var points = source[i].split(" ");
    List point = [];
    point.add(int.parse(points[0]));
    point.add(int.parse(points[1]));
    inputs.add(point);
  }
  print(polygonArea(inputs));
}

/*
  $ dart adrianfcn.dart
  62358620.0
*/
