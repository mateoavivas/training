/*
 $ eslint mrsossa.js
*/
/* global print */

import { readFileSync } from 'fs';

const dat = readFileSync('DATA.lst').toString().split('\n');
let sum = 0;
for (let ind = 1; ind < dat.length - 1; ind++) {
  let subI = ind + 1;
  if (ind === dat.length - 2) {
    subI = 1;
  }
  const token = dat[ind].split(' ');
  const tokenn = dat[subI].split(' ');
  sum += ((token[0] * tokenn[1]) - (token[1] * tokenn[0]));
}
const numb = 0.5;
print(numb * sum);

/*
 $ js mrsossa.js
60992863
*/
