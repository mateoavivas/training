/*
$ rustup run nightly cargo clippy
Compiling mrsossa.rs
$ rustc mrsossa.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
  let file = File::open("DATA.lst").unwrap();
  let reader = BufReader::new(file);
  let mut lines = Vec::new();
  for line in reader.lines() {
    let token = line.unwrap();
    lines.push(token);
  }
  let mut i = 1;
  let mut sum:i32 = 0;
  let mut subi;
  while i < lines[0].parse::<i32>().unwrap(){
    subi = i + 1;
    if i == lines.len() as i32 - 1{
      subi = 1;
    }
    let token:Vec<&str> = lines[i as usize].split(" ").collect();
    let tokenn:Vec<&str> = lines[subi as usize].split(" ").collect();
    let n1 = token[0].parse::<i32>().unwrap();
    let n2 = tokenn[1].parse::<i32>().unwrap();
    let n3 = token[1].parse::<i32>().unwrap();
    let n4 = tokenn[0].parse::<i32>().unwrap();
    let suma = n1 * n2;
    let sumb = n3 * n4;
    sum = sum + (suma - sumb);
    i = i + 1;
  }
  println!("{}",0.5 * sum as f32);
}

/*
 $ rustc mrsossa.rs
 $ ./mrsossa
 60992863
*/

