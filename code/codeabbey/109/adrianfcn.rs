/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn lucky_tickets(digit: u64, base: u64) -> Vec<u64> {
    let mut d = digit;
    let mut lucky_tickets = Vec::new();
    let no_even = d % 2 == 1;
    if no_even {
        d -= 1;
    }
    let max_len = (digit / 2) * (base - 1);
    let mut cout = 0;
    if d > 0 {
        let mut tickets = vec![0; (max_len + 1) as usize];
        gen_tickets((digit / 2) - 1, 0, base, &mut tickets);
        for i in tickets {
            cout += i.pow(2) as u64;
        }
        if no_even {
            lucky_tickets.push(cout * base);
        } else {
            lucky_tickets.push(cout);
        }
    } else {
        lucky_tickets.push(base);
    }
    lucky_tickets
}

fn gen_tickets(d: u64, p: u64, sup: u64, tickets: &mut Vec<u64>) {
    if d == 0 {
        for i in 0..sup {
            let new_d = p + i;
            tickets[new_d as usize] += 1;
        }
    } else {
        for i in 0..sup {
            gen_tickets(d - 1, p + i, sup, tickets);
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("DATA.lst")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut inputs = vec![];
    let mut line = contents.lines();
    line.next();
    for l in line {
        for c in l.split_whitespace() {
            inputs.push(c.trim().parse::<u64>().unwrap());
        }
        let _base = inputs[1];
        let _digits = inputs[0];
        let tickets = lucky_tickets(_digits, _base);
        for i in tickets {
            print!("{} ", i);
        }
        inputs.clear();
    }
    Ok(())
}
/*
$ ./adrianfcn.rs
25288120 184756 128912240 344 252 22899818176 58199208 40 8 8092 1231099425
1132138107 44152809
*/
