/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build542504703
*/

package main

import (
  "fmt"
  "os"
  "strconv"
)

func main() {

  if len(os.Args) != 2 {
    fmt.Println("Command format: lope391 [int]")
    return
  }

  v, err := strconv.Atoi(os.Args[1])

  if err != nil {
    fmt.Println("Please enter a valid number")
    return
  }

  res := ""
  half := v / 2
  even := (v%2 == 0)
  //Add half the number to memory position 1
  for i := 1; i <= half; i++ {
    res += "+"
    if i%5 == 0 && i < half && i != 0 {
      res += " "
    }
  }
  //Multiply the number on memory position 1 by 2
  //Save the contents on memory position 2 and point to memory position 2
  res += "[>++<-]>"
  //If numer is even, print, if its odd add one and print
  if !even {
    res += "+:"
  } else {
    res += ":"
  }

  fmt.Println(res)
}

/*
./lope391 85
+++++ +++++ +++++ +++++ +++++ +++++ +++++ +++++ ++[>++<-]>+:
*/
