/*
$ npx eslint fgomezoso.js
*/

/*
This program sorts an array using selection of maximum element.
The output prints out the index of selected maximum at each pass.
*/

const file = require('fs');

function selectionSort(length, arrayOfNumbers) {
  const firstIndex = 0;
  let max = arrayOfNumbers[firstIndex];
  let maxPosition = 0;

  for (let idx = 0; idx < length; idx += 1) {
    if (arrayOfNumbers[idx] > max) {
      max = arrayOfNumbers[idx];
      maxPosition = idx;
    }
  }

  const aux = arrayOfNumbers[length - 1];
  arrayOfNumbers[length - 1] = arrayOfNumbers[maxPosition];
  arrayOfNumbers[maxPosition] = aux;

  return [ arrayOfNumbers, maxPosition ];
}

function readText(content) {
  const textlines = content.split('\n');
  const [ strLength, strNumbers ] = textlines;


  const len = parseInt(strLength, 10);

  const nums = strNumbers.split(' ')
    .map((value) => (parseInt(value, 10)));

  return [ len, nums ];
}

function main(content) {
  const arrayMax = [];
  const limit = 1;

  const [ length, numbers ] = readText(content);

  let [ result, maxPos ] = selectionSort(length, numbers);

  arrayMax.push(maxPos);

  let idx = length - 1;

  while (idx > limit) {
    [ result, maxPos ] = selectionSort(idx, result);
    arrayMax.push(maxPos);
    idx -= 1;
  }

  const maxPosArray = arrayMax.join(' ');

  process.stdout.write(`${ maxPosArray }`);
}

function readFile() {
  file.readFile('DATA.lst', 'utf8', (fatalErr, inputString) => {
    if (fatalErr) {
      throw fatalErr;
    }
    const content = inputString;
    main(content);
  });
}

readFile();

/*
$ node fgomezoso.js
111 119 19 31 39 58 52 51 89 11 85 38 69 92 28 61 32 4 4 12 94 49 92
21 42 74 15 62 79 85 38 68 28 40 77 50 16 36 5 79 22 48 75 2 9 6 5
34 51 14 33 48 27 2 66 4 2 6 55 43 19 27 20 21 18 34 27 31 15 19 39
7 27 13 17 7 32 41 5 40 11 3 25 21 23 12 35 12 10 12 29 11 21 16 17
26 10 4 17 3 20 15 17 1 10 3 8 4 2 10 10 0 5 2 7 0 1 2 4 2 0 1
*/
