/* eslint no-sync: ["error", { allowAtRootLevel: true }]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];
const numberOfEnglishLetters = 26;
const beginning = 65;

let dictionary = files.readFileSync(path.join(__dirname, 'words.lst'), 'utf8');
const inputData = files.readFileSync(path.join(__dirname, 'DATA.lst'), 'utf8');

const [ cases ] = inputData.split('\r\n');
const words = inputData.split('\r\n').slice(1);
dictionary = dictionary.split('\r\n');
const alphabet = [ ...Array(numberOfEnglishLetters).keys() ]
  .map((x, y) => String.fromCharCode(y + beginning))
  .join('')
  .toLowerCase()
  .split('');

const otherLetters = [];

for (let i = 0; i < Number(cases); i++) {
  const lettersOfWord = words[i].split('');
  const filterDictionaryByLength = dictionary
    .filter((x) => x.length === words[i].length);
  const filterDictionaryBycontent = filterDictionaryByLength
    .map((x) => x.split(''))
    .filter((x) => x.includes(...lettersOfWord));
  let correctWords = [];
  let wordsToPush = filterDictionaryBycontent;
  let others = alphabet;
  for (let index = 0; index < lettersOfWord.length; index++) {
    wordsToPush = wordsToPush.filter((x) => x.includes(lettersOfWord[index]));
    others = others.filter((y) => y !== lettersOfWord[index]);
  }

  otherLetters.push(others);
  correctWords.push(wordsToPush);
  correctWords = correctWords
    .map((x) => x.map((y) => y.join('')))
    .flat()
    .filter((x) => x !== words[i]);

  results.push(correctWords);
}

const cleanResult = [];

for (let i = 0; i < results.length; i++) {
  const areNotAnagram = [];
  for (let x = 0; x < results[i].length; x++) {
    const word = results[i][x].split('');
    const lettersOfWord = words[i].split('');
    const others = otherLetters[i];
    if (word.some((y) => others.includes(y))) {
      const isNotAnagram = results[i].slice(x, x + 1);
      areNotAnagram.push(isNotAnagram);
      continue;
    }
    const sumWord = word
      .map((ele) => ele.charCodeAt(0))
      .reduce((accu, current) => accu + current);

    const sumOriginal = lettersOfWord
      .map((ele) => ele.charCodeAt(0))
      .reduce((accu, current) => accu + current);

    if (sumWord !== sumOriginal) {
      const isNotAnagram = results[i].slice(x, x + 1);
      areNotAnagram.push(isNotAnagram);
    }
  }
  const flat = areNotAnagram.flat();
  let newArr = results[i];
  newArr = newArr.filter((x) => !flat.includes(x));
  cleanResult.push(newArr);
}

console.log(cleanResult.map((x) => x.length).join(' '));

/*
$ js ngarci
output:
4 3 3 5 3 3 3
*/
