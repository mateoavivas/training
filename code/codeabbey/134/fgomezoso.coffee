###
> coffeelint fgomezoso.coffee
  ✓ fgomezoso.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###
fs = require 'fs'

trackText = (width,height,length) ->

  startX = 0
  endX = length
  startY = 0
  endY = 1
  W = width
  H = height
  positionX = 0
  positionY = 0
  directionX = "Right"
  directionY = "Down"
  count = 0
  limit = 101

  moveX = (currentDirection) ->

    if currentDirection is "Right"
      startX++
      endX++
    else
      startX--
      endX--

  moveY = (currentDirection) ->

    if currentDirection is "Down"
      startY++
      endY++
    else
      startY--
      endY--

  result =

    while (count < limit)

      positionX = startX
      positionY = startY

      if directionX is "Right"
        if startX >=0 and endX < W
          moveX directionX
        else
          directionX = "Left"
          moveX directionX
      else
        if startX > 0 and endX <= W
          moveX directionX
        else
          directionX = "Right"
          moveX directionX

      if directionY is "Down"
        if startY >=0 and endY < H
          moveY directionY
        else
          directionY = "Up"
          moveY directionY
      else
        if startY > 0 and endY <= H
          moveY directionY
        else
          directionY = "Down"
          moveY directionY

      count++

      s = "#{positionX} #{positionY}"

  console.log result.join ' '

readInputs = (parameters)->

  cleanString = parameters.replace('\r\n', '')
  paramArray = cleanString.split ' '

  width = paramArray[0]
  height = paramArray[1]
  length = paramArray[2]

  trackText width,height,length

main = ->

  filename = 'DATA.lst'
  fs.readFile filename, (err, contents) -> readInputs contents.toString()

main()

###
> coffee fgomezoso.coffee
0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15
16 16 17 17 18 16 19 15 20 14 21 13 22 12 23 11 24 10 25 9 26 8 27 7 28 6 29
5 30 4 31 3 32 2 33 1 34 0 35 1 34 2 33 3 32 4 31 5 30 6 29 7 28 8 27 9 26
10 25 11 24 12 23 13 22 14 21 15 20 16 19 17 18 16 17 15 16 14 15 13 14 12
13 11 12 10 11 9 10 8 9 7 8 6 7 5 6 4 5 3 4 2 3 1 2 0 1 1 0 2 1 3 2 4 3 5 4
6 5 7 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13 15 14 16 15 17 16 16 17 15 18
14 19 13 20 12 21 11 22 10 23 9 24 8 25 7 26 6 27 5 28 4 29 3 30 2
###
