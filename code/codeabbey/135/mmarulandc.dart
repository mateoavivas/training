/*
  $ dartanalyzer mmarulandc.dart
  Analyzing mmarulandc.dart
  No issues found!
 */

import 'dart:io';

//recursive one line function to get an array splited in 8 chunks
pairs(list) =>
  list.isEmpty ? list : ([list.take(8)]..addAll(pairs(list.skip(8))));

main() async {
  var file = File('DATA.lst');
  var contents;
  var completeBits = '';
  contents = await file.readAsString();
  var splitedData = contents.split("");
  var letters = new List<String>.from(splitedData);
  letters.removeLast();
  for (var letter in letters) {
    switch (letter) {
      case ' ':
        completeBits += '11';
        break;
      case 't':
        completeBits += '1001';
        break;
      case 'n':
        completeBits += '10000';
        break;
      case 's':
        completeBits += '0101';
        break;
      case 'r':
        completeBits += '01000';
        break;
      case 'd':
        completeBits += '00101';
        break;
      case '!':
        completeBits += '001000';
        break;
      case 'c':
        completeBits += '000101';
        break;
      case 'm':
        completeBits += '000011';
        break;
      case 'g':
        completeBits += '0000100';
        break;
      case 'b':
        completeBits += '0000010';
        break;
      case 'v':
        completeBits += '00000001';
        break;
      case 'k':
        completeBits += '0000000001';
        break;
      case 'q':
        completeBits += '000000000001';
        break;
      case 'e':
        completeBits += '101';
        break;
      case 'o':
        completeBits += '10001';
        break;
      case 'a':
        completeBits += '011';
        break;
      case 'i':
        completeBits += '01001';
        break;
      case 'h':
        completeBits += '0011';
        break;
      case 'l':
        completeBits += '001001';
        break;
      case 'u':
        completeBits += '00011';
        break;
      case 'f':
        completeBits += '000100';
        break;
      case 'p':
        completeBits += '0000101';
        break;
      case 'w':
        completeBits += '0000011';
        break;
      case 'y':
        completeBits += '0000001';
        break;
      case 'j':
        completeBits += '000000001';
        break;
      case 'x':
        completeBits += '00000000001';
        break;
      case 'z':
        completeBits += '000000000000';
        break;
      default:
        break;
    }
  }
  var solution = '';
  dynamic listBytes = pairs(completeBits.split(''));
  for (var byte in listBytes) {
    dynamic b8 = byte.toString().replaceAll('(', '')
      .replaceAll(')', '').replaceAll(' ', '').split(',');
    if(b8.length < 8) {
      while(b8.length < 8) {
        b8.insert(b8.length,'0');
      }
    }
    var stringNumber = b8.join('');
    var hex = int.parse(stringNumber,radix: 2).toRadixString(16);
    if(hex.length < 2) {
      var auxHex = '0'+hex.substring(0,hex.length);
      hex = auxHex;
    }
    solution += hex + ' ';
  }
  print(solution.toUpperCase());
}

/* $ dart mmarulandc.dart
  93 B8 51 82 25 22 E1 23 12 E4 1D CD AE 24 50 09 25 2D 86 69 5C 80 91 01 A8
  84 50 BC C7 0A D5 72 09 60 D7 88 9A 43 0E 95 2E 6E 42 28 54 55 B0 97 29 8C
  34 C3 93 B9 04 D0 89 52 5C 8D 17 7A 12 10 E7 4C 2A CA 43 60 89 BC C7 93 A1
  EE 05 D3 0C 37 00 38 AD 6A F1 13 93 B8 2B 0A 22 67 88 99 12 84 B2 70 40 72
  00 23 40 1C 81 22 8E 50 E0 A1 62 89 4C 02 63 5C 15 03 18 17 21 6B 50
*/
