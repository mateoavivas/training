###
$ coffeelint -f ../coffeelint.json neds.coffee
✓ neds.coffee
✓ Ok! » 0 errors and 0 warnings in 1 file
$ coffee -c neds.coffee
###

table = {
  ' ': '11',
  't': '1001',
  'n': '10000',
  's': '0101',
  'r': '01000',
  'd': '00101',
  '!': '001000',
  'c': '000101',
  'm': '000011',
  'g': '0000100',
  'b': '0000010',
  'v': '00000001',
  'k': '0000000001',
  'q': '000000000001',
  'e': '101',
  'o': '10001',
  'a': '011',
  'i': '01001',
  'h': '0011',
  'l': '001001',
  'u': '00011',
  'f': '000100',
  'p': '0000101',
  'w': '0000011',
  'y': '0000001',
  'j': '000000001',
  'x': '00000000001',
  'z': '000000000000'
}

fs = require 'fs'
filePath = 'DATA.lst'
string = ''
byte = ''
bytes = ''
out = ''
flag = false

byteToHex = (bytes) ->
  bytes.split(" ").map((byte) ->
    byte = parseInt(byte, 2).toString(16)
    if byte.length == 1
      out += '0' + byte
    else
      out += byte
    out += ' ')

  console.log out.toUpperCase()

codeString = (dat) ->
  count = 0
  while count < dat.length - 1
    string += table[dat[count]]
    count++

  count = 0
  while count < string.length
    byte += string[count]
    if count == string.length - 1
      flag = true
      while byte.length < 8
        byte += '0'

    if (count + 1) % 8 == 0 || count == string.length - 1
      if flag
        bytes += byte
      else
        bytes += byte + ' '
      byte = ''
    count++

  byteToHex(bytes)

main = () ->
  fs.readFile(filePath, (err, dat) -> codeString(dat.toString()))

main()

###
$ coffee neds.coffee
93 B8 51 82 25 22 E1 23 12 E4 1D CD AE 24 50 09 25 2D 86 69 5C 80 91 01 A8 84 50
BC C7 0A D5 72 09 60 D7 88 9A 43 0E 95 2E 6E 42 28 54 55 B0 97 29 8C 34 C3 93 B9
04 D0 89 52 5C 8D 17 7A 12 10 E7 4C 2A CA 43 60 89 BC C7 93 A1 EE 05 D3 0C 37 00
38 AD 6A F1 13 93 B8 2B 0A 22 67 88 99 12 84 B2 70 40 72 00 23 40 1C 81 22 8E 50
E0 A1 62 89 4C 02 63 5C 15 03 18 17 21 6B 50
###
