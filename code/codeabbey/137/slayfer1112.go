/*
$ golint slayfer1112.go
*/

package main

import (
  "bufio"
  "fmt"
  "math"
  "os"
  "sort"
  "strconv"
  "strings"
)

//dataEntry function to get the data from a file named "DATA.lst"
func dataEntry() [][]string {
  var data string

  dataIn, _ := os.Open("DATA.lst")

  dataScan := bufio.NewScanner(dataIn)

  for dataScan.Scan() {
    line := dataScan.Text()
    data = line
  }

  var dataArray []string
  dataArray = strings.Split((data), "")

  dataMap := make(map[string]int)
  for i := 0; i < len(dataArray); i++ {
    dataMap[dataArray[i]] = strings.Count(data, dataArray[i])
  }

  var Array [][]string
  for n := range dataMap {
    x := []string{n, strconv.Itoa(dataMap[n])}
    Array = append(Array, x)
  }

  return Array
}

//SortArray function to sort Array depend in the quantity and the ascii value
func SortArray(Array [][]string) [][]string {

  sort.Slice(Array[:], func(i, j int) bool {
    for x := range Array[i] {
      x = 1
      a, _ := strconv.Atoi(Array[i][x])
      b, _ := strconv.Atoi(Array[j][x])
      if a == b {

        c := []rune(Array[i][0])
        d := []rune(Array[j][0])
        if c[0] > d[0] {
          continue
        }
        return true
      }
      return a > b
    }
    return false
  })

  return Array
}

//splitArray function used to calculate where we need to split the Array
func splitArray(Array [][]string) int {
  var values []int
  for i := 0; i < len(Array); i++ {
    x, _ := strconv.Atoi(Array[i][1])
    values = append(values, x)
  }
  counter1 := 0
  counter2 := 0
  for i := 0; i < len(values); i++ {
    counter2 += values[i]
  }
  diff1 := math.Abs(float64(counter2 - counter1))
  diff2 := float64(counter2 + 1)
  pos := 0
  for diff2 > diff1 {
    diff2 = diff1
    pos++
    counter1 = 0
    counter2 = 0
    for i := 0; i < len(values); i++ {
      if i < pos {
        counter1 += values[i]
      } else {
        counter2 += values[i]
      }
    }
    diff1 = math.Abs(float64(counter2 - counter1))
    if diff2 == diff1 {
      return (pos - 2)
    }
  }
  return (pos - 2)
}

//addBin use the pos to make 2 slices with the array and eval each one
func addBin(Array [][]string) [][]string {
  posI := splitArray(Array)
  for i := 0; i < (posI + 1); i++ {
    Array[i][2] = Array[i][2] + "O"
  }
  for i := (posI + 1); i < len(Array); i++ {
    Array[i][2] = Array[i][2] + "I"
  }
  if len(Array[:(posI+1)]) != 1 {
    addBin(Array[:(posI + 1)])
  }
  if len(Array[(posI+1):]) != 1 {
    addBin(Array[(posI + 1):])
  }
  return Array
}

//main get use all functions to take an Array and print the final result
func main() {

  Array := dataEntry()

  for i := 0; i < len(Array); i++ {
    x := []rune(Array[i][0])
    y := int(x[0])
    z := strconv.Itoa(y)
    Array[i] = append(Array[i], "", z)
  }

  Array = SortArray(Array)
  Array = addBin(Array)

  for i := 0; i < len(Array); i++ {
    fmt.Print(Array[i][3], " ", Array[i][2], " ")
  }
  fmt.Println("")
  fmt.Println("")

}

/*
  32 OOO 101 OOI 97 OIO 116 OIIO 110 OIII 111 IOOO 104 IOOI 105 IOIOO
  115 IOIOI 99 IOIIO 114 IOIII 100 IIOOO 108 IIOOIO 109 IIOOII 117 IIOIO
  119 IIOIIO 34 IIOIII 121 IIIOOO 107 IIIOOI 112 IIIOIO 46 IIIOIIO
  73 IIIOIII 102 IIIIOOO 44 IIIIOOI 45 IIIIOIO 98 IIIIOII 103 IIIIIOO
  113 IIIIIOIO 118 IIIIIOII 63 IIIIIIOO 66 IIIIIIOI 72 IIIIIIIO
  87 IIIIIIIIO 89 IIIIIIIII
*/
