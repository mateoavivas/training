# frozen_string_literal: true

# root@g4cc3p:~/RubymineProjects/g4cc3p-144# rubocop -l
# Inspecting 1 file
#                  .
#                      1 file inspected, no offenses detected

# frozen_string_literal: true

def mod_inverse(coefficient_s, mod_m, gcd)
  if gcd.abs == 1 && coefficient_s.negative?
    coefficient_s + mod_m
  elsif gcd.abs == 1 && coefficient_s.positive?
    coefficient_s
  end
end

def egcd(coefficient_a, mod_m, coe_s: 1, coe_t: 0)
  old_r = coefficient_a
  new_r = mod_m
  while new_r != 0
    q, r_temp = old_r.divmod(new_r)
    old_r = new_r
    new_r = r_temp
    coe_s, coe_t = coe_t, coe_s - q * coe_t
    return [old_r, coe_s] if old_r == 1
  end
  [-1, 0]
end

def find_x(mod_m, coefficient_a, coefficient_b)
  egcd_nums = egcd(coefficient_a, mod_m)
  if egcd_nums[0] == 1
    inv = mod_inverse(egcd_nums[1], mod_m, egcd_nums[0])
    (- coefficient_b * inv) % mod_m
  else
    -1
  end
end

def solve_mod_inverse
  test_cases = []
  sol = []
  File.open('DATA.lst', 'r').each do |line|
    test_cases.push(line.split(' '))
  end
  test_cases.each_with_index do |test, index|
    next if index.zero?

    sol.push(find_x(test[0].to_i, test[1].to_i, test[2].to_i))
  end
  sol
end

def main
  solutions = solve_mod_inverse
  solutions.each do |x|
    print x
    print "\t"
  end
end

main

# /usr/bin/ruby /root/RubymineProjects/g4cc3p-144/g4cc3p.rb
# -1 28973 -1 183594 946 2118 ... -1 -1 19902142 3007
# Process finished with exit code 0
