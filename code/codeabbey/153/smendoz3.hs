-- $ ghc -o smendoz3 smendoz3.hs-
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad
import Data.Fixed
import Data.Maybe
import Data.List

main = do
  input <- getLine
  input2 <- getLine
  let n = read input :: Integer
  let cypher = read input2 :: Integer
  let preroot = sqroof n (div n 2)
  let xoot = snd (whilex preroot n)
  let root = fst (whilex preroot n)
  let p = root - sqfloor xoot (div xoot 2)
  let q = root + sqfloor xoot (div xoot 2)
  let phi = n - p - q + 1
  let d = fromJust (modInv e phi)
  let a = modPow cypher d n
  let cmessage = fst (splitAt (fromJust (findString "00" (show a))) (show a))
  let message = translate cmessage ""
  print message

e = 65537

translate :: String -> String -> String
translate [] str = str
translate (x:y:xs) str =
  translate xs (str ++ [toEnum (read (x:[y]) :: Int) :: Char])

findString :: (Eq a) => [a] -> [a] -> Maybe Int
findString search str = findIndex (isPrefixOf search) (tails str)

whilex :: Integer -> Integer -> (Integer, Integer)
whilex r n = do
  let xoot = (r ^ 2) - n
  if sqroof xoot (div xoot 2) == sqfloor xoot (div xoot 2) then
    (r, xoot)
  else
    whilex (r + 1) n

modPow :: Integer -> Integer -> Integer -> Integer
modPow base 1 m = mod base m
modPow base pow m
  | even pow = mod (modPow base (div pow 2) m ^ 2) m
  | odd  pow = mod (modPow base (div (pow-1) 2) m ^ 2 * base) m

modInv :: Integer -> Integer -> Maybe Integer
modInv a m
  | 1 == g = Just (mkPos i)
  | otherwise = Nothing
  where
    (i, _, g) = gcdExt a m
    mkPos x
      | x < 0 = x + m
      | otherwise = x

gcdExt :: Integer -> Integer -> (Integer, Integer, Integer)
gcdExt a 0 = (1, 0, a)
gcdExt a b =
  let (q, r) = a `quotRem` b
      (s, t, g) = gcdExt b r
  in (t, s - q * t, g)

sqroof :: Integer -> Integer -> Integer
sqroof 0 y = 0
sqroof 1 y = 1
sqroof x y
  | y > div x y = sqroof x (div (div x y + y) 2)
  | x == y ^ 2 = y
  | otherwise = y + 1

sqfloor :: Integer -> Integer -> Integer
sqfloor 0 y = 0
sqfloor 1 y = 1
sqfloor x y =
  if y > div x y then
    sqfloor x (div (div x y + y) 2)
  else
    y
-- $ ./smendoz3
--   TOE SAT EAR BAR CAR PLY EAT AXE
