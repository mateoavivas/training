"""
$ pylint juansorduz97.py
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python juansorduz.py
"""


def newbinarylength(initiallength_value):
    """Return the binary length with parity bits"""
    checkvalues_flag = 1
    finallength_value = initiallength_value + 2
    basetwo_value = 2
    while checkvalues_flag == 1:
        basetwo_value = basetwo_value * 2
        if finallength_value >= basetwo_value:
            finallength_value = finallength_value + 1
        else:
            checkvalues_flag = 0
    return finallength_value


def binwithoutparbts(finalbinarylength_value, binary_value):
    """Return a list with parity bits as 0"""
    finalbinary_value = [0] * finalbinarylength_value
    basetwo_value = 1
    for i, _ in enumerate(finalbinary_value):
        if i == basetwo_value - 1:
            basetwo_value = basetwo_value * 2
        else:
            finalbinary_value[i] = binary_value[0]
            binary_value = binary_value[1:]
    return finalbinary_value


def calculatebinarybits(binary_value):
    """Return binary number with parity bits"""
    basetwo_value = 1
    while len(binary_value) > basetwo_value:
        initialposition_value = basetwo_value
        xor_value = 0
        position_value = initialposition_value
        state_flag = 1
        auxiliar_value = 1
        while len(binary_value) >= position_value:
            if state_flag == 1:
                if str(xor_value) == str(binary_value[position_value - 1]):
                    xor_value = 0
                else:
                    xor_value = 1
            if auxiliar_value == basetwo_value:
                state_flag = 1 - state_flag
                auxiliar_value = 0
            auxiliar_value = auxiliar_value + 1
            position_value = position_value + 1
        binary_value[basetwo_value - 1] = str(xor_value)
        basetwo_value = basetwo_value * 2
    return binary_value


def decodebinary(binary_list):
    """Return the binary without parity bits"""
    checkvalues_flag = 1
    basetwo_value = 1
    while checkvalues_flag == 1:
        if len(binary_list) > basetwo_value:
            basetwo_value = basetwo_value * 2
        if basetwo_value > len(binary_list):
            basetwo_value = basetwo_value / 2
            checkvalues_flag = 0
    checkvalues_flag = 1
    while checkvalues_flag:
        binary_list.pop(basetwo_value - 1)
        basetwo_value = basetwo_value / 2
        if basetwo_value < 1:
            checkvalues_flag = 0
    return binary_list


def checkerror(binary_value):
    """Return the binary value withour errors"""
    errorposition_value = 0
    basetwo_value = 1
    while len(binary_value) > basetwo_value:
        initialposition_value = basetwo_value
        xor_value = 0
        position_value = initialposition_value
        state_flag = 1
        firstvalue_flag = 1
        auxiliar_value = 1
        while len(binary_value) >= position_value:
            if firstvalue_flag == 1:
                firstvalue_flag = 0
                xor_value = str(binary_value[position_value - 1])
            elif state_flag == 1:
                if str(xor_value) == str(binary_value[position_value - 1]):
                    xor_value = 0
                else:
                    xor_value = 1
            if auxiliar_value == basetwo_value:
                state_flag = 1 - state_flag
                auxiliar_value = 0
            auxiliar_value = auxiliar_value + 1
            position_value = position_value + 1
        if xor_value == 1:
            errorposition_value = errorposition_value + basetwo_value
        basetwo_value = basetwo_value * 2
    if errorposition_value != 0:
        if binary_value[errorposition_value - 1] == "1":
            binary_value[errorposition_value - 1] = "0"
        elif binary_value[errorposition_value - 1] == "0":
            binary_value[errorposition_value - 1] = "1"
    return binary_value


FILE = open("DATA.lst", "r")
CONTENTFILE = FILE.read().splitlines()
LINE = 0
N1 = int(CONTENTFILE[LINE])
N1VALUES = ['0'] * N1
N1FINALVALUES = ['0'] * N1
for a in range(0, N1):
    LINE = LINE + 1
    N1VALUES[a] = CONTENTFILE[LINE]
LINE = LINE + 1
N2 = int(CONTENTFILE[LINE])
N2VALUES = ['0'] * N2
N2FINALVALUES = ['0'] * N2
for b in range(0, N2):
    LINE = LINE + 1
    N2VALUES[b] = CONTENTFILE[LINE]
for c in range(0, N1):
    BINARYLENGTH = len(N1VALUES[c])
    FINALBINARYLENGTH = newbinarylength(BINARYLENGTH)
    INTERMEDIUMBINARY = binwithoutparbts(FINALBINARYLENGTH, N1VALUES[c])
    N1FINALVALUES[c] = "".join(calculatebinarybits(INTERMEDIUMBINARY))
for d in range(0, N2):
    N2NUMBER = list(N2VALUES[d])
    N2NUMBERCK = checkerror(N2NUMBER)
    while N2NUMBERCK != checkerror(N2NUMBER):
        N2NUMBERCK = checkerror(N2NUMBER)
    N2FINALVALUES[d] = "".join(decodebinary(N2NUMBERCK))
FINALNUMBERS = N1FINALVALUES + N2FINALVALUES
for e, _ in enumerate(FINALNUMBERS):
    if len(FINALNUMBERS) > e:
        print(FINALNUMBERS[e]),
    else:
        print FINALNUMBERS[e]

# $ python juansorduz.py
# 1010101000000 1000011000010101010 11100110101011 1011011111101100111110110011
# 100 001001001100001011111001 00110110111001010101010 110110011101110111001000
#  1111001100110011001000 10110000010101111 01111101001111110111000 00001011111
# 0110011010100 01000001111101 001001101010 0101010 00100000101000 110100100111
# 0111110100011 00011101000010001101110 010101100111110010101011 01111001110000
# 11000 0010110111110001001100010 1001100101111001 100101000111101011 111011000
# 00111110000 00100000111010101011111100 0011100101110100100 011001010100101010
#  111100100 01110101000111110101
