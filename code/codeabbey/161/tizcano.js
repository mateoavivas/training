/*
$ eslint tizcano.js
$
*/

function reverse(arr) {
  if (arr.length > 0) {
    return [ ...reverse(arr.slice(1)), arr[0] ];
  }
  return [];
}
function auxNeighbors(graph, visited, start) {
  const text = graph.map((vertex) => {
    if (vertex[0] === start) {
      if (!visited.includes(vertex[1])) {
        return vertex[1];
      }
      return false;
    }
    return false;
  });
  return text.filter((element) => element !== false);
}

function auxDfs(graph, visited = [], start, order = []) {
  if (visited.includes(start) || order.includes(start)) {
    return { visited, order };
  }
  const neighbors = auxNeighbors(graph, visited, start);
  const newVisited = visited.concat(start);
  const newOrder = order;
  const topological = neighbors.reduce(
    (acc, element) => auxDfs(graph, acc.visit, element, acc.order),
    { visited: newVisited, order: newOrder }
  );

  return {
    visited: topological.visited,
    order: topological.order.concat(start),
  };
}
function dfs(graph) {
  return graph.reduce(
    (acc, element) => {
      if (!acc.visited.includes(element[0])) {
        return auxDfs(graph, acc.visited, element[0], acc.order);
      }
      return acc;
    },
    { visited: [], order: [] }
  );
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const graph = inputFile.slice(1).map((element) => element.split(' '));
  const solvedObjectReversed = dfs(graph);
  const solvedObject = reverse(solvedObjectReversed.order);
  const output = process.stdout.write(`${ solvedObject.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/*
$ node tizcano.js
output:
Hx Er Pp Vq Dg Wz Ez Nm Ne Yp Mm Xh Ue Ik Vl Uf Et Tl Ok Fr Pe Ep Xt Fj Pq Fk
Ds Yb Ix Qj Oy Wm Uj It Da Ci Gy Ht Dn Fp Ev Zh Sg Aw Pg Nd Tn Pz Wp Rs Hb Ca
Rm Jo Ww Fw Wj Bn Nl Tm Ft Hi Uu Js Fb Re Bc Pl Os Sc Ri Pt Fg Lg Si Zm Yu Tg
Tq Aq Mj Ns Ma Kf Ql Zi Ve Gh Wu Al Vw Vu Jz Mf Lr Cq Uq Ua Xv Bo Ug Rk Fx Ys
Qz Ki Fl Um Rt Lv Du Yc Iu Yw Ka Lz Lk Vz Wh Rr Lu Cy Ia Ol Il Pf Py Rb In Rd
Am Fs Eo Bu Bg Ky Po Va Zt La Vn Go Ag Ea Fa Rp Mk Tz Mb Tt Px Dq Ow Uz So Tk
*/
