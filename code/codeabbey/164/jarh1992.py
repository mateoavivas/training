#!/usr/bin/python3
"""
$ pylint jarh1992.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

import random

N = list(input().split(" "))
NOD = int(N[0])
LNK = int(N[1])
WKR, STP = 50, 20
INP = [[] for i in range(NOD)]
RESP = [0 for i in range(NOD)]

for i in range(LNK):
    a = list(map(int, input().split(' ')))
    INP[a[0]].append(a[1])

END = 0
for n in range(NOD):
    for w in range(WKR):
        for s in range(STP):
            END = random.choice(INP[n])
        RESP[END] += 1

print ' '.join(list(map(str, RESP)))

# $ python jarh1992.py
# 13 31
# 0 2
# 0 5
# 0 7
# 0 8
# 1 11
# 2 4
# 3 1
# 3 6
# 3 7
# 4 1
# 4 3
# 4 5
# 4 6
# 4 9
# 5 11
# 6 4
# 6 7
# 6 10
# 6 12
# 7 2
# 8 7
# 9 0
# 9 4
# 9 11
# 10 0
# 10 3
# 10 4
# 11 6
# 11 10
# 11 12
# 12 8
# 31 26 61 18 104 20 42 106 61 10 26 117 28
