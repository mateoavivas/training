/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ clouds ] = inputData.toString().split('\r\n');
  const arrData = inputData.toString().split('\r\n').slice(1);
  const flatAngle = 180;

  for (let i = 0; i < Number(clouds); i++) {
    const [ distanceOne, angleBeta, angleAlpha ] = arrData[i]
      .split(' ')
      .map((x) => Number(x));
    const angleBetaRadians = (angleBeta * Math.PI / flatAngle);
    const angleAlphaRadians = (angleAlpha * Math.PI / flatAngle);
    const tanBeta = Math.tan(angleBetaRadians);
    const tanAlpha = Math.tan(angleAlphaRadians);
    const distanceTwo = (tanAlpha * distanceOne) / (tanBeta - tanAlpha);
    const height = Math.abs(Math.round(tanBeta * distanceTwo));
    results.push(height);
  }

  console.log(results.map((x) => x.toString()).join(' '));
});

/*
$ js ngarci
output:
1207 1609 1163
*/
