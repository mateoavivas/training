"""
$ pylint juansorduz97.py
Global evaluation
-----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python juansorduz.py
"""


def squareroot(number):
    """Return the sqr root regarding problem statement where M*M <= Z."""
    r_value = number / 2
    a_value = number
    while a_value > 1:
        d_value = number / r_value
        a_value = abs(r_value - d_value)
        r_value = (r_value + d_value) / 2
    checknum_flag = 1
    initialsqr_value = r_value
    while checknum_flag == 1:
        if initialsqr_value * initialsqr_value < number:
            checknum_flag = 0
        if checknum_flag == 1:
            initialsqr_value = initialsqr_value - 1
    return initialsqr_value


FILE = open("DATA.lst", "r")
INPUT = str.split(FILE.read())
VALUES = [int(i) for i in INPUT]

K = VALUES[0]
N = VALUES[1]
POLYGONSIZES = 6
R = pow(10, K)
D = int(R / 2)
H = squareroot(R * R - D * D)
X = squareroot(D * D + R * R - 2 * R * H + H * H)
for i in range(1, N):
    POLYGONSIZES = POLYGONSIZES * 2
    D = int(X / 2)
    H = squareroot(R * R - D * D)
    X = squareroot(D * D + R * R - 2 * R * H + H * H)
PI = POLYGONSIZES * X
print PI

# $ python juansorduz.py
# 31415926535897932384625217937520959553073552723686719488
