// $ golangci-lint run oscardjuribe.go
//

// Code to solve the problem Spaceship Weight Fraud from codeabbey

package main

import (
  "bufio"
    "fmt"
    "log"
    "os"
  "strconv"
)

// method to find the greater number between to hex strings
func isMax(current string, compared string) bool {

  var c1 byte
  var c2 byte

  var num1 uint64
  var num2 uint64

  // iterate over parameters
  var pos int

  // current chars
  c1 = current[pos]
  c2 = compared[pos]

  // convert to number
  num1, _ = strconv.ParseUint(string(c1), 16, 32)
  num2, _ = strconv.ParseUint(string(c2), 16, 32)

  // iterate while digits are the same
  for pos < len(current) && num1 == num2 {

    // current chars
    c1 = current[pos]
    c2 = compared[pos]

    // convert to number
    num1, _ = strconv.ParseUint(string(c1), 16, 32)
    num2, _ = strconv.ParseUint(string(c2), 16, 32)

    pos ++
  }
  // check if current digit is greater
  if num2 > num1 {
    // then the first parameter is smaller
    return false
  }

  // the first parameter is greater
  return true

}

// get the min and max value after swappings
func minAndMax(current string) (string, string) {

  var currentMax string
  var currentMin string

  currentMin = current
  currentMax = current

  for i := 0; i < len(current) - 1; i++ {

    for j := i + 1; j < len(current); j++ {

      var auxChar rune

      // create auxiliar arrays to swap
      var copy = current

      var newString = []rune(copy)

      // swap chars
      auxChar = newString[i]
      newString[i] = newString[j]
      newString[j] = auxChar

      // check if it is greater
      if isMax(string(newString), currentMax) {
        currentMax = string(newString)
      }

      // check if it is smaller
      if !isMax(string(newString), currentMin) && newString[0] != '0' {
        currentMin = string(newString)
      }
    }

  }
  // return max and min
  return currentMin, currentMax
}

func main() {
  // current number
  var currentNumber string

  // store max and min after solution
  var max string
  var min string

  // final solution
  var solution string

  // open file
    file, err := os.Open("./DATA.lst")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    // read file
    scanner := bufio.NewScanner(file)

    // read testcases
    scanner.Scan()

    for scanner.Scan() {
      // read current number as string
        currentNumber = scanner.Text()

        // get min and max strings
    min, max = minAndMax(currentNumber)

    // concatenate solution
    solution += min + " " + max + " "
    }

  // print solution
  fmt.Printf("%s\n", solution)

}

// go run oscardjuribe.go
// 16F1EB8F838DC629365CA7FA3 F6F1EB8F838DC6293651A7CA3 27422082B7DA66EF4A
// F742208227DA66EB4A 249D9B80A74EDB3FE48 F49D9B80A24EDB37E48 1FE664F58ACFE
// FFE164F58AC6E 17068A8C967C56DE77 E7068A8C967C561D77
// 184A8A32626B08B606FCF54CBBD9 F84A8A32621B08B606FC654CBBD9 278DB55558B8979
// D785B25558B8979 1F38F0845400392 FF3830845400192 103CD8121DAB43D731A
// DA3CD8121DAB4317310 1886C86E294347F2 F886C86E29434721 142EEC32AA6BA
// E41E2C32AA6BA 13D309AEB32E89E99FEBEC7 F31309AEB32E89E99DEBEC7
// 2FC5D7EAA0DB979 FEC5D72AA0DB979 1B4352DF2BAB823740D FB4352DB2BA1823740D
// 1CC2CAD627BD208 DCC2CAD627B8201 1F75B3743AC7A992665203 F375B1743AC7A992665203
// 1A40C42571CDDD413ED897 EA40C42571CDDD4113D897 1D810948CFCCCCF FD810948CFCCC1C
// 20D5EAA0B9E495 E0D5EAA0B99425 1300395B54820A1122E8E EE00395B54820A1122381
