/*
➜  191 git:(sgomezatfluid) ✗ dartanalyzer sgomezatfluid.dart
Analyzing sgomezatfluid.dart...
No issues found!
*/

import 'dart:io';

void main() async {
  List<String> data = await new File('DATA.lst').readAsLines();
  String answer = '';
  data.forEach(
    (value) {
      int minPosition = 0;
      int maxPosition = 0;
      int minSwap = 0;
      int maxSwap = 0;
      minPosition = minPositionFrom(value, 0, 0);
      maxPosition = maxPositionFrom(value, 0);
      for(int i=0; i < value.length; i++) {
        String valoractual = value[i];
        String valorMinimo = value[minPosition];
        if(valoractual == valorMinimo && i+1 < value.length) {
          minPosition = minPositionFrom(value, i+1, i+1);
          minSwap = i+1;
        } else {
          if(minSwap==0 && value[minPosition] == '0'){
            minSwap++;
            minPosition = minPositionFrom(value, i+1, i+1);
          } else {
            answer += swap(value, minSwap, minPosition) + ' ';
            break;
          }
        }
      }
      for(int i=0; i < value.length; i++) {
        if(value[i] == value[maxPosition] && i+1 < value.length) {
          maxPosition = maxPositionFrom(value, i+1);
          maxSwap = i+1;
        } else {
          break;
        }
      }
      answer += swap(value, maxSwap, maxPosition) + ' ';
    }
  );
  print(answer);
}

int minPositionFrom(String value, int initial, int swap) {
  int minPosition = initial;
  for(int i=initial; i < value.length; i++) {
    if(int.parse('0x${value[i]}') <= int.parse('0x${value[minPosition]}')) {
      if(swap > 0 || value[i] != '0'){
        minPosition = i;
      }
    }
  }
  return minPosition;
}

int maxPositionFrom(String value, int initial) {
  int maxPosition = initial;
  for(int i=initial; i < value.length; i++) {
    if(int.parse('0x${value[i]}') >= int.parse('0x${value[maxPosition]}')) {
      if(value[i] != '0') {
        maxPosition = i;
      }
    }
  }
  return maxPosition;
}

String swap(String str, int indexa, int indexb) {
  List<String> strlist = str.split('');
  String a = strlist[indexa];
  String b = strlist[indexb];
  strlist[indexa] = b;
  strlist[indexb] = a;
  return strlist.join();
}


/*
➜  191 git:(sgomezatfluid) ✗ dart sgomezatfluid.dart
16F1EB8F838DC629365CA7FA3 F6F1EB8F838DC6293651A7CA3 27422082B7DA66EF4A
F742208227DA66EB4A 249D9B80A74EDB3FE48 F49D9B80A24EDB37E48 1FE664F58ACFE
FFE164F58AC6E 17068A8C967C56DE77 E7068A8C967C561D77 184A8A32626B08B606FCF54CBBD9
F84A8A32621B08B606FC654CBBD9 278DB55558B8979 D785B25558B8979 1F38F0845400392
FF3830845400192 103CD8121DAB43D731A DA3CD8121DAB4317310 1886C86E294347F2
F886C86E29434721 142EEC32AA6BA E41E2C32AA6BA 13D309AEB32E89E99FEBEC7
F31309AEB32E89E99DEBEC7 2FC5D7EAA0DB979 FEC5D72AA0DB979 1B4352DF2BAB823740D
FB4352DB2BA1823740D 1CC2CAD627BD208 DCC2CAD627B8201 1F75B3743AC7A992665203
F375B1743AC7A992665203 1A40C42571CDDD413ED897 EA40C42571CDDD4113D897
1D810948CFCCCCF FD810948CFCCC1C 20D5EAA0B9E495 E0D5EAA0B99425
1300395B54820A1122E8E EE00395B54820A1122381
*/
