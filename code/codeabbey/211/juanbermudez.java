package RETO211;
import java.util.*;
import java.awt.List;

public class InformationEntropy {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    String[] data= {"company plays with pigeons","samantha saves sins",
                    "taiss extracts clothes","alla gives pigeons",
                    "president sees integer array","rosanna plays with scenes",
                    "noah sits on scenes","taiss extracts girls",
                    "rosanna passes by ions","teah gives pigeons",
                    "maya begat plasma rifle","amanda examines sins",
                    "company dreams of cheese","irina arranges beer",
                    "alla saves scent","emma loves clothes",
                    "amanda reads scenes","signiora loves alcohol"};  
    for (String data0 : data) {
      System.out.printf("%.11f ",entropy(frequencyData(data0)));      
    }
  }
  
  public static double[] frequencyData(String data) {
    HashSet<String> uniqueData=new HashSet<String>();
    String newDate;
    for (int i = 0; i < data.length(); i++) {
      newDate=Character.toString(data.charAt(i));
      uniqueData.add(newDate);
    }
    double[] returnData=new double[uniqueData.size()];
    String[] arrayUnique=uniqueData.toArray(new String[0]);
    //System.out.println(uniqueData);
    for (int i = 0; i < arrayUnique.length; i++) { 
      for (int j = 0; j < data.length(); j++) {
        newDate=Character.toString(data.charAt(j));
        if (newDate.equals(arrayUnique[i])) {
          returnData[i]++;
        }
      }
      returnData[i]=returnData[i]/data.length();
    }
    return returnData;
  } 
  public static double entropy(double[] frequencyData) {
    double entropy=0;
    for (int i = 0; i < frequencyData.length; i++) {
      double resultLog=(Math.log(1/frequencyData[i]))/(Math.log(2));
      entropy=frequencyData[i]*resultLog+entropy;
    }
    return entropy;
  }
}
