# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program calculates the smallest integer that is divisible by all
integers on a given interval [right, left]
"""
from __future__ import print_function


def gcd(num1, num2):

    """
    Returns the greatest common divisor of two numbers by using the
    euclides algorithm
    """
    while num1 != num2:

        if num1 > num2:
            num1 -= num2
        else:
            num2 -= num1

    return num1


def lcm(num1, num2):

    """
    Returns the least common multiple of two numbers by using
    the gcd function
    """
    lcm1 = int((num1 * num2)/gcd(num1, num2))

    return lcm1


def smallest_multiple(left, right):

    """
    lcm function is calculated and stored in order to reuse it
    again for the next iteration
    """
    lcm2 = 1

    for i in range(left, right):

        lcm2 = lcm(lcm2, lcm(i, i+1))

    return lcm2


def main():

    """
    Reads the input data and uses the smallest multiple function
    """
    data = open("DATA.lst", "r")

    number1 = int(data.readline().rstrip('\n'))
    number2 = int(data.readline().rstrip('\n'))

    smallest = smallest_multiple(number1, number2)

    print(smallest)

    data.close()


main()

# $ python fgomezoso.py
# 840
