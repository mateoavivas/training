# !usr/bin/python
# [Finished in 0.4s]

# pylint edr3mo.py
# No config file found, using default configuration
#
# -------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 9.29/10, +0.71)
"""
Determine if it is possible to rearrange
the characters in a string
to obtain another string.
"""


def symbols_permutation():
    """Determine the string can be permutated"""
    symbols_file = open("DATA.lst", "r")
    words = symbols_file.read().split(", ")
    word_1 = words[0]
    word_2 = words[1]
    is_permutation = True
    if len(word_1) != len(word_2):
        is_permutation = False
    else:
        for i in word_1:
            if i in word_2:
                is_permutation = True
            else:
                is_permutation = False
                break
    print is_permutation


symbols_permutation()
# python ./edrmo.py
# True
