"""
$ pylint calderonsin.py
Global evaluation
-----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def maxlen():
    """max lenght of a substring thar repeats at leats twice"""
    data = open('DATA.lst', 'r')
    string = data
    maxi = 0
    for aux1 in range(len(string)):
        substring = ""
        for aux2 in string[aux1:]:
            substring += aux2
            if string[aux1:].rfind(substring) != string[aux1:].find(substring):
                maxi = max(maxi, len(substring))
                continue
    print maxi


maxlen()
# $ python3 calderonsin.py build
# 10
