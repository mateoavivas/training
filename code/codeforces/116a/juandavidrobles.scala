object JuanDavidRobles116A {
  def main(args: Array[String]): Unit = {

    import java.util.Scanner
    import java.io.{BufferedReader, InputStreamReader}

    val streamReader: InputStreamReader = new InputStreamReader(System.in)
    val scanner: Scanner = new Scanner(new BufferedReader(streamReader))

    val n: Int = Integer.parseInt(scanner.nextLine())
    var count: Int = 0
    var capability: Int = 0

    var exit: Int = 0
    var enter: Int = 0

    for (i <- 0 until n){
      exit = scanner.nextInt()
      enter = scanner.nextInt()

      count = count - exit + enter
      if (capability < count){
        capability = count
      }
    }

    println(capability)

  }
}
