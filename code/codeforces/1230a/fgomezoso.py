# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program calculates if its possible to give 4 bags of "X" candies
to two friends so that both friends receive the same amount of candies
"""
from __future__ import print_function


def main():

    """
    If the total number of candies in the 4 bags is an even number
    Then the program checks if one the bags has half of the candies

    If not, then it adds different combinations of bags and checks if the
    result has half of the candies
    """
    data = open("DATA.lst", "r")

    bag_input = data.readline().rstrip('\n')
    bags = list(map(int, bag_input.split()))
    total_bags = 4

    count = 0
    result = 0

    for i in range(0, total_bags):

        count = bags[i] + count

    if count % 2 == 0:

        for i in range(0, total_bags):

            if bags[i] == count/2:

                result = 1

        for j in range(1, total_bags):

            if bags[0] + bags[j] == count/2:

                result = 1
                break

        for k in range(2, total_bags):

            if bags[0] + bags[1] + bags[k] == count/2:

                result = 1
                break

        if bags[0] + bags[2] + bags[3] == count/2:

            result = 1

    if result == 1:

        print("YES")

    else:

        print("NO")

    data.close()


main()

# $ python fgomezoso.py
# YES
