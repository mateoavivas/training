object JuanDavidRobles131A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn

    var string: String = StdIn.readLine()
    var conditional: Boolean = false

    var substring: String = string.substring(1)

    conditional = string.charAt(0).equals(string.toLowerCase.charAt(0)) &&
      substring.equals(substring.toUpperCase)

    if (string.equals(string.toUpperCase)){
      string = string.toLowerCase
    }

    if (conditional){
      string = string.toLowerCase.capitalize
    }

    println(string)
  }
}
