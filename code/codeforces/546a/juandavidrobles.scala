object JuanDavidRobles546A {
  def main(args: Array[String]): Unit = {

    import java.util.Scanner
    import java.io.InputStreamReader
    import java.io.BufferedReader

    val streamReader: InputStreamReader = new InputStreamReader(System.in)
    val scanner: Scanner = new Scanner(new BufferedReader(streamReader))

    var firstCost: Int = Integer.valueOf(scanner.next())
    var money: Int = Integer.valueOf(scanner.next())
    var bananas: Int = Integer.valueOf(scanner.next())

    var total: Int = 0

    for (i <- 1 to bananas){
      total = total + i * firstCost
    }
    var result: Int = total - money

    if (result < 0){
      result = 0
    }

    println(result)
  }
}
