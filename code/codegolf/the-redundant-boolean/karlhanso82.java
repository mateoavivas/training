/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 27, 2019 9:46:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
import java.util.Properties;
import java.net.URL;

class Truhtyfalse {
 URL path;
 InputStream input;
 static Properties prop;
 public Truhtyfalse() throws Exception {
  path = ClassLoader.getSystemResource("DATA.lst");
  input = new FileInputStream(path.getFile());
  prop = new Properties();
  prop.load(input);
 }

 public static Boolean comparison(String decimal) {
  if (decimal.equals(prop.getProperty("one"))) {
   return true;
  } else {
   boolean value = Boolean.parseBoolean(decimal);
   return false;
  }
 }

 public static void main(String args[]) throws Exception {
  Truhtyfalse obj = new Truhtyfalse();
  boolean result;
  System.out.println(prop.getProperty("msg"));
  System.out.println(prop.getProperty("val1"));
  System.out.println(prop.getProperty("val2"));
  int a = Integer.parseInt(prop.getProperty("val1"));
  int b = Integer.parseInt(prop.getProperty("val2"));
  result = comparison(String.valueOf(a));
  System.out.println(result);
  result = comparison(String.valueOf(b));
  System.out.println(result);
 }
}
/*
$ java karlhanso82
Truthyor false
$
*/

