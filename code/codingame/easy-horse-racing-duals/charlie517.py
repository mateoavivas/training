#! /usr/bin/env python

'''
$ ./charlie517.py
$ pylint charlie517.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

NUM = -1

with open('DATA.lst') as f:
    NUMHORSES = f.readline()
    SORTHORSES = sorted(f.read().splitlines())
    DISTANCE = int(max(SORTHORSES)) - int(min(SORTHORSES))

for i in range(len(SORTHORSES)-1):
    if int(SORTHORSES[i+1]) - int(SORTHORSES[i]) < DISTANCE:
        DISTANCE = int(SORTHORSES[i+1]) - int(SORTHORSES[i])
        NUM = i

for i in range(len(SORTHORSES)-1):
    if int(SORTHORSES[i+1]) - int(SORTHORSES[i]) == DISTANCE:
        print int(SORTHORSES[i+1]) - int(SORTHORSES[i])

# ./charlie517
# 2
