# > pylint kzccardona.py
#
#    -------------------------------------------------------------------
#    Your code has been rated at 10.00/10

"""
This code find the sum of all the unique multiples of particular numbers
Up to but not including that number
"""
from __future__ import print_function


def sum_of_multiples(limit, multiples):

    # type: (int, list[str]) -> int
    """
    This function find the sum of all the unique multiples
    """
    acumulate = 0
    for num in range(1, limit):
        for multiple in multiples:
            if int(multiple) > 0:
                if num % int(multiple) == 0:
                    acumulate = acumulate + num
                    break
    return acumulate


for numbers in open("DATA.lst", "r"):
    number = numbers.rstrip()
    numberlist = number.split(';')
    print(sum_of_multiples(int(numberlist[0]), numberlist[1].split(',')))


# > python ./kzccardona.py
# 233168
# 2318
# 51
