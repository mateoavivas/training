/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
import java.lang.Math;

class lekriss {

  public static void main (String args[]) throws Exception{
    FileReader r = new FileReader(new File("DATA.lst"));
    BufferedReader br = new BufferedReader(r);
    Scanner input = new Scanner(br);
    int limtA = input.nextInt();
    int LimitB = input.nextInt();
    String out = "";

    for(int i=limtA; i<=LimitB; i++) {
      if(isPrime(i)) {
        if(isPrime(sumOfDigits(i))) {
          out += i+" ";
        }
      }
    }
    System.out.print(out);
  }

  public static boolean isPrime(int evaluated) {
    if(evaluated==2) {
      return true;
    }
    else {
      for(int i=2; i<Math.sqrt(evaluated)+1; i++) {
        if(evaluated%i==0) {
          return false;
        }
      }
    }
    return true;
  }

  public static int sumOfDigits(int toEval) {
    int result = 0;
    String temp = toEval +"";
    for(int i=0; i<temp.length(); i++) {
      result+=temp.charAt(i)-48;
    }
    return result;
  }

}
/*
$ java lekriss
$ 11 23 29 41 43 47
*/
