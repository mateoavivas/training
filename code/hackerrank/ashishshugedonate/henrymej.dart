
/*
flutter analyze #lints
*/
import 'dart:math';
import 'dart:io';

double squares(int n) {
  return (n * (n + 1) * (2 * n + 1))/6;
}

bool binary(int a, int b, int candies) {
  int c = a + ((b - a)~/2);
  if(squares(c) > candies) {
      return true;
    }
  return false;
}

int solve(int x) {
  int b = pow(10, 6);
  int a = 0;
  while (b - a > 1) {
    if (binary(a, b, x)) {
      b = a + ((b - a)~/2);
    }
    else {
      a = a + ((b - a)~/2);
    }
  }
  if(squares(b) == x) {
    return b;
  }
  return a;
}
void main() {
  new File('DATA.lst').readAsString().then((String contents) {
    var list = contents.split("\n");
    int n = int.parse(list[0]);
    String result = "";
    for(int i = 1; i <= n; i++) {
      if(i % 7 == 0 && i != n) {
        result += "\n";
      }
      else if (i > 1) {
        result += " ";
      }
      result += (
        solve(int.parse(list[i]))).toString();
    }
    print(result);
  });
}
/*
dart henrymej.dart
131476 113653 109891 99545 137094 143815
92003 137040 143029 123742 114832 129951 139763
90565 121671 141824 110056 116406 113672 139258
109894 121746 117812 139458 87813 140009 138646
142966 86043 122951 86928 123615 101968 108978
117056 126960 94400 98622 137744 98270 89397
105799 134507 94185 138234 143626 133399 141396
119569 77160
*/
