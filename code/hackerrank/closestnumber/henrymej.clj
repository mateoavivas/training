;
;lein eastwood #linting
;

(require '[clojure.string :as str])

(def inputs (str/split (slurp "DATA.lst") #"\n"))

(defn exp [b e] (Math/pow b e))

(defn abs [n] (max n (- n)))

(defn solve [a b x]
  (cond
    (> (abs (- (exp a b) (* x (int (quot (exp a b) x)))))
    (abs (- (exp a b) (* x (+ 1 (int (quot (exp a b) x)))))))
    (* x (+ 1 (int (quot (exp a b) x))))
  :else
    (* x (int (quot (exp a b) x)))
  )
)

(def q (Integer/parseInt (str/trim (get inputs 0))))

(def queries [])
(def i 1)
(doseq [_ (range q)]
  (def queries (conj queries (vec (
    map #(Integer/parseInt %) (
      clojure.string/split (
        get inputs i) #" ")))))
  (def i (inc i))
)

(def i 0)
(def result "")
(doseq [_ (range q)]
  (def query (get queries i))
  (def a (get query 0))
  (def b (get query 1))
  (def x (get query 2))
  (def i (inc i))
  (def result (str result (solve a b x) " "))
)
(println result)

;
;clojure henrymej.clj
;0 0 0 454278 0 89021456 653379375 0 1 1
;
