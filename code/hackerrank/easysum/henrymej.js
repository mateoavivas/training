
/*
tslint --init #linting config
tslint henrymejia2.js #linting
*/

/* global BigInt */

const files = require('fs');
const path = require('path');

function solve(First, Second) {
  const first = BigInt(First);
  let second = BigInt(Second);
  const quot = BigInt(Math.floor(Number(first / second)));
  let sums = second * (second - BigInt(1)) / BigInt(2);
  sums += sums * quot;
  second = first % second;
  sums += second * (second + BigInt(1)) / BigInt(2);
  return sums;
}

files.readFile(path.join(__dirname, 'DATA.lst'), (flaw, stream) => {
  if (flaw) {
    throw flaw;
  }
  const inputs = stream.toString().split('\n');
  const [ total ] = inputs;
  let outputs = '';
  for (let index = 1; index <= total; index++) {
    const input = inputs[index].split(' ');
    const base = 10;
    const outp = solve(parseInt(input[0], base), parseInt(input[1], base));
    outputs = `${ outputs } ${ outp }`;
  }
  outputs = outputs.substring(1, outputs.length);
  files.writeFile('Output.txt', outputs, (issue) => {
    if (issue) {
      throw issue;
    }
  });
});
/*
node henrymejia2.js
136698 294241 339342 311689 232011 289351 209511 156927
160891 42703 203294 439434 18731 470290 300148 160904 18240
34833 227628 339027 115740 316515 7318 66486 325041 74983 21219 270733
369271 176292 14160 364974 263388 8383 56770 29956 94052 298782 44298
55167 36108 387559 452104 195084 388163 135760 345645 41161 110281 81218
*/
