#!/bin/python

"""
$ pylint l0vecraft.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""


def grading_students(grades):
    """solution function"""
    result = []
    tmp = 0
    for i in grades:
        if(i >= 38 and i % 5 > 2):
            tmp = i + (5 - i % 5)
            result.append(tmp)
        else:
            result.append(i)
    return result


if __name__ == "__main__":
    with open('./DATA.lst') as data:
        CONVERTDATA = [int(x) for x in data.readlines()]
        RESULT = grading_students(CONVERTDATA)

# $ python l0vecraft.py
# 4 75 67 40 33
