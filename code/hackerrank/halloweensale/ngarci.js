/* eslint brace-style: ["error", "stroustrup"]*/
/*
$ eslint ngarci
$ node ngarci
*/
const files = require('fs');
const path = require('path');

function howManyGames(price, discount, min, money) {
  let numberOfGames = 0;
  while (money >= price) {
    if (numberOfGames === 0) {
      money -= price;
      numberOfGames++;
    }
    else if (price > min) {
      if (price - discount <= min) {
        price = min;
      }
      else {
        price -= discount;
      }
      money -= price;
      numberOfGames++;
    }
    else {
      money -= min;
      numberOfGames++;
    }
  }
  return numberOfGames;
}

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ NumberOne, NumberTwo, NumberThree, NumberFour ] = inputData
    .toString()
    .split(' ');

  const outputs = howManyGames(
    Number(NumberOne),
    Number(NumberTwo),
    Number(NumberThree),
    Number(NumberFour)
  );

  files.writeFile('Output.txt', outputs, (issue) => {
    if (issue) {
      throw issue;
    }
  });
});

/*
$ node ngarci
output:
test case 0: 6
*/
