/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

function kaprekarNumbers(init, end) {
  const results = [];
  for (let i = init; i <= end; i++) {
    const square = Math.pow(i, 2);
    const stringSquare = String(square);
    const sizeNumber = String(i).length;
    const arrDigits = stringSquare
      .split('');
    arrDigits.splice(-sizeNumber, 0, ':');
    const sumDigits = arrDigits
      .join('')
      .split(':')
      .map((x) => Number(x))
      .reduce((x, y) => x + y);
    if (sumDigits === i) {
      results.push(i);
    }
  }
  if (results.length > 0) {
    return results.join(' ');
  }
  return 'INVALID RANGE';
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ init, end ] = inputData
    .split('\r\n')
    .map((x) => Number(x));

  console.log(kaprekarNumbers(init, end));
});

/*
$ js ngarci
output:
1 9 45 55 99
*/
