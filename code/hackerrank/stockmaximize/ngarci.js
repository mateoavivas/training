/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

function stockmax(prices) {
  let benefit = 0;
  let currentMax = prices[prices.length - 1];
  for (let i = prices.length - 1; i >= 0; i--) {
    if (prices[i] > currentMax) {
      currentMax = prices[i];
    }
    benefit += currentMax - prices[i];
  }
  return benefit;
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const araysPrices = inputData.split('\r\n').filter((x) => x.length > 1);

  araysPrices.forEach((x) => {
    const prices = x.split(' ').map((ele) => Number(ele));
    console.log(stockmax(prices));
  });
});

/*
$ js ngarci
output:
0
197
3
*/
