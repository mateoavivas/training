//Area de un triangulo y un cuadrado

import java.util.Scanner;

public class Reto3 {
    public static void main(String[] args) {
   
        Scanner digit = new Scanner(System.in);
        int baseTriangulo   = 0;
        int alturaTriangulo = 0;
        int areaTriangulo   = 0;
        int ladoCuadrado    = 0;
        int areaCuadrado    = 0;
        
        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto3*");
        System.out.println("*******");
        System.out.println("");

        System.out.println("**********************");
        System.out.println("*Area de un triangulo*");
        System.out.println("**********************");
        System.out.println("");
        System.out.print("Ingrese la base del triangulo en centimetros: ");
        baseTriangulo = digit.nextInt();
        System.out.println("");
        System.out.print("Ingrese el altura del triangulo en centimetros: ");
        alturaTriangulo = digit.nextInt();
        System.out.println("");
    
        System.out.println("*********************");
        System.out.println("*Area de un cuadrado*");
        System.out.println("*********************"); 
        System.out.println("");
        System.out.print("Ingrese un lado del cuadrado en centimetros: ");
        ladoCuadrado = digit.nextInt();
        System.out.println("");

        System.out.println("************");
        System.out.println("*Resultados*");
        System.out.println("************"); 
        System.out.println("");

        areaTriangulo = (baseTriangulo*alturaTriangulo)/2;
        System.out.println("El area del triangulo es: "
                           +areaTriangulo+" centimetros");
        System.out.println("");

        areaCuadrado = ladoCuadrado*ladoCuadrado;
        System.out.println("El area del cuadrado es: "
                           +areaCuadrado+" centimetros");
        System.out.println("");
    }
}
