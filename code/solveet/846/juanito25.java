//Bucle, multiplicacion o suma de los 10 primeros numeros

import java.util.Scanner;

public class Reto10 {
    public static void main(String[] args) {

        Scanner digit      = new Scanner(System.in);
        int numero         = 0; 
        int suma           = 0; 
        int multiplicacion = 1; 

        System.out.println("");
        System.out.println("********");
        System.out.println("*Reto10*");
        System.out.println("********");
    
        System.out.println("");
        System.out.println("**************************************");
        System.out.println("*(*) o (+) de los 10 primeros numeros*");
        System.out.println("**************************************");
        System.out.println("");

        System.out.print("Digite por favor un numero: ");
        numero = digit.nextInt();
        System.out.println("");

        for (int i = 1; i<=10; i++) {
            if(numero>10){
                multiplicacion = multiplicacion*i;
                System.out.println(multiplicacion);
            } 
            else if (numero<=10) {
                suma = suma+i;    
                System.out.println(suma);
            } 
        }  
    }
}
