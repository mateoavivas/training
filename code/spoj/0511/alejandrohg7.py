#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        for row in opened_file:
            if len(row) == 2:
                pass
            else:
                row = row.rstrip("\n")
                list_of_lists.append(row)
        return list_of_lists


def hello_kitty(case_word, no_replicates):
    '''this function takes a word and creates a matrix of itself
       rotating his letters till the first column spells the same of
        the original word '''
    base_matrix = case_word*(int(no_replicates) - 1)
    letter_adder = "0"
    letter_position = 0
    print(case_word+base_matrix)
    for letter in case_word[:-1]:
        letter_adder += letter
        letter_position += 1
        print(case_word[letter_position: None]+base_matrix+letter_adder[1:])


for case in open_file():
    word = case[:-2]
    no_times = case[-1]
    hello_kitty(word, no_times)

# $ python3 alejandrohg7.py
# Love
# oveL
# veLo
# eLov
# KittyKitty
# ittyKittyK
# ttyKittyKi
# tyKittyKit
# yKittyKitt
