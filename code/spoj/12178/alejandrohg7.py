#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        final_list_of_list = []
        for number in opened_file:
            list_of_lists.append(number.split())
        for inner_list in list_of_lists:
            final_list = []
            for number in inner_list:
                if '.' in number or ',' in number:
                    final_list.append(float(number))
                else:
                    final_list.append(int(number))
            final_list_of_list.append(final_list)
        return final_list_of_list


def grid_creator(lines, columns):
    """This function prints lines and columns"""
    if columns == 1:
        grid = [['****'], ['*..*'], ['*..*']]
        lines = lines
        while lines > 0:
            for row in grid:
                print(row[0]*columns)
            lines -= 1
        print(str('****' * columns))
    else:
        lines = lines
        columns_multiplier = (columns * 4) - (columns - 1)
        print(str('*' * columns_multiplier))
        while lines > 0:
            asterisk_dots = '*..' * columns
            print(str(asterisk_dots + '*'))
            print(str(asterisk_dots + '*'))
            print(str('*' * columns_multiplier))
            lines -= 1


for input_spoj in open_file()[1:]:
    lines_spoj = int(input_spoj[0])
    columns_spoj = int(input_spoj[1])
    grid_creator(lines_spoj, columns_spoj)
    print("")

# $ python3 alejandrohg7.py
# ****
# *..*
# *..*
# ****
# *..*
# *..*
# ****
# *..*
# *..*
# ****
#
# *************
# *..*..*..*..*
# *..*..*..*..*
# *************
# *..*..*..*..*
# *..*..*..*..*
# *************
# *..*..*..*..*
# *..*..*..*..*
# *************
# *..*..*..*..*
# *..*..*..*..*
# *************
#
# ****************
# *..*..*..*..*..*
# *..*..*..*..*..*
# ****************
# *..*..*..*..*..*
# *..*..*..*..*..*
# ****************
