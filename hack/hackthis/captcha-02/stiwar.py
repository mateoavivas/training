import requests
from PIL import Image
from skimage.measure import structural_similarity as ssim
import cv2

def compare(strA, strB):
    imgA = cv2.imread(strA)
    imgB = cv2.imread(strB)
    imgA = cv2.cvtColor(imgA, cv2.COLOR_BGR2GRAY)
    imgB = cv2.cvtColor(imgB, cv2.COLOR_BGR2GRAY)
    return ssim(imgA,imgB)

def output(n):
    switcher = {
        1: ":D",
        2: ":)",
        3: ":p",
        4: ":(",
        5: ";)",
        6: "B)",
        7: ":@",
        8: ":o",
        9: ":s",
        10: ":|",
        11: ":/",
        12: "<3"
    }
    return switcher.get(n, "sinDatos")

url = 'https://www.hackthis.co.uk/levels/extras/captcha2.php'
cookies={'PHPSESSID': 'your_cookie'}
req = requests.get(url, cookies=cookies, stream=True)
req.raise_for_status()
with open('c.png', 'wb') as fd:
    for chunk in req.iter_content(chunk_size=50000):        
        fd.write(chunk)

imagen = Image.open("c.png")
caja = (5, 0, 395, 40)
region = imagen.crop(caja) #recortar imagen
region.save("c1.png")
imagen = Image.open("c1.png")

width, height = imagen.size
out = ""
l = 26
for i in range(0,15):
    cajaEmo = (l*i, 0, l*(i+1), 40)
    region = imagen.crop(cajaEmo)
    region.save("e"+str(i)+".png")
    
for i in range(0,15):
    still = 't'
    j = 1
    while still=='t':
        r = compare("e"+str(i)+".png","emos/"+str(j)+".png")
        if r == 1.0:
            still = 'f'
            out = out + output(j)
        j += 1
url = 'https://www.hackthis.co.uk/levels/captcha/2'
response = requests.post(url, cookies=cookies, data = {'answer':out})
print(response.content)
