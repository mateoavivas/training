# language: en

Feature: Solve the challenge Who said this?
  From the happy-security.de website
  From the Kryptographie category
  With my username raballestas

  Background:
    Given a string

  Scenario: Successful solution
    When I decode the string with a shift of -3
    Then I google the decoded string
    And I see it is attributed to Gaius Julius Caesar
    And I solve the challenge
