## Version 2.0
## language: en

Feature: nightmare-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/nightmare_be1285a95aa20e8fa154cb977c37fee5.php
    """
    When I open the url with Firefox
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_nightmare where pw=('') and id!='admin'
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    NIGHTMARE Clear!
    """
    When I try with the well known payload:
    """
    pw=') OR 1 = 1 --
    """
    Then I get "No Hack ~_~"

   Scenario: Success:Sqli-nullbyte-technique
    Given I inspect the code one more time
    And I see that they validate some strings to avoid union sqli
    And also validates "#|-" and backslash characters
    And the length of the payload
    """
    if(preg_match('/prob|_|\.|\(\)|#|-/i', $_GET[pw])) exit("No Hack ~_~");
    if(strlen($_GET[pw])>6) exit("No Hack ~_~");
    if($result['id']) solve("nightmare");
    """
    When I use a quote and a nullbyte to bypass the control
    And a well known payload
    """
    ?pw=')=0;%00
    """
    Then I solve the challenge
