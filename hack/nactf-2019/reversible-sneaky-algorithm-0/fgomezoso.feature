## Version 2.0
## language: en

Feature: reversible-sneaky-algorithm-0 - crypto - nactf
  Site:
    https://www.nactf.com/
  Category:
    Crypto
  User:
    fgomezoso
  Goal:
    Enter the flag in the format nactf{<FLAG>}

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
     | Python          | 3.8.0        |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    https://www.nactf.com/challenges#Reversible%20Sneaky%20Algorithm%20#0
    """
    And the challenge has a flag field
    And the challenge provides a txt file "rsa.txt"
    And there is a cipher, private key and modulus within the txt file
    And the message to decipher is originally a string containing a flag

  Scenario: Success:Decipher the RSA message using theory and Python
    Given I know the challenge is about the RSA cryptosystem
    When I check the method to decrypt the RSA message
    Then I find out there is an equation for dechipering messages
    """
    m = c^d (mod n)
    """
    And It is a modular exponentiation
    And the c, d and n variables are already known
    When I'm going to calculate a modular exponentiation using python
    Then I need to use the pow function with 3 arguments
    And I can find the secret message
    When I know the message value
    Then I have to decode it to hexadecimal and ascii
    When I write a python script "fgomezoso.py" using the previous steps
    Then I get the flag
    """
    nactf{<FLAG>}
    """
    And I solve the challenge
