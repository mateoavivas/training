## Version 2.0
## language: en

Feature: http://overthewire.org/wargames/bandit
  Site:
    http://overthewire.org/wargames/bandit/
  User:
    charlie517 (wechall)
  Goal:
    Read file with special characters

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Linux Mint      | 4.15.0-45     |
    | SSH             | OpenSSH_7.6p1 |
  Machine information:
    Given I am accessing the bandit.labs.overthewire.org
    And SSH with ssh bandit0.labs.overthewire.org -p 2220
    And enter a console that allows me to move around the system
    And SSH version OpenSSH_7.4p1
    And is running on Linux bandit with kernel 4.18.12

  Scenario: Success:
    Given I am logged in the machine
    And I cat the "-" file
    Then I can see the password for level2
    And get access to bandit2 [bandit1-evidence](bandit1.png)
