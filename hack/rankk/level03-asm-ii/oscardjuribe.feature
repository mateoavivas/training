## Version 2.0
## language: en

Feature: level03-asm-ii
  Site:
    https://www.rankk.org/challenges/asm-ii.py
  User:
    alestorm980 (wechall)
  Goal:
    Find the lower bits of the "B" register at each block

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7.15+   |
  Machine information:
    Given An asm code
    Then Find the value in the lower bits of the "B" register
    And Convert it to ascii

  Scenario: Fail: Doing it by hand
    Given An asm code
    When I try to solve the challenge by hand
    Then I make some mistakes during the calculation
    And I get a wrong answers

  Scenario: Success: Python script
    Given An asm code
    When I fail doing it by hand
    Then I decide to create a python script "oscardjuribe.py"
    When I parse the operation at each line
    Then I create methods to simulate the asm instruction
    And I store the registers value in an array
    When I have a blank line
    Then I convert the lower bits of the register "B" to ascii
    When I run "python oscardjuribe.py"
    Then I get the output
    """
    The solution is: <flag>
    """
    And I solve the challenge
