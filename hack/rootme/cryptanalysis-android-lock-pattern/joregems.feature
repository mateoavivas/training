## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
    decoding, BoF...
  User:
    joregems
  Goal:
    Discover the key pattern for unlock an android phone

  Background:
    Hacker's software:
      | <Software name> | <Version>   |
      | Ubuntu 18.10    | Ubuntu 18.10 |
      | Firefox Quantum | 66.0.2 (64-bit)|
      | self made brute force program| unversioned |

    Machine information:
      Given the files of system of android phone obtained from:
      """
      "https://www.root-me.org/es/Challenges/Criptoanalisis
      /System-Android-lock-pattern"

      System - Android lock pattern
      """
      And my program source available in
      """
      https://gitlab.com/joregems/bruteforce_android_pattern/tree/master/source
      """
      And is available too in
      """
      or
      joregems.pl
      """
      And the executable file available in
      """
      https://gitlab.com/joregems/bruteforce_android_pattern/tree/master/source
      the executable is named as brute
      """
      Given the password is numeric and is on hexadecimal:
      Given the file gesture.key is in /data/system folder
      And the file gesture.key contains a hashed sha1 password
      And its only the sha1 of the numbers from 0 to 8 written like the example:
      """
      example
      sha1("\x01\x02\x03\x00")
      """
      And learned from
      """
      https://github.com/sch3m4/androidpatternlock
      """
      And using a self made program to write a bruteforce dictionary
      And based on heap algorithm adapted from
      """
      https://es.wikipedia.org/wiki/Algoritmo_de_Heap
      """
      And using brute force with the dictionary created
      Then I can know what is the password.

  Scenario: Success: Making a force brute dictionary
    Given the elements of the password are known
    And known the heap algorithm can find all permutations of the set
    """
    set=("\x00","\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09");
    """
    Then writing code and make
    And trimming the set and getting shorter combinations
    """
    example of shorter combination
     "\x01\x02\x03\x00"
    """
    Then making iterations and deleting repeated elements we get a dictionary
    And gathering all possible combinations
    Then we get a dictionary
    And have all possible keys suitable for brute force attack

  Scenario: Success: Brute force attack
    Given we have a list with all possible combinations of the password
    Then we hash with sha1 algorithm every password
    """
    example:
    sha1("\x01\x02\x03\x00")
    """
    And compare with the one from gesture.key
    And using my program
    """
    "./brute gesture.key" using the executable form gitlab repository

    "perl brute.pl gesture.key" source code from repository
    you will need install some dependencies

    "perl joregems.pl gesture.key"
    using the source code, you will need install some dependencies.
    """
    And gesture.key is the path of the gesture.key file
    Then we got the code
    """
    010405020603070800
    """
    Then convert it to decimal we get
    """
    145263780
    """
    Then we put it on the validation field from the challenge page and get
    """
    Well done you win 20 Points
    """
