"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting

"""
from __future__ import print_function


def decrypt_middle_10_challenge(data):
    """ Function to decrypt """

    for _ in range(int(data())):
        encrypted_string = input().split()
        inv_map = {'1A': 'a', '1B': 'b', '1C': 'c', '1D': 'd',
                   '1E': 'e', '1F': 'f', '20': 'g', '21': 'h',
                   '22': 'i', '23': 'j', '24': 'k', '25': 'l',
                   '26': 'm', '27': 'n', '28': 'o', '29': 'p',
                   '2A': 'q', '2B': 'r', '2C': 's', '2D': 't',
                   '2E': 'u', '2F': 'v', '30': 'w', '31': 'x',
                   '32': 'y', '33': 'z', '34': 'A', '35': 'B',
                   '36': 'C', '37': 'D', '38': 'E', '39': 'F',
                   '3A': 'G', '3B': 'H', '3C': 'I', '3D': 'J',
                   '3E': 'K', '3F': 'L', '40': 'M', '41': 'N',
                   '42': 'O', '43': 'P', '44': 'Q', '45': 'R',
                   '46': 'S', '47': 'T', '48': 'U', '49': 'V',
                   '4A': 'W', '4B': 'X', '4C': 'Y', '4D': 'Z',
                   '50': '.', '51': '_', '52': ';', '53': ':',
                   '54': '?', '55': '!', '56': ' ', '10': '0',
                   '11': '1', '12': '2', '13': '3', '14': '4',
                   '15': '5', '16': '6', '17': '7', '18': '8',
                   '19': '9'}

        indexedmap = []
        puzzle = []
        groups = []

        for index, tup in enumerate(inv_map.items()):
            indexedmap.append((index, tup[0]))
            puzzle.append((index, tup[1]))

        indexedmap = dict(indexedmap)
        indexedmap = {v: k for k, v in indexedmap.items()}

        for pairs_str in range(len(encrypted_string[0])):
            groups.append(encrypted_string[0][pairs_str:pairs_str+2])

        for str_to_decrypt in groups[0::2]:
            groups.append(str_to_decrypt)
        del groups[0:14]

        for group in enumerate(groups):

            if group[0] in [0, 3, 6]:
                groups[group[0]] = indexedmap[group[1]]

            if group[0] in [1, 4]:
                idx_val = indexedmap[group[1]] - 1
                groups[group[0]] = idx_val

            if group[0] in [2, 5]:
                idx_val = indexedmap[group[1]] - 2
                groups[group[0]] = idx_val

        for res in enumerate(groups):
            groups[res[0]] = puzzle[res[1]]

        for val in enumerate(groups):
            groups[val[0]] = val[1][1]

        print(''.join(groups))


decrypt_middle_10_challenge(input)

# $python dferrans.py < DATA.lst
# m0n5t3r
