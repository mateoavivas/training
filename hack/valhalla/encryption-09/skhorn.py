#!/usr/bin/env python3


class CustomEncryption():
    """
    Script to solve the equation encryption given
    on the challenge.
    """
    def __init__(self):

        crypted = []
        phrase = "for the glory of the gods"
        crypted = self.encrypt(phrase)

        msg = ''.join(crypted)
        print(msg)

    def encrypt(self, phrase):
        """
        Fn to encrypt plain text given an encryption eq
        The equation is:
        E(x) = 3x + 6 % 26
        Where x stands for each character on the plain text
        leaving aside the spacing.
        Args:
            phrase(str)     :Plain text message to cipher

        Returns:
            text[]          :Ciphertext

        """
        text = []
        for i, item in enumerate(phrase):
            if item is not ' ':
                char_index = self.alphabet(item)
                encrypt_func = ((3 * char_index) + 6) % 26
                text.append(self.alphabet(encrypt_func))
            else:
                text.append(" ")

        return text

    def alphabet(self, char):
        """
        Fn for the sake of simplicity:
            - take a character and get its index value inside the alphabet
            - take a number and get its correspondent character
        Args:
            char(str/int):      Character or number

        Returns:
            The value or the key given the param
        """
        alpha = {
            'a': 0,
            'b': 1,
            'c': 2,
            'd': 3,
            'e': 4,
            'f': 5,
            'g': 6,
            'h': 7,
            'i': 8,
            'j': 9,
            'k': 10,
            'l': 11,
            'm': 12,
            'n': 13,
            'o': 14,
            'p': 15,
            'q': 16,
            'r': 17,
            's': 18,
            't': 19,
            'u': 20,
            'v': 21,
            'w': 22,
            'x': 23,
            'y': 24,
            'z': 25
        }

        if isinstance(char, int):
            for key, value in alpha.items():
                if value == char:
                    return key

        else:
            return alpha[char]


CustomEncryption()
