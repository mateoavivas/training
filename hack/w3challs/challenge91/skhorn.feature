# language: en

Feature: Solve challenge 91
  From site W3Challs
  From Cryptography Category
  With my username Skhorn

  Background:
    Given the fact i have an account on W3Challs site
    And i have Debian 9 as Operating System
    And i have internet access
    And i downloaded the resources provided in the challenge

  Scenario: Failed attempt
    Given the link to the challenge
    And a description of a company using a method of deduplication
    And a concern with privacy 
    And a tar.gz file to download
    And i extract the files
    And the description states there might be useful information in the files
    And in the file there are three folders: client, dump, mails
    And while checking the files inside the mails folder
    And i see there are two mails
    And one of those mails states that the sender setted up a database
    And this database uses the my.cnf_template sent as an attachment in the mail
    And that it only changed the password for a 6-digit numeric one
    And while inspecting in depth the mails, there can be seen a file exchange
    And one of those files is a mysql config template: my.cnf_template
    And the other file is a pdf containing an interview
    And while reading the interview i see a few revelant statements
    And one of those statements is client side encryption
    And this encryption its done using SHA-256 - AES-256 combination
    And another statement talk about deduplication done in server side
    And this method it's accomplished via Convergent Encryption
    When i check the dump folder
    And i see two directorys containg one file each
    And the files are called: my.cnf and my.cnf_template
    And i open the files
    And i see it seems to be encrypted data
    And i set those files as the targets
    When i check the client folder
    And i see two folders plus a client.py script
    And discarding one empty folder called /shared_folder
    And i check the other folder called BitMaison
    And i see there are several python scripts: BMClient.py, BMServer.py, BMUserInterface.py
    And i begin to read the code in the script to understand what it does
    And while doing this i fix several syntatic errors to run the scripts
    And i try to run BMUserInterface.py
    And an error stating bad interpreter it's thrown
    And i fix it using dos2unix on all the scripts 
    And next i do a few tests within the functions in BMClient.py
    And in BMClient.py had to edit an AES function that required an extra argument, the IV 
    And stating that IV means Initialization Vector
    And while searching for Convergent Encryption in google
    And i see there is an specific attack called partial information attack
    And it states that having enough information on what was the encrypted file
    And by trying to cipher several plain text following the same format
    And that by doing this it is possible to get the information that was encrypted
    And given the fact that a 6-digit numeric password was used
    And i use a software called crunch to generate a file containg all possible numbers in range of 0-9
    And i code a script to help me to change a single line in the my.cnf_template
    And i know this line its the one about the password
    And i code another function to cipher each generated new file
    And i code another function to compare that file with the one in the dump file
    And next i fired it up
    And took a coffe plus a smoke to wait for results
    Then there was no positive result

  Scenario: Succesful Solution
    When i got no positive result
    And i checked my script to see what might be wrong
    And i suspected about the extra argument i had to add in a function in BMClient.py 
    And then i searched for the version of pycrypto 
    And i prepared a virtualenv in the folder
    And i see that the pycrypto version i'm using is 2.6
    And i uninstall it
    And install pycrypto 2.2
    And then i fired it up again first against the my.cnf_template encrypted inside the dump folder
    And it gave a match
    And use the the numbers file generated
    And within the script i code a function to replace the single line related with password
    And compare this new file with the other encrpyted file in dump folder called my.cnf
    And run the script again
    And took a coffe plus a smoke to wait for results
    And it gave a match
    And i searched wich password was the one
    And i use this number as input in the challenge to solve it
    And i pass the challenge
