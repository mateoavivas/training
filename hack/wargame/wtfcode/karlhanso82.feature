# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr:8080/wtf_code/
  Category:
    programming
    hacking
  User:
    karlhanso82
  Goal:
    unveil the message

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
  Given the challenge URL
  """
  http://wargame.kr:8080/wtf_code/
  """
  Then I open the url with Chromium
  And I see a text in korean
  Then I see another text this time in english
  """
  is this source code really???? i can`t see anything really!
  """
  Then a link with a file called source_code.ws
  """
  https://ideone.com/wooCEG
  """
  And after a quick inspection
  Then I find that is filled with whitespaces and dots
  And I suppose that it must be related with esoteric programing language

 Scenario: Success:getting-the-flag
  Then I decided to search for more info on internet
  And I found a esoteric white programming language
  Then I put the code in a online tool
  And Then I run it with whitespace language
  Then it display an output with flag of the challenge
  """
  Wow! Key is c4a6f0ff57de9911468612a12c83c9ff6ff70626
  """
  Then I put the key and solve the challenge

