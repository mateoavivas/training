Feature: Solve Batch III challenge
  from site World of Wargame
  logged as EdPrado4


Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have VMWare Software
  And I have a Windows XP virtual machine
  And I have OllyDB software
  And I have Sublime Text software

Scenario: First failed attempt
  Given A crypted batch file
  When I load the batch file into OllyDB
  And I analize the assembler code
  Then I do not find anything relevant
  And I understand I need to find a way to decrypt the code

Scenario: Second failed attempt
  Given A crypted batch file
  When I try to edit the file
  And I find the encription key of the code
  Then I write a script in python to decode it
  And I find the sourcecode of the file
  And I spot the word "cadena" within the code
  When I use "cadena" as password
  Then I fail the challenge
  But I realize that it is the content of "cadena" what I need

Scenario: Challenge solved
  Given My previous failed attempts
  When I analize the sourcecode
  And I realize there is another encription within the code
  Then I find the internal encription key
  When I use the internal encription key in the string "ybhiboijn"
  Then I find the password
  And I solve the challenge
